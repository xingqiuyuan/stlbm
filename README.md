# STLBM Library: Many-core Lattice Boltzmann with C++ Parallel Algorithms

![Lid-driven cavity flow at Re=10'000 simulated with STLBM](data/cavity_re10000.jpeg)

STLBM is a framework for multi-threaded parallel lattice-Boltzmann simulations on multiple platforms, including many-core CPUs and GPUs, with massive efficiency. Its unique feature is that it relies only on the C++17 standard (Parallel Algorithms) to achieve parallel efficiency, and does not use any language extensions, external libraries, vendor-specific code annotations, or pre-compilation steps. To exploit these parallel features on CPUs, it requires an STL backend like the open-source Intel Thread Building Blocks (TBB) or a recent version of Visual C++, and on NVidia GPUs you will want to look out for the nvc++ compiler in a recent version of the NVIDIA HPC SDK.

STLBM ships with a full-featured end-user application (the flow in a lid-driven 3D cavity) with appropriate analysis and post-processing functionality. The actual purpose of the library is however to serve as a collection of templates for users to start their own parallel LB code or to accelerate their own applications. A didactic approach has been chosen which keeps the code extremely concise, and which lets the user play with different variants:

   * Some versions of the code use aggressive loop unrolling to achieve optimal performance, especially on GPUs, while their "educational" counterparts are more lightly optimized and thus more readable (but still reach good, and sometimes better, performance, especially on CPUs).
   * Three different LB implementation schemes (double-population scheme, swap algorithm, and AA-pattern) are available, combined with two different memory layouts (array-of-structure and structure-of-array), to allow the selection of an optimal choice for a given platform.
   * Nine different commonly used LB collision models are available in the framework. Among them, two are based on the BGK operator with either a weighted second-order or a non-weighted fourth-order equilibrium. The TRT was included due to its very good behavior for pore-scale porous media simulations. Several multi-relaxation-time formulations are also available (raw, Hermite, central, central Hermite and cumulant). Finally, the recursive regularized collision model is dedicated to high-Reynolds number flow simulations. 

An in-depth description of the STBLM library is found in the manuscript [Cross-platform programming model for many-core lattice Boltzmann simulations](https://arxiv.org/abs/2010.11751).


## How to cite STLBM

The article describing STLBM is currently in preprint stage and can be cited as follows:

```    
@misc{latt2020crossplatform,
      title={Cross-platform programming model for many-core lattice {B}oltzmann simulations}, 
      author={Jonas Latt and Christophe Coreixas and Joël Beny},
      year={2020},
      eprint={2010.11751},
      archivePrefix={arXiv},
      primaryClass={physics.comp-ph}
}
```    

# How to use the STLBM library

## To learn how to implement parallel LB codes with C++17 Parallel Algorithms

Have a look at the self-contained codes (``apps/selfcontained_cavity_aa_soa``, ``apps/selfcontained_cavity_swap_aos``, ``apps/selfcontained_cavity_twopop_soa``) to understand the implementation strategy of LB applications with C++ Parallel Algorithms: they exhibit a full application (including data analysis and post-processing) in a single file, and in less than 500 lines of code. Explanations are provided in our article on STLBM.

## To figure out which implementation scheme is optimal on your platform

Run the ``cavity`` application in benchmark mode: ``cd out; cp ../apps/config.cavity.benchmark_of_all_schemes config; ./cavity``. This runs ten times all six implementation schemes for a chosen LB collision model (default is bgk, but you can edit the ``config`` file to choose another one) and plots the six median performance measurements to the terminal.

## To simulate a flow in a 3D lid-driven cavity

Run the ``cavity`` application in production mode: ``cd out; cp ../apps/config.cavity.production config``. Edit the ``config``file to adjust the parameters, and run the application: ``./cavity``.

# Compilation and execution

The STLBM library has been compiled and tested with recent versions of the following compilers: ``g++ (GNU GCC)``,  ``CLang``, ``Intel ICC`` (important: you need the ``-ipo`` option to cirumvent a compiler error), ``Visual C++ (Visual Studio 2019)``, ``nvc++ (based on the PGI compiler; NVIDIA HPC SDK)``.

## Under Linux

To produce a CPU version, you need: a recent C++ compiler (e.g. ``sudo apt install clang``), a recent version of CMake (e.g. ``sudo apt install cmake``), and Intel's Thread Building Blocks (e.g. ``sudo apt install libtbb-dev``). To produce a NVIDIA GPU version, you need a recent version of the [NVIDIA HPC SDK](https://developer.nvidia.com/hpc-sdk). STLBM is compiled with CMake, which needs to be called providing the name of the C++ compiler: the ``compile.sh`` script does this for you. Edit the script to uncomment the line with the selected compiler, and compile the project with ``./compile.sh``. The four executables of the project (the ``cavity`` executable and the targets of the three self-contained codes) are not in the ``out`` directory.

Note: the ``compile.sh`` removes the CMake cache every time to guarantee everything goes well when you change compiler. This is inconvenient if you want to work on the code and frequently compile withe the same compiler. Instead of running ``compile.sh`` every time, you rather switch to the ``build`` directory and type ``make``: that will save you time.

Note: if you execute the ``cavity`` executable, you must place a configuration file into the ``out`` directory, named ``config``; you can use one of the templates: ``cd out; cp ../apps/config.cavity.benchmark_of_single_scheme config; ./cavity``

## Under Windows

We are only covering Windows compilation for CPUs, as the NVIDIA GPU backend for Parallel Algorithms was not available under Windows at the moment this document was written. Install a recent version of Visual Studio 2019. Community edition is OK, you must include the Visual C++ compiler and possibly also some extensions relative to HPC and parallel C++ features. In the ``Get Started`` section of the welcome screen, select ``Clone a repository``, and provide the address ``https://gitlab.com/unigehpfs/stlbm``; click ``Clone``. Some windows appear, including one named ``Output``, which shows that the project is identified as a  ``CMake`` project, and CMake is being configured. Wait until the line ``Cmake generation finished`` is produced. If you see an error message in the ``Output`` window, something went wrong: maybe a missing Visual Studio package, or a misconfiguration, or something else.

In the toolbar, you are now interested in two dialogs, one which is called ``Configuration`` and shows your default configuration (e.g. ``x64-Debug (default)``), and one which is called ``Select Startup Item``: it has a green arrow on the left, and a black arrow on the right which, when selected, reveals the four targets of the STLBM project: the ``cavity`` executable and the three selfcontained codes. You can now either select one of the targets, and compile-and-execute it in one step by clicking on the green arrow, or you can build all targets through the menu item ``Build -> Build All``. Watch again the ``Output`` window to make sure there is no error message (warnings are OK).

Your default configuration is probably in debug mode, but you are looking for efficiency in mode. Right-click ``CMakeLists.txt`` in the STLBM root directory and select ``CMake Settings for stlbm``. In the ``Configurations`` pane, click the green "+" symbol and add a ``x64-Release`` configuration. Save your configuration (Ctl-S), check that the messages in the ``Output`` window are not suspicious, and see the ``x64-Release`` Configuation appear in the ``Configuration`` dialog of the toolbar. Select it and rebuild your project (menu ``Build -> Rebuild All``). You can now execute one of your four executables directly from Visual Studio or, for better performance, from Windows explorer (the ``Select Startup Item`` dialog tells you where they are located). Be aware that

* If you execute one of the self-contained codes, you may be frustrated by the fact that the command window closes before you could read the MLups performed by the program. Add something like ``int a; cin >> a;`` at the end of the program to make it wait for a keystroke before closing.

* If you execute the ``cavity`` executable, you must place a configuration file into the same directory as the executable. It must be named ``config`` without extension, and you find examples in the ``apps`` directory of STLBM.

## Under Mac OS X

We have never compiled STLBM on a Mac, but we hear from our colleagues that it "just works like Linux if you use Homebrew (a package management system)", at least if you are intending to run the code on CPUs (not GPUs). Try to install bash, CMake, a C++ compiler, and Intel's Thread Building Blocks, after which you should be able to follow the Linux instructions. If you succeed, don't hesitate to communicate us your procedure so that we can publish it here !

# Performance measurements on selected platforms

This section lists some platforms on which the STLBM library showed good performance.

Performance is measured in million lattice site updates per second (MLups, more is better). Note that all performance measurements were carried out with double-precision floating-point arithmetics.

| Platform                 | Model  | Optimization  | Scheme                | MLups  |
| ------------------------ | ------ | ------------- | --------------------- | ------ |
| EPCY 64-Core (CPU)       | BGK-O2 | Not unrolled  | AA soa                | 322.0  |
| NVIDIA RTX 2080 Ti (GPU) | BGK-O2 | Unrolled      | double-population soa | 1189.9 |

Details of the tested platforms:

| Name               | Model                                      | Compiler / Options                    |
| ------------------ | ------------------------------------------ | ------------------------------------- |
| EPCY 64-Core       | AMD EPYC 7742 64-Core Processor @ 2.24 GHz | CLang 10.0.1 / -std=c++17 -O3         |
| NVIDIA RTX 2080 Ti | TU102 [GeForce RTX 2080 Ti]                | nvc++ 20.7-0 / -stdpar -std=c++17 -O3 |

# Software License
Note: the license text below covers all C++ source code files of the STLBM project. The license terms for the data files are specified [here](data/README.md).

STLBM SOFTWARE LIBRARY

Copyright © 2020 University of Geneva

Authors: Jonas Latt, Christophe Coreixas, Joël Beny

Contact: Jonas.Latt@unige.ch

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
