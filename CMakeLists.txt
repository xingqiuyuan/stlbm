# ******************************************************************************
# STLBM SOFTWARE LIBRARY

# Copyright © 2020 University of Geneva
# Authors: Jonas Latt, Christophe Coreixas, Joël Beny
# Contact: Jonas.Latt@unige.ch

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ******************************************************************************

# To run cmake with the appropriate compiler, you should invoke
# it by running the script "compile.sh". Edit "compile.sh" first
# to select your compiler.

cmake_minimum_required(VERSION 3.6)

project(stlbm LANGUAGES CXX)

include(CheckLanguage)
check_language(CUDA)
if (CMAKE_CUDA_COMPILER)
    enable_language(CUDA)
endif()

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/../out)

message(STATUS "Compiling with ${CMAKE_CXX_COMPILER_ID} compiler. Compilation command is ${CMAKE_CXX_COMPILER}.")

set (USE_TBB OFF)

if(${CMAKE_CXX_COMPILER_ID} STREQUAL GNU)
    set(CMAKE_CXX_FLAGS "-std=c++17 -O3 -Wall")
    set (USE_TBB ON)
    #set(CMAKE_EXE_LINKER_FLAGS "-ltbb")
elseif(${CMAKE_CXX_COMPILER_ID} STREQUAL Clang)
    set(CMAKE_CXX_FLAGS "-std=c++17 -O3 -Wall -Wextra -Wpedantic")
    set (USE_TBB ON)
    #set(CMAKE_EXE_LINKER_FLAGS "-ltbb")
elseif(${CMAKE_CXX_COMPILER_ID} STREQUAL Intel)
    set(CMAKE_CXX_FLAGS "-std=c++17 -ipo -O3 -xHOST -Wall")
    set (USE_TBB ON)
    #set(CMAKE_EXE_LINKER_FLAGS "-ltbb")
elseif(${CMAKE_CXX_COMPILER_ID} STREQUAL PGI) # nvc++ is based on the PGI compiler
	set(CMAKE_CXX_FLAGS "-stdpar -O1 -Wall -std=c++17")
elseif(${CMAKE_CXX_COMPILER_ID} STREQUAL MSVC)
    set(CMAKE_CXX_FLAGS "/Ox /Ot /GS- /GL /std:c++latest")
else()
    message(FATAL_ERROR "Compiler ${CMAKE_CXX_COMPILER_ID} is unknown.")
endif()

set(CMAKE_CUDA_FLAGS "-arch=sm_60 -O3")

add_subdirectory(apps)

