// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm.h"
#include <fstream>
#include <iomanip>
#include <cmath>
#include <string>
#include <sstream>
#include <stdexcept>
#include <map>
#include <set>
#include <any>

// Compute the average kinetic energy in lattice units in all bulk cells.
template<class LBM>
double computeEnergy(LBM& lbm)
{
    Dim const& dim = lbm.dim;
    double energy = 0.;
    for (int iX = 0; iX < dim.nx; ++iX) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            for (int iZ = 0; iZ < dim.nz; ++iZ) {
                size_t i = lbm.xyz_to_i(iX, iY, iZ);
                if (lbm.flag[i] != CellType::bounce_back) {
                    auto[rho, v] = lbm.macro(lbm.lattice[i]);
                    energy += v[0] * v[0] + v[1] * v[1] + v[2] * v[2];
                }
            }
        }
    }
    energy *= 0.5;
    return energy;
}

// Compute the maximum velocity in lattice units in all bulk cells.
template<class LBM>
double computeMaxVel(LBM& lbm)
{
    Dim const& dim = lbm.dim;
    double maxVel = 0.;
    for (int iX = 0; iX < dim.nx; ++iX) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            for (int iZ = 0; iZ < dim.nz; ++iZ) {
                size_t i = lbm.xyz_to_i(iX, iY, iZ);
                if (lbm.flag[i] != CellType::bounce_back) {
                    auto[rho, v] = lbm.macro(lbm.lattice[i]);
                    double velSqr = v[0] * v[0] + v[1] * v[1] + v[2] * v[2];
                    if (velSqr > maxVel) maxVel = velSqr;
                }
            }
        }
    }
    maxVel = std::sqrt(maxVel);
    return maxVel;
}

// Compute the permeability of a porous media, assuming a converged x-directional flow with
// imposed velocity. To do this, the pressure difference is computed between the two buffer
// zones in the inflow and the outflow area.
template<class LBM>
double computePermeability(LBM& lbm, int buffer, double ulb, double nu_lb, double dx)
{
    Dim const& dim = lbm.dim;
    double av_rho1 = 0.;
    double av_rho2 = 0.;
    double width = dim.nx - 2 * buffer;
    // Start at 1, because the x=0 row is a bounce-back row.
    for (int iX = 1; iX < buffer; ++iX) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            for (int iZ = 0; iZ < dim.nz; ++iZ) {
                size_t i1 = lbm.xyz_to_i(iX, iY, iZ);
                size_t i2 = lbm.xyz_to_i(iX - 1 + buffer + width, iY, iZ);
                auto[rho1, v1] = lbm.macro(lbm.lattice[i1]);
                auto[rho2, v2] = lbm.macro(lbm.lattice[i2]);
                av_rho1 += rho1;
                av_rho2 += rho2;
            }
        }
    }
    av_rho1 /= (buffer-1) * dim.ny * dim.nz;
    av_rho2 /= (buffer-1) * dim.ny * dim.nz;
    double delta_p = 1./3. * (av_rho1 - av_rho2);
    return (ulb * nu_lb * width / delta_p) * dx * dx;
}

// Save all macroscopic variables in a text file (for post-processing for example
// in Python or Octave).
template<class LBM>
void saveFields(LBM& lbm)
{
    Dim const& dim = lbm.dim;
    std::ofstream fvx("vx.dat");
    std::ofstream fvy("vy.dat");
    std::ofstream fvz("vz.dat");
    std::ofstream fv("v.dat");
    std::ofstream frho("rho.dat");
    for (int iX = dim.nx - 1; iX >= 0; --iX) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            size_t i = lbm.xyz_to_i(iX, iY, dim.nz / 2);
            auto [rho, v] = lbm.macro(lbm.lattice[i]);
            if (lbm.flag[i] == CellType::bounce_back) {
                rho = 1.0;
                v = { lbm.f(i, 0) / (6. * lbm.t[0]),
                      lbm.f(i, 1) / (6. * lbm.t[1]),
                      lbm.f(i, 2) / (6. * lbm.t[2]) };
            }
            fvx << v[0] << " ";
            fvy << v[1] << " ";
            fvz << v[2] << " ";
            fv << std::sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]) << " ";
            frho << rho << " ";

        }
        fvx << "\n";
        fvy << "\n";
        fvz << "\n";
        fv << "\n";
        frho << "\n";
    }
}

// Save the macroscopic variables in a VTK file (for post-processing for
// example with Paraview).
template<class LBM>
void saveVtkFields(LBM& lbm, int time_iter, double dx = 0.)
{
    using namespace std;
    Dim const& dim = lbm.dim;
    if (dx == 0.) {
        dx = 1. / dim.nx;
    }
    stringstream fNameStream;
    fNameStream << "sol_" << setfill('0') << setw(7) << time_iter << ".vtk";
    string fName = fNameStream.str();
    ofstream fStream;
    fStream.open(fName.c_str());
    fStream << "# vtk DataFile Version 2.0" << endl ;
    fStream << "iteration " << time_iter << endl ;
    fStream << "ASCII" << endl ;
    fStream << endl ;
    fStream << "DATASET STRUCTURED_POINTS" << endl ;
    fStream << "DIMENSIONS " << dim.nx << " " << dim.ny << " " << dim.nz << endl ;
    fStream << "ORIGIN 0 0 0" << endl ;
    fStream << "SPACING " << dx << " " << dx << " " << dx << endl ;
    fStream << endl ;
    fStream << "POINT_DATA " << dim.nx*dim.ny*dim.nz << endl ;

    // Density
    fStream << "SCALARS Density float 1" << endl ;
    fStream << "LOOKUP_TABLE default" << endl ;
    for (int iZ = dim.nz-1; iZ >=0; --iZ) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            for (int iX = 0; iX < dim.nx; ++iX) {
                size_t i = lbm.xyz_to_i(iX, iY, iZ);
                auto [rho, v] = lbm.macro(lbm.lattice[i]);
                if (lbm.flag[i] == CellType::bounce_back) {
                    rho = 1.0;
                    v = { lbm.f(i, 0) / (6. * lbm.t[0]),
                          lbm.f(i, 1) / (6. * lbm.t[1]),
                          lbm.f(i, 2) / (6. * lbm.t[2]) };
                }
                fStream << rho << " ";
            }
            fStream << endl ;
        }
        fStream << endl ;
    }
    fStream << endl ;

    // velocity_X
    fStream << "SCALARS velocity_X float 1" << endl ;
    fStream << "LOOKUP_TABLE default" << endl ;
    for (int iZ = dim.nz-1; iZ >=0; --iZ) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            for (int iX = 0; iX < dim.nx; ++iX) {
                size_t i = lbm.xyz_to_i(iX, iY, iZ);
                auto [rho, v] = lbm.macro(lbm.lattice[i]);
                if (lbm.flag[i] == CellType::bounce_back) {
                    v = { lbm.f(i, 0) / (6. * lbm.t[0]),
                          lbm.f(i, 1) / (6. * lbm.t[1]),
                          lbm.f(i, 2) / (6. * lbm.t[2]) };
                }
                fStream << v[0] << " ";
            }
            fStream << endl ;
        }
        fStream << endl ;
    }
    fStream << endl ;

    // velocity_Y
    fStream << "SCALARS velocity_Y float 1" << endl ;
    fStream << "LOOKUP_TABLE default" << endl ;
    for (int iZ = dim.nz-1; iZ >=0; --iZ) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            for (int iX = 0; iX < dim.nx; ++iX) {
                size_t i = lbm.xyz_to_i(iX, iY, iZ);
                auto [rho, v] = lbm.macro(lbm.lattice[i]);
                if (lbm.flag[i] == CellType::bounce_back) {
                    v = { lbm.f(i, 0) / (6. * lbm.t[0]),
                          lbm.f(i, 1) / (6. * lbm.t[1]),
                          lbm.f(i, 2) / (6. * lbm.t[2]) };
                }
                fStream << v[1] << " ";
            }
            fStream << endl ;
        }
        fStream << endl ;
    }
    fStream << endl ;

    // velocity_Z
    fStream << "SCALARS velocity_Z float 1" << endl ;
    fStream << "LOOKUP_TABLE default" << endl ;
    for (int iZ = dim.nz-1; iZ >=0; --iZ) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            for (int iX = 0; iX < dim.nx; ++iX) {
                size_t i = lbm.xyz_to_i(iX, iY, iZ);
                auto [rho, v] = lbm.macro(lbm.lattice[i]);
                if (lbm.flag[i] == CellType::bounce_back) {
                    v = { lbm.f(i, 0) / (6. * lbm.t[0]),
                          lbm.f(i, 1) / (6. * lbm.t[1]),
                          lbm.f(i, 2) / (6. * lbm.t[2]) };
                }
                fStream << v[2] << " ";
            }
            fStream << endl ;
        }
        fStream << endl ;
    }

    // Wall flag matrix
    fStream << "SCALARS Flag int 1" << endl ;
    fStream << "LOOKUP_TABLE default" << endl ;
    for (int iZ = dim.nz-1; iZ >=0; --iZ) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            for (int iX = 0; iX < dim.nx; ++iX) {
                size_t i = lbm.xyz_to_i(iX, iY, iZ);
                if (lbm.flag[i] == CellType::bounce_back) {
                    fStream << "1" << " ";
                }
                else {
                    fStream << "0" << " ";
                }
            }
            fStream << endl ;
        }
        fStream << endl ;
    }
}

// Save center-line velocity and pressure profiles in a text file.
template<class LBM>
void saveProfiles(LBM& lbm, double ulb)
{
    using namespace std;
    Dim const& dim = lbm.dim;
    ofstream middlex("middlex.dat");
    ofstream middley("middley.dat");
    ofstream middlez("middlez.dat");
    int x1, x2, y1, y2, z1, z2;

    if (dim.nx % 2 == 0) {
        x1 = dim.nx / 2 - 1;
        x2 = dim.nx / 2;
    }
    else {
        x1 = x2 = (dim.nx - 1) / 2;
    }

    if (dim.ny % 2 == 0) {
        y1 = dim.ny / 2 - 1;
        y2 = dim.ny / 2;
    }
    else {
        y1 = y2 = (dim.ny - 1) / 2;
    }

    if (dim.nz % 2 == 0) {
        z1 = dim.nz / 2 - 1;
        z2 = dim.nz / 2;
    }
    else {
        z1 = z2 = (dim.nz - 1) / 2;
    }

    for (int iX = 1; iX < dim.nx - 1; ++iX) {
        size_t i1 = lbm.xyz_to_i(iX, y1, z1);
        size_t i2 = lbm.xyz_to_i(iX, y2, z2);
        if (lbm.flag[i1] != CellType::bounce_back && lbm.flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.nx - 2);
            double pos = (static_cast<double>(iX) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lbm.lattice[i1]);
            auto [rho2, v2] = lbm.macro(lbm.lattice[i2]);
            middlex << setw(20) << setprecision(8) << pos
                    << setw(20) << setprecision(8) << 0.5 * (v1[0] + v2[0]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[1] + v2[1]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[2] + v2[2]) / ulb << "\n";
        }
    }

    for (int iY = 1; iY < dim.ny - 1; ++iY) {
        size_t i1 = lbm.xyz_to_i(x1, iY, z1);
        size_t i2 = lbm.xyz_to_i(x2, iY, z2);
        if (lbm.flag[i1] != CellType::bounce_back && lbm.flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.ny - 2);
            double pos = (static_cast<double>(iY) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lbm.lattice[i1]);
            auto [rho2, v2] = lbm.macro(lbm.lattice[i2]);
            middley << setw(20) << setprecision(8) << pos
                    << setw(20) << setprecision(8) << 0.5 * (v1[0] + v2[0]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[1] + v2[1]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[2] + v2[2]) / ulb << "\n";
        }
    }

    for (int iZ = 1; iZ < dim.nz - 1; ++iZ) {
        size_t i1 = lbm.xyz_to_i(x1, y1, iZ);
        size_t i2 = lbm.xyz_to_i(x2, y2, iZ);
        if (lbm.flag[i1] != CellType::bounce_back && lbm.flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.nz - 2);
            double pos = (static_cast<double>(iZ) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lbm.lattice[i1]);
            auto [rho2, v2] = lbm.macro(lbm.lattice[i2]);
            middlez << setw(20) << setprecision(8) << pos
                    << setw(20) << setprecision(8) << 0.5 * (v1[0] + v2[0]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[1] + v2[1]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[2] + v2[2]) / ulb << "\n";
        }
    }
}

// Compute time averages of the center-line velocity and pressure profiles and save
// the current version of the averages in a text file.
template<class LBM>
void saveProfiles(LBM& lbm, double ulb,
                  std::vector<std::array<double, 3>>& xprof,
                  std::vector<std::array<double, 3>>& yprof,
                  std::vector<std::array<double, 3>>& zprof, int num_profiles, bool writeToDisk )
{
    using namespace std;
    Dim const& dim = lbm.dim;
    ofstream *middlex = nullptr;
    ofstream *middley = nullptr;
    ofstream *middlez = nullptr;
    if (writeToDisk) {
        middlex = new ofstream("middlex.dat");
        middley = new ofstream("middley.dat");
        middlez = new ofstream("middlez.dat");
    }
    int x1, x2, y1, y2, z1, z2;

    if (dim.nx % 2 == 0) {
        x1 = dim.nx / 2 - 1;
        x2 = dim.nx / 2;
    }
    else {
        x1 = x2 = (dim.nx - 1) / 2;
    }

    if (dim.ny % 2 == 0) {
        y1 = dim.ny / 2 - 1;
        y2 = dim.ny / 2;
    }
    else {
        y1 = y2 = (dim.ny - 1) / 2;
    }

    if (dim.nz % 2 == 0) {
        z1 = dim.nz / 2 - 1;
        z2 = dim.nz / 2;
    }
    else {
        z1 = z2 = (dim.nz - 1) / 2;
    }

    for (int iX = 1; iX < dim.nx - 1; ++iX) {
        size_t i1 = lbm.xyz_to_i(iX, y1, z1);
        size_t i2 = lbm.xyz_to_i(iX, y2, z2);
        if (lbm.flag[i1] != CellType::bounce_back && lbm.flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.nx - 2);
            double pos = (static_cast<double>(iX) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lbm.lattice[i1]);
            auto [rho2, v2] = lbm.macro(lbm.lattice[i2]);
            double vx = 0.5 * (v1[0] + v2[0]) / ulb;
            double vy = 0.5 * (v1[1] + v2[1]) / ulb;
            double vz = 0.5 * (v1[2] + v2[2]) / ulb;
            xprof[iX - 1][0] += vx;
            xprof[iX - 1][1] += vy;
            xprof[iX - 1][2] += vz;
            vx = xprof[iX - 1][0] / static_cast<double>(num_profiles);
            vy = xprof[iX - 1][1] / static_cast<double>(num_profiles);
            vz = xprof[iX - 1][2] / static_cast<double>(num_profiles);
            if (writeToDisk) {
                *middlex << setw(20) << setprecision(8) << pos
                         << setw(20) << setprecision(8) << vx
                         << setw(20) << setprecision(8) << vy
                         << setw(20) << setprecision(8) << vz << "\n";
            }
        }
    }

    for (int iY = 1; iY < dim.ny - 1; ++iY) {
        size_t i1 = lbm.xyz_to_i(x1, iY, z1);
        size_t i2 = lbm.xyz_to_i(x2, iY, z2);
        if (lbm.flag[i1] != CellType::bounce_back && lbm.flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.ny - 2);
            double pos = (static_cast<double>(iY) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lbm.lattice[i1]);
            auto [rho2, v2] = lbm.macro(lbm.lattice[i2]);
            double vx = 0.5 * (v1[0] + v2[0]) / ulb;
            double vy = 0.5 * (v1[1] + v2[1]) / ulb;
            double vz = 0.5 * (v1[2] + v2[2]) / ulb;
            yprof[iY - 1][0] += vx;
            yprof[iY - 1][1] += vy;
            yprof[iY - 1][2] += vz;
            vx = yprof[iY - 1][0] / static_cast<double>(num_profiles);
            vy = yprof[iY - 1][1] / static_cast<double>(num_profiles);
            vz = yprof[iY - 1][2] / static_cast<double>(num_profiles);
            if (writeToDisk) {
                *middley << setw(20) << setprecision(8) << pos
                         << setw(20) << setprecision(8) << vx
                         << setw(20) << setprecision(8) << vy
                         << setw(20) << setprecision(8) << vz << "\n";
            }
        }
    }

    for (int iZ = 1; iZ < dim.nz - 1; ++iZ) {
        size_t i1 = lbm.xyz_to_i(x1, y1, iZ);
        size_t i2 = lbm.xyz_to_i(x2, y2, iZ);
        if (lbm.flag[i1] != CellType::bounce_back && lbm.flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.nz - 2);
            double pos = (static_cast<double>(iZ) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lbm.lattice[i1]);
            auto [rho2, v2] = lbm.macro(lbm.lattice[i2]);
            double vx = 0.5 * (v1[0] + v2[0]) / ulb;
            double vy = 0.5 * (v1[1] + v2[1]) / ulb;
            double vz = 0.5 * (v1[2] + v2[2]) / ulb;
            zprof[iZ - 1][0] += vx;
            zprof[iZ - 1][1] += vy;
            zprof[iZ - 1][2] += vz;
            vx = zprof[iZ - 1][0] / static_cast<double>(num_profiles);
            vy = zprof[iZ - 1][1] / static_cast<double>(num_profiles);
            vz = zprof[iZ - 1][2] / static_cast<double>(num_profiles);
            if (writeToDisk) {
                *middlez << setw(20) << setprecision(8) << pos
                         << setw(20) << setprecision(8) << vx
                         << setw(20) << setprecision(8) << vy
                         << setw(20) << setprecision(8) << vz << "\n";
            }
        }
    }
    delete middlex;
    delete middley;
    delete middlez;
}

// Parse a simple text configuration file for the provided keywords. Returns the obtained values
// and raises an exception in case of a mismatch (wrong type, missing keyword, extra keyword).
inline auto parse_configfile(std::set<std::string> double_param, std::set<std::string> int_param,
                             std::set<std::string> bool_param, std::set<std::string> string_param,
                             std::map<std::string, std::any> parameters)
{
    using namespace std;
    size_t nparam = double_param.size() + int_param.size() + bool_param.size() + string_param.size();

    ifstream ifile("config");
    if (!ifile) {
        throw invalid_argument("Config file not found. It should be name \"config\", without extension, and placed in the current directory.");
    }

    ifile.ignore(numeric_limits<streamsize>::max(), '\n');
    while (ifile) {
        string keyword;
        if (!(ifile >> keyword)) {
            break;
        }
        if (double_param.find(keyword) != double_param.end()) {
            double value;
            ifile >> value;
            parameters[keyword] = value;
        }
        else if (int_param.find(keyword) != int_param.end()) {
            int value;
            ifile >> value;
            parameters[keyword] = value;
        }
        else if (bool_param.find(keyword) != bool_param.end()) {
            bool value;
            ifile >> boolalpha >> value;
            parameters[keyword] = value;
        }
        else if (string_param.find(keyword) != string_param.end()) {
            string value;
            ifile >> value;
            parameters[keyword] = value;
        }
        else {
            throw invalid_argument("Wrong keyword in config file: " + keyword);
        }
        ifile.ignore(numeric_limits<streamsize>::max(), '\n');
    }
    if (parameters.size() != nparam) {
        throw invalid_argument("Insufficient number of arguments");
    }
    return parameters;
}

