# ******************************************************************************
# STLBM SOFTWARE LIBRARY

# Copyright © 2020 University of Geneva
# Authors: Jonas Latt, Christophe Coreixas, Joël Beny
# Contact: Jonas.Latt@unige.ch

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ******************************************************************************

cmake_minimum_required(VERSION 3.6)

include_directories(../educational_lbm ../optimized_lbm ../extended_collision_models)

file(GLOB EDUCATIONAL "../educational_lbm/*.h")
file(GLOB OPTIMIZED "../optimized_lbm/*.h")
file(GLOB EXTENDED "../extended_collision_models/*.h")

add_executable(cavity cavity.cpp ${EDUCATIONAL} ${OPTIMIZED} ${EXTENDED})
add_executable(porous porous.cpp ${EDUCATIONAL} ${OPTIMIZED} ${EXTENDED})
add_executable(cavity_hiRe cavity_hiRe.cpp ${EDUCATIONAL} ${OPTIMIZED} ${EXTENDED})
add_executable(selfcontained_cavity_twopop_soa selfcontained_cavity_twopop_soa.cpp)
add_executable(selfcontained_cavity_aa_soa selfcontained_cavity_aa_soa.cpp)
add_executable(selfcontained_cavity_swap_aos selfcontained_cavity_swap_aos.cpp)

#add_executable(cavity_q27 cavity_q27.cpp ${EDUCATIONAL} ${OPTIMIZED} ${EXTENDED})
#add_executable(selfcontained_twopop_soa_q27 selfcontained_twopop_soa_q27.cpp)
#add_executable(selfcontained_aa_soa_q27 selfcontained_aa_soa_q27.cpp)
#add_executable(selfcontained_swap_aos_q27 selfcontained_swap_aos_q27.cpp)

if (USE_TBB)
    target_link_libraries(cavity "tbb")
    target_link_libraries(porous "tbb")
    target_link_libraries(cavity_hiRe "tbb")
    target_link_libraries(selfcontained_cavity_twopop_soa "tbb")
    target_link_libraries(selfcontained_cavity_aa_soa "tbb")
    target_link_libraries(selfcontained_cavity_swap_aos "tbb")
    
    #target_link_libraries(cavity_q27 "tbb")
    #target_link_libraries(selfcontained_twopop_soa_q27 "tbb")
    #target_link_libraries(selfcontained_aa_soa_q27 "tbb")
    #target_link_libraries(selfcontained_swap_aos_q27 "tbb")
endif()

if(CMAKE_CUDA_COMPILER)
    add_executable(cuboltz_bench cuboltz_bench.cu)
endif()

