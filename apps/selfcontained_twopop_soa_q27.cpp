// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// This program implements a lid-driven 3D cavity flow.
// It is a self-contained code extract of the stlbm library, which runs the BGK
// collision model with the double-population structure-of-array scheme.
// The code is easy to adapt by copy-pasting other code portions from stlbm:
// - This code implements BGK (order-2), which you can replace by TRT by looking
//   up educational_lbm/twopop_soa.h, or other collision models by looking up
//   the directory extended_collision_models/
// - This code uses "educational" implementations of BGK, i.e. without aggressive
//   loop unrolling. You can apply aggressive loop unrolling by looking up
//   optimized_lbm/twopop_soa_bgk_unrolled.h
// - This code uses the double-population scheme and the structure-of-array
//   layout. To change this, replace the BGK class by another one found in
//   educational_lbm/, optimized_lbm/, or extended_collision_models/. If you move
//   away from the double-population scheme, you must also modify the "runCavity"
//   function: you're better off starting from one of the two other self-contained
//   codes.

#include <vector>
#include <array>
#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <execution>
#include <chrono>
#include <cmath>

using namespace std;
using namespace std::chrono;

double Re        = 100.;  // Reynolds number
double ulb       = 0.02;  // Velocity in lattice units
int N            = 128;   // Number of nodes in x-direction
bool benchmark   = true;  // Run in benchmark mode ?
double max_t     = 10.0;  // Non-benchmark mode: Total time in dim.less units
int out_freq  = 200;      // Non-benchmark mode: Frequency in LU for output of terminal message and profiles (use 0 for no messages)
int data_freq = 0;        // Non-benchmark mode: Frequency in LU of full data dump (use 0 for no data dump)
int bench_ini_iter = 1000; // Benchmark mode: Number of warmup iterations
int bench_max_iter = 2000; // Benchmark mode: Total number of iterations

enum class CellType : uint8_t { bounce_back, bulk };

inline auto d3q27_constants() {
    // The discrete velocities of the d3q27 mesh.
    vector<array<int, 3>> c_vect = {
        {-1, 0, 0}, { 0,-1, 0}, { 0, 0,-1},
        {-1,-1, 0}, {-1, 1, 0}, {-1, 0,-1},
        {-1, 0, 1}, { 0,-1,-1}, { 0,-1, 1},
        {-1,-1,-1}, {-1,-1, 1}, {-1, 1,-1}, {-1, 1, 1},
        { 0, 0, 0},
        { 1, 0, 0}, { 0, 1, 0}, { 0, 0, 1},
        { 1, 1, 0}, { 1,-1, 0}, { 1, 0, 1},
        { 1, 0,-1}, { 0, 1, 1}, { 0, 1,-1},
        { 1, 1, 1}, { 1, 1,-1}, { 1,-1, 1}, { 1,-1,-1}
    };

    // The opposite of a given direction.
    vector<int> opp_vect =
        { 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 
          13, 
           0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12 };
    // The lattice weights.
    vector<double> t_vect =
        {
          2./27., 2./27., 2./27., 1./54., 1./54., 1./54.,1./54., 1./54., 1./54.,
          1./216., 1./216., 1./216., 1./216.,
          8./27.,
          2./27., 2./27., 2./27., 1./54., 1./54., 1./54.,1./54., 1./54., 1./54.,
          1./216., 1./216., 1./216., 1./216.
        };
    return make_tuple(c_vect, opp_vect, t_vect);
}

// Stores the number of elements in a rectangular-shaped simulation.
struct Dim {
    Dim(int nx_, int ny_, int nz_)
        : nx(nx_), ny(ny_), nz(nz_),
          nelem(static_cast<size_t>(nx) * static_cast<size_t>(ny) * static_cast<size_t>(nz)),
          npop(27 * nelem)
    { }
    int nx, ny, nz;
    size_t nelem, npop;
};

// Compute lattice-unit variables and discrete space and time step for a given lattice
// velocity (acoustic scaling).
auto lbParameters(double ulb, int lref, double Re) {
    double nu = ulb * static_cast<double>(lref) / Re;
    double omega = 1. / (3. * nu + 0.5);
    double dx = 1. / static_cast<double>(lref);
    double dt = dx * ulb;
    return make_tuple(nu, omega, dx, dt);
}

// Print the simulation parameters to the terminal.
void printParameters(bool benchmark, double Re, double omega, double ulb, int N, double max_t) {
    cout << "Lid-driven 3D cavity, " << (benchmark ? "benchmark" : "production") << " mode" << endl;
    cout << "N = " << N << endl;
    cout << "Re = " << Re << endl;
    cout << "omega = " << omega << endl;
    cout << "ulb = " << ulb << endl;
    if (benchmark) {
        cout << "Now running " << bench_ini_iter << " warm-up iterations." << endl;
    }
    else {
        cout << "max_t = " << max_t << endl;
    }
}

// Return a new clock for the current time, for benchmarking.
auto restartClock() {
    return make_pair(high_resolution_clock::now(), 0);
}

// Compute the time elapsed since a starting point, and the corresponding
// performance of the code in Mega Lattice site updates per second (MLups).
template<class TimePoint>
void printMlups(TimePoint start, int clock_iter, size_t nelem) {
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    double mlups = static_cast<double>(nelem * clock_iter) / duration.count();

    cout << "Benchmark result: " << setprecision(4) << mlups << " MLUPS" << endl;
}

// Instances of this class are function objects: the function-call operator executes a
// collision-streaming cycle.
struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 2 * 27 * nelem; }

    CellData* lattice;
    CellType* flag;
    int* parity;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;

    // Convert linear index to Cartesian indices.
    auto i_to_xyz (int i) {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    // Convert Cartesian indices to linear index.
    size_t xyz_to_i (int x, int y, int z) {
        return z + dim.nz * (y + dim.ny * x);
    };

    // The "f" operator always refers to the population with parity 0, in which
    // the momentum-exchange term is stored.
    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    // Get the pre-collision populations of the current time step (their location depends on parity).
    double& fin (int i, int k) {
        return lattice[*parity * dim.npop + k * dim.nelem + i];
    }
    
    // Get the post-collision, streamed populations at the end of the current step.
    double& fout (int i, int k) {
        return lattice[(1 - *parity) * dim.npop + k * dim.nelem + i];
    }

    // Initialize the lattice with zero velocity and density 1.
    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 27; ++k) {
            fin(i, k) = t[k];
        }
    };

    // Compute the macroscopic variables density and velocity on a cell.
    auto macro (double& f0) {
        auto i = &f0 - lattice;
        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12);
        double X_P1 = fin(i,14) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26);
        double X_0  = fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,13) + fin(i,15) + fin(i,16) + fin(i,21) + fin(i,22);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,18) + fin(i,25) + fin(i,26);
        double Y_P1 = fin(i,15) + fin(i,17) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i, 4) + fin(i,11) + fin(i,12);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i, 9) + fin(i,11) + fin(i,20) + fin(i,22) + fin(i,24) + fin(i,26);
        double Z_P1 = fin(i,16) + fin(i,19) + fin(i,21) + fin(i,23) + fin(i,25) + fin(i, 6) + fin(i, 8) + fin(i,10) + fin(i,12);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return std::make_pair(rho, u);
    }

    // Execute the streaming step on a cell.
    void stream (int i, int k, int iX, int iY, int iZ, double pop_out) {
        int XX = iX + c[k][0];
        int YY = iY + c[k][1];
        int ZZ = iZ + c[k][2];
        size_t nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, opp[k]) = pop_out + f(nb, k);
        }
        else {
            fout(nb, k) = pop_out;
        }
    };

    // Execute second-order BGK collision on a cell.
    auto collideBgk(int i, int k, double rho, std::array<double, 3> const& u, double usqr) {
        double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
        double eq = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        double eqopp = eq - 6.* rho * t[k] * ck_u;
        double pop_out = (1. - omega) * fin(i, k) + omega * eq;
        double pop_out_opp = (1. - omega) * fin(i, opp[k]) + omega * eqopp;
        return std::make_pair(pop_out, pop_out_opp);
    }

    // Execute an iteration (collision and streaming) on a cell.
    void operator() (double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[rho, u] = macro(f0);
            auto[iX, iY, iZ] = i_to_xyz(i);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);

            for (int k = 0; k < 13; ++k) {
                auto[pop_out, pop_out_opp] = collideBgk(i, k, rho, u, usqr);

                stream(i, k, iX, iY, iZ, pop_out);
                stream(i, opp[k], iX, iY, iZ, pop_out_opp);
            }

            // Treat the case of the "rest-population" (c[k] = {0, 0, 0,}).
            for (int k: {13}) {
                double eq = rho * t[k] * (1. - usqr);
                fout(i, k) =  (1. - omega) * fin(i, k) + omega * eq;
            }
        }
    }
};

// Save the centerline velocity profiles into a text file to compare with reference values.
void saveProfiles(LBM& lbm, Dim dim, double ulb, typename LBM::CellData* lattice, CellType* flag)
{
    using namespace std;
    ofstream middlex("middlex.dat");
    ofstream middley("middley.dat");
    int x1 = dim.nx / 2, x2 = dim.nx / 2,
        y1 = dim.ny / 2, y2 = dim.ny / 2,
        z1 = dim.nz / 2, z2 = dim.nz / 2;

    if (dim.nx % 2 == 0) x1--;
    if (dim.ny % 2 == 0) y1--;
    if (dim.nz % 2 == 0) z1--;

    for (int iX = 1; iX < dim.nx - 1; ++iX) {
        size_t i1 = lbm.xyz_to_i(iX, y1, z1);
        size_t i2 = lbm.xyz_to_i(iX, y2, z2);
        if (flag[i1] != CellType::bounce_back && flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.nx - 2);
            double pos = (static_cast<double>(iX) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lattice[i1]);
            auto [rho2, v2] = lbm.macro(lattice[i2]);
            middlex << setw(20) << setprecision(8) << pos
                    << setw(20) << setprecision(8) << 0.5 * (v1[0] + v2[0]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[1] + v2[1]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[2] + v2[2]) / ulb << "\n";
        }
    }

    for (int iY = 1; iY < dim.ny - 1; ++iY) {
        size_t i1 = lbm.xyz_to_i(x1, iY, z1);
        size_t i2 = lbm.xyz_to_i(x2, iY, z2);
        if (flag[i1] != CellType::bounce_back && flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.ny - 2);
            double pos = (static_cast<double>(iY) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lattice[i1]);
            auto [rho2, v2] = lbm.macro(lattice[i2]);
            middley << setw(20) << setprecision(8) << pos
                    << setw(20) << setprecision(8) << 0.5 * (v1[0] + v2[0]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[1] + v2[1]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[2] + v2[2]) / ulb << "\n";
        }
    }
}

// Save the velocity and density into a text file to produce images in post-processing.
void saveSlice(LBM& lbm, Dim dim, typename LBM::CellData* lattice, CellType* flag)
{
    std::ofstream fvx("vx.dat");
    std::ofstream fvy("vy.dat");
    std::ofstream fvz("vz.dat");
    std::ofstream fv("v.dat");
    std::ofstream frho("rho.dat");
    for (int iX = dim.nx - 1; iX >= 0; --iX) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            size_t i = lbm.xyz_to_i(iX, iY, dim.nz / 2);
            auto [rho, v] = lbm.macro(lattice[i]);
            if (flag[i] == CellType::bounce_back) {
                rho = 1.0;
                v = { lbm.f(i, 0) / (6. * lbm.t[0]),
                      lbm.f(i, 1) / (6. * lbm.t[1]),
                      lbm.f(i, 2) / (6. * lbm.t[2]) };
            }
            fvx << v[0] << " ";
            fvy << v[1] << " ";
            fvz << v[2] << " ";
            fv << std::sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]) << " ";
            frho << rho << " ";

        }
        fvx << "\n";
        fvy << "\n";
        fvz << "\n";
        fv << "\n";
        frho << "\n";
    }
}

// Compute the average kinetic energy in the domain.
double computeEnergy(LBM& lbm, Dim const& dim, typename LBM::CellData* lattice, CellType* flag)
{
    double energy = 0.;
    for (int iX = 0; iX < dim.nx; ++iX) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            for (int iZ = 0; iZ < dim.nz; ++iZ) {
                size_t i = lbm.xyz_to_i(iX, iY, iZ);
                if (flag[i] != CellType::bounce_back) {
                    auto[rho, v] = lbm.macro(lattice[i]);
                    energy += v[0] * v[0] + v[1] * v[1] + v[2] * v[2];
                }
            }
        }
    }
    energy *= 0.5;
    return energy;
}

// Initializes the flag matrix and the populations to set up a cavity flow. In particular, the
// momentum-exchange term is assigned to the wall cells.
void iniCavity(LBM& lbm, Dim& dim, CellType* flag, double ulb, vector<double> const& ulid) {
    for (size_t i = 0; i < dim.nelem; ++i) {
        auto[iX, iY, iZ] = lbm.i_to_xyz(i);
        if (iX == 0 || iX == dim.nx-1 || iY == 0 || iY == dim.ny-1 || iZ == 0 || iZ == dim.nz-1) {
            flag[i] = CellType::bounce_back;
            if (iX == dim.nx-1) {
                for (int k = 0; k < 27; ++k) {
                    lbm.f(i, k) = - 6. * lbm.t[k] * ulb * (
                        lbm.c[k][0] * ulid[0] + lbm.c[k][1] * ulid[1] + lbm.c[k][2] * ulid[2] );
                }
            }
            else {
                for (int k = 0; k < 27; ++k) {
                    lbm.f(i, k) = 0.;
                }
            }
        }
        else {
            flag[i] = CellType::bulk;
        }
    }
}

// Runs a simulation of a flow in a lid-driven cavity using the two-population scheme.
void runCavityTwoPop(bool benchmark, double Re, double ulb, int N, double max_t)
{
    // CellData is either a double (structure-of-array) or an array<double, 27> (array-of-structure).
    using CellData = typename LBM::CellData; 
    vector<double> ulid {0., 1., 0.}; // Velocity on top lid in dimensionless units.

    Dim dim {N, N, N};

    auto[nu, omega, dx, dt] = lbParameters(ulb, N - 2, Re);
    printParameters(benchmark, Re, omega, ulb, N, max_t);

    vector<CellData> lattice_vect(LBM::sizeOfLattice(dim.nelem));
    CellData* lattice = &lattice_vect[0];

    // The "vector" is used as a convenient way to allocate the flag array on the heap.
    vector<CellType> flag_vect(dim.nelem);
    CellType* flag = &flag_vect[0];

    // The "vector" is used as a convenient way to allocate the integer value "parity"
    // on the heap. That's a necessity when the code is used on GPUs, because the
    // parity (which is 0/1, depending on whether the current time iteration is even/odd)
    // is modified on the host, and accessed on the device. The parity flag is used
    // to decide in which of the two population arrays the pre-collision LB variables are located.
    vector<int> parity_vect {0};
    int* parity = &parity_vect[0];

    // The lattice constants (discrete velocities, opposite indices, weghts) are mostly
    // used in the not-unrolled versions of the code. They must be allocated on the
    // heap so they can be shared with the device in case of a GPU execution.
    auto[c, opp, t] = d3q27_constants();
    // Instantiate the function object for the for_each call.
    LBM lbm{lattice, flag, parity, &c[0], &opp[0], &t[0], omega, dim};

    // Initialize the populations.
    for_each(lattice, lattice + dim.nelem, [&lbm](CellData& f0) { lbm.iniLattice(f0); });

    // Set up the geometry of the cavity.
    iniCavity(lbm, dim, flag, ulb, ulid);
    // Reset the clock, to be used when a benchmark simulation is executed.
    auto[start, clock_iter] = restartClock();

    // The average energy, dependent on time, can be used to monitor convergence, or statistical
    // convergence, of the simulation.
    ofstream energyfile("energy.dat");
    // Maximum number of time iterations depending on whether the simulation is in benchmark mode or production mode.
    int max_time_iter = benchmark ? bench_max_iter : static_cast<int>(max_t / dt);
    for (int time_iter = 0; time_iter < max_time_iter; ++time_iter) {
        if (benchmark && time_iter == bench_ini_iter)  {
            cout << "Now running " << bench_max_iter - bench_ini_iter
                 << " benchmark iterations." << endl;
            tie(start, clock_iter) = restartClock();
        }
        // If this option is chosen in the config file, save the raw populations in text files periodically.
        if (!benchmark && data_freq != 0 && time_iter % data_freq == 0 && time_iter > 0) {
            saveSlice(lbm, dim, lattice, flag);
        }
        // If this option is chosen in the config file, output some statistics and write the center-line
        // velocity profiles to a text file.
        if (!benchmark && out_freq != 0 && time_iter % out_freq == 0 && time_iter > 0) {
            cout << "Saving profiles at iteration " << time_iter
                 << ", t = " << setprecision(4) << time_iter * dt << setprecision(3)
                 << " [" << time_iter * dt / max_t * 100. << "%]" << endl;
            saveProfiles(lbm, dim, ulb, lattice, flag);
            double energy = computeEnergy(lbm, dim, lattice, flag) *dx*dx / (dt*dt);
            cout << "Average energy: " << setprecision(8) << energy << endl;
            energyfile << setw(10) << time_iter * dt << setw(16) << setprecision(8) << energy << endl;
        }

        // With the double-population scheme, collision and streaming are fused and executed in the following loop.
        for_each(execution::par_unseq, lattice, lattice + dim.nelem, lbm);
        // After a collision-streaming cycle, swap the parity for the next iteration.
        *parity = 1 - *parity;
        ++clock_iter;
    }

    if (benchmark) {
        printMlups(start, clock_iter, dim.nelem);
    }
}

int main() {
    runCavityTwoPop(benchmark, Re, ulb, N, max_t);
    return 0;
}
