// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// Definition of fundamental constants for LBM implementations.

#pragma once

#include <vector>
#include <array>
#include <tuple>
#include <map>
#include <string>
#include <cstdint>

enum class CellType : uint8_t { bounce_back, bulk };
enum class LBModel {
              bgk,       // BGK with a weighted second-order equilibrium
              trt,       // Two-relaxation time (adjusted for precise wall location)
              bgk_o4,    // BGK with a non-weighted fourth-order equilibrium
              rm,        // Multi-relaxation scheme based on raw-moment space
              hm,        // Multi-relaxation scheme based on hermite-moment space
              cm,        // Multi-relaxation scheme based on central-moment space
              chm,       // Multi-relaxation scheme based on central hermite-moment space
              k,         // Kumulants
              rr         // Multi-relaxation scheme based on Recursive-Regularized approach
};

enum class DataStructure { twopop_aos, twopop_soa, swap_aos, swap_soa, aa_aos, aa_soa, all };
inline std::map<std::string, DataStructure> stringToDataStructure()
{
    return { {"twopop_aos", DataStructure::twopop_aos},
             {"twopop_soa", DataStructure::twopop_soa},
             {"swap_aos", DataStructure::swap_aos},
             {"swap_soa", DataStructure::swap_soa},
             {"aa_aos", DataStructure::aa_aos},
             {"aa_soa", DataStructure::aa_soa},
             {"all", DataStructure::all} };
}

inline std::map<std::string, LBModel> stringToLBModel()
{
    return { {"bgk", LBModel::bgk},
             {"trt", LBModel::trt},
             {"bgk_o4", LBModel::bgk_o4},
             {"rm", LBModel::rm},
             {"hm", LBModel::hm},
             {"cm", LBModel::cm},
             {"chm", LBModel::chm},
             {"k", LBModel::k},
             {"rr", LBModel::rr} };
}

inline auto d3q19_constants() {
    // The discrete velocities of the d3q19 mesh.
    std::vector<std::array<int, 3>> c_vect = {
        {-1, 0, 0}, { 0,-1, 0}, { 0, 0,-1},
        {-1,-1, 0}, {-1, 1, 0}, {-1, 0,-1},
        {-1, 0, 1}, { 0,-1,-1}, { 0,-1, 1},
        { 0, 0, 0},
        { 1, 0, 0}, { 0, 1, 0}, { 0, 0, 1},
        { 1, 1, 0}, { 1,-1, 0}, { 1, 0, 1},
        { 1, 0,-1}, { 0, 1, 1}, { 0, 1,-1}
    };

    // The opposite of a given direction.
    std::vector<int> opp_vect =
        { 10, 11, 12, 13, 14, 15, 16, 17, 18, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8 };

    // The lattice weights.
    std::vector<double> t_vect =
        {
            1./18., 1./18., 1./18., 1./36., 1./36., 1./36., 1./36., 1./36., 1./36.,
            1./3.,
            1./18., 1./18., 1./18., 1./36., 1./36., 1./36., 1./36., 1./36., 1./36.
        };
    return std::make_tuple(c_vect, opp_vect, t_vect);
}

// Stores the number of elements in a rectangular-shaped simulation.
struct Dim {
    Dim(int nx_, int ny_, int nz_)
        : nx(nx_), ny(ny_), nz(nz_),
          nelem(static_cast<size_t>(nx) * static_cast<size_t>(ny) * static_cast<size_t>(nz)),
          npop(19 * nelem)
    { }
    int nx, ny, nz;
    size_t nelem, npop;
};

// Index ordering of moments. Same order as in E1.
enum {
    // Order 0
    M000 = 0,

    // Order 1
    M100 = 1,
    M010 = 2,
    M001 = 3,

    // Order 2
    M200 = 4,
    M020 = 5,
    M002 = 6,
    M110 = 7,
    M101 = 8,
    M011 = 9,

    // Order 3
    M210 = 10,
    M201 = 11,
    M021 = 12,
    M120 = 13,
    M102 = 14,
    M012 = 15,

    // Order 4
    M220 = 16,
    M202 = 17,
    M022 = 18,

};

enum {
    FM00 = 0,
    F0M0 = 1,
    F00M = 2,
    FMM0 = 3,
    FMP0 = 4,
    FM0M = 5,
    FM0P = 6,
    F0MM = 7,
    F0MP = 8,
    F000 = 9,
    FP00 = 10,
    F0P0 = 11,
    F00P = 12,
    FPP0 = 13,
    FPM0 = 14,
    FP0P = 15,
    FP0M = 16,
    F0PP = 17,
    F0PM = 18
};

