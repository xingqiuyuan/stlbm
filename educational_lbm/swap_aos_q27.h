// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// Implementation of swap scheme array-of-structure, for O2-BGK and TRT, without aggressive loop unrolling.

#pragma once

#include "lbm_q27.h"
#include <vector>
#include <array>
#include <tuple>

namespace swap_aos {

struct LBM {
    using CellData = std::array<double, 27>;
    static size_t sizeOfLattice(size_t nelem) { return nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;

    auto i_to_xyz (int i) {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[i][k];
    }

    CellData& cell(int i) {
        return lattice[i];
    }

    auto iniLattice (CellData& cell) {
        for (int k = 0; k < 27; ++k) {
            cell[k] = t[k];
        }
    };

    auto macro (CellData const& pop) {
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12];
        double X_P1 = pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26];
        double X_0  = pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[13] + pop[15] + pop[16] + pop[21] + pop[22];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[18] + pop[25] + pop[26];
        double Y_P1 = pop[15] + pop[17] + pop[21] + pop[22] + pop[23] + pop[24] + pop[ 4] + pop[11] + pop[12];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[ 9] + pop[11] + pop[20] + pop[22] + pop[24] + pop[26];
        double Z_P1 = pop[16] + pop[19] + pop[21] + pop[23] + pop[25] + pop[ 6] + pop[ 8] + pop[10] + pop[12];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };
};


struct Stream : public LBM {
    void operator() (CellData& cell) {
        auto i = &cell - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];
        for (int k = 0; k < 13; ++k) {
            int XX = (iX + c[k][0]);
            int YY = (iY + c[k][1]);
            int ZZ = (iZ + c[k][2]);
            if (XX >= 0 && XX < dim.nx && YY >= 0 && YY < dim.ny && ZZ >= 0 && ZZ < dim.nz) {
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                    f(nb, k) += cell[opp[k]];
                }
                if (cellType != CellType::bounce_back) {
                    if (nbCellType == CellType::bounce_back) {
                        cell[opp[k]] -= f(nb, k);
                    }
                    else {
                        std::swap(cell[opp[k]], f(nb, k));
                    }
                }
            }
        }
    }
};


struct Collide : public LBM {

    auto collideBgk(int i, int k, double rho, std::array<double, 3> const& u, double usqr) {
        double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
        double eq = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        double eqopp = eq - 6.* rho * t[k] * ck_u;
        double pop_out = (1. - omega) * f(i, k) + omega * eq;
        double pop_out_opp = (1. - omega) * f(i, opp[k]) + omega * eqopp;
        return std::make_pair(pop_out, pop_out_opp);
    }

    void iterateBgk(CellData& cell) {
        int i = &cell - lattice;
        if (flag[i] == CellType::bulk) {
            auto[rho, u] = macro(cell);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);

            for (int k = 0; k < 13; ++k) {
                auto[pop_out, pop_out_opp] = collideBgk(i, k, rho, u, usqr);
                cell[opp[k]] = pop_out;
                cell[k] = pop_out_opp;
            }

            for (int k: {13}) {
                double& pop = cell[k];
                double eq = rho * t[k] * (1. - usqr);
                pop =  (1. - omega) * pop + omega * eq;
            }
        }
    }

    // eq_minus and eq_plus are computed via macroscopic variables
    // f_minus is computed via f_plus
    auto collideTrt(int i, int k, double rho, std::array<double, 3> const& u, double usqr) {
        double s_plus = omega;
        double s_minus = 8. * (2. - s_plus) / (8. - s_plus);
        double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];

        double eq_plus = rho * t[k] * (1. + 4.5 * ck_u * ck_u - usqr);
        double eq_minus = rho * t[k] * 3. * ck_u;
        double pop_in = f(i, k);
        double pop_in_opp = f(i, opp[k]);
        double f_plus = 0.5 * (pop_in + pop_in_opp);
        double f_minus = f_plus - pop_in_opp;

        double pop_out = pop_in - s_plus * (f_plus- eq_plus) - s_minus * (f_minus - eq_minus);
        double pop_out_opp = pop_in_opp - s_plus * (f_plus - eq_plus) + s_minus * (f_minus - eq_minus);

        return std::make_pair(pop_out, pop_out_opp);
    }

    void iterateTrt(CellData& cell) {
        int i = &cell - lattice;
        if (flag[i] == CellType::bulk) {
            auto[rho, u] = macro(cell);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);

            for (int k = 0; k < 13; ++k) {
                auto[pop_out, pop_out_opp] = collideTrt(i, k, rho, u, usqr);
                cell[opp[k]] = pop_out;
                cell[k] = pop_out_opp;
            }

            for (int k: {13}) {
                double& pop = cell[k];
                double eq = rho * t[k] * (1. - usqr);
                pop =  (1. - omega) * pop + omega * eq;
            }
        }
    }

    void operator() (CellData& cell) {
        if (model == LBModel::bgk) {
            iterateBgk(cell);
        }
        else {
            iterateTrt(cell);
        }
    }
};

} // namespace swap_aos

