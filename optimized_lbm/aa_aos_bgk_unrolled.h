// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// Implementation of AA-pattern array-of-structure, for O2-BGK, with aggressive loop unrolling.

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>

namespace aa_aos_bgk_unrolled {

struct LBM {
    using CellData = std::array<double, 19>;
    static size_t sizeOfLattice(size_t nelem) { return nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;
    bool periodic = false;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[i][k];
    }

    CellData& cell(int i) {
        return lattice[i];
    }

    auto iniLattice (CellData& cell) {
        for (int k = 0; k < 19; ++k) {
            cell[k] = t[k];
        }
    };

    auto iniLattice (CellData& cell, double rho, std::array<double, 3> const& u) {
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 19; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            cell[k] = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    auto macro (CellData const& pop) {
        double X_M1 = pop[0] + pop[3] + pop[4] + pop[5] + pop[6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[9] + pop[1] + pop[2] + pop[7] + pop[8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[1] + pop[3] + pop[7] + pop[8] + pop[14];
        double Y_P1 = pop[4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[2] + pop[5] + pop[7] + pop[16] + pop[18];
        double Z_P1 = pop[6] + pop[8] + pop[12] + pop[15] + pop[17];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };

};

struct Even : public LBM {

    // Optimizations are based on: 
    // 1 - Computation of opposite equilibrium populations using symmetries
    // 2 - Precomputation of ck_u terms
    auto collideBgkUnrolled(CellData& cell, double rho, std::array<double, 3> const& u, double usqr) {
        double ck_u03 = u[0] + u[1];
        double ck_u04 = u[0] - u[1];
        double ck_u05 = u[0] + u[2];
        double ck_u06 = u[0] - u[2];
        double ck_u07 = u[1] + u[2];
        double ck_u08 = u[1] - u[2];

        double eq_00    = rho * t[ 0] * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr);
        double eq_01    = rho * t[ 1] * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr);
        double eq_02    = rho * t[ 2] * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr);
        double eq_03    = rho * t[ 3] * (1. - 3. * ck_u03 + 4.5 * ck_u03 * ck_u03 - usqr);
        double eq_04    = rho * t[ 4] * (1. - 3. * ck_u04 + 4.5 * ck_u04 * ck_u04 - usqr);
        double eq_05    = rho * t[ 5] * (1. - 3. * ck_u05 + 4.5 * ck_u05 * ck_u05 - usqr);
        double eq_06    = rho * t[ 6] * (1. - 3. * ck_u06 + 4.5 * ck_u06 * ck_u06 - usqr);
        double eq_07    = rho * t[ 7] * (1. - 3. * ck_u07 + 4.5 * ck_u07 * ck_u07 - usqr);
        double eq_08    = rho * t[ 8] * (1. - 3. * ck_u08 + 4.5 * ck_u08 * ck_u08 - usqr);
        double eq_09    = rho * t[ 9] * (1. - usqr);
        double eqopp_00 = eq_00 + rho * t[ 0] * 6. * u[0];
        double eqopp_01 = eq_01 + rho * t[ 1] * 6. * u[1];
        double eqopp_02 = eq_02 + rho * t[ 2] * 6. * u[2];
        double eqopp_03 = eq_03 + rho * t[ 3] * 6. * ck_u03;
        double eqopp_04 = eq_04 + rho * t[ 4] * 6. * ck_u04;
        double eqopp_05 = eq_05 + rho * t[ 5] * 6. * ck_u05;
        double eqopp_06 = eq_06 + rho * t[ 6] * 6. * ck_u06;
        double eqopp_07 = eq_07 + rho * t[ 7] * 6. * ck_u07;
        double eqopp_08 = eq_08 + rho * t[ 8] * 6. * ck_u08;

        double pop_out_00 = (1. - omega) * cell[ 0] + omega * eq_00;
        double pop_out_10 = (1. - omega) * cell[10] + omega * eqopp_00;
        cell[ 0] = pop_out_10;
        cell[10] = pop_out_00;

        double pop_out_01 = (1. - omega) * cell[ 1] + omega * eq_01;
        double pop_out_11 = (1. - omega) * cell[11] + omega * eqopp_01;
        cell[ 1] = pop_out_11;
        cell[11] = pop_out_01;

        double pop_out_02 = (1. - omega) * cell[ 2] + omega * eq_02;
        double pop_out_12 = (1. - omega) * cell[12] + omega * eqopp_02;
        cell[ 2] = pop_out_12;
        cell[12] = pop_out_02;

        double pop_out_03 = (1. - omega) * cell[ 3] + omega * eq_03;
        double pop_out_13 = (1. - omega) * cell[13] + omega * eqopp_03;
        cell[ 3] = pop_out_13;
        cell[13] = pop_out_03;

        double pop_out_04 = (1. - omega) * cell[ 4] + omega * eq_04;
        double pop_out_14 = (1. - omega) * cell[14] + omega * eqopp_04;
        cell[ 4] = pop_out_14;
        cell[14] = pop_out_04;

        double pop_out_05 = (1. - omega) * cell[ 5] + omega * eq_05;
        double pop_out_15 = (1. - omega) * cell[15] + omega * eqopp_05;
        cell[ 5] = pop_out_15;
        cell[15] = pop_out_05;

        double pop_out_06 = (1. - omega) * cell[ 6] + omega * eq_06;
        double pop_out_16 = (1. - omega) * cell[16] + omega * eqopp_06;
        cell[ 6] = pop_out_16;
        cell[16] = pop_out_06;

        double pop_out_07 = (1. - omega) * cell[ 7] + omega * eq_07;
        double pop_out_17 = (1. - omega) * cell[17] + omega * eqopp_07;
        cell[ 7] = pop_out_17;
        cell[17] = pop_out_07;

        double pop_out_08 = (1. - omega) * cell[ 8] + omega * eq_08;
        double pop_out_18 = (1. - omega) * cell[18] + omega * eqopp_08;
        cell[ 8] = pop_out_18;
        cell[18] = pop_out_08;

        cell[ 9] = (1. - omega) * cell[ 9] + omega * eq_09;
    }


    void iterateBgkUnrolled(CellData& cell) {
        int i = &cell - lattice;
        if (flag[i] == CellType::bulk) {
            auto[rho, u] = macro(cell);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);

            collideBgkUnrolled(cell, rho, u, usqr);
        }
    }

    void operator() (CellData& cell) {
        iterateBgkUnrolled(cell);
    }
};

struct Odd : public LBM {

    // Optimizations are based on: 
    // 1 - Computation of opposite equilibrium populations using symmetries
    // 2 - Precomputation of ck_u terms
    void collideBgkUnrolled(std::array<double, 19>& pop, double rho, std::array<double, 3> const& u, double usqr) {
        double ck_u03 = u[0] + u[1];
        double ck_u04 = u[0] - u[1];
        double ck_u05 = u[0] + u[2];
        double ck_u06 = u[0] - u[2];
        double ck_u07 = u[1] + u[2];
        double ck_u08 = u[1] - u[2];

        double eq_00    = rho * t[ 0] * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr);
        double eq_01    = rho * t[ 1] * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr);
        double eq_02    = rho * t[ 2] * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr);
        double eq_03    = rho * t[ 3] * (1. - 3. * ck_u03 + 4.5 * ck_u03 * ck_u03 - usqr);
        double eq_04    = rho * t[ 4] * (1. - 3. * ck_u04 + 4.5 * ck_u04 * ck_u04 - usqr);
        double eq_05    = rho * t[ 5] * (1. - 3. * ck_u05 + 4.5 * ck_u05 * ck_u05 - usqr);
        double eq_06    = rho * t[ 6] * (1. - 3. * ck_u06 + 4.5 * ck_u06 * ck_u06 - usqr);
        double eq_07    = rho * t[ 7] * (1. - 3. * ck_u07 + 4.5 * ck_u07 * ck_u07 - usqr);
        double eq_08    = rho * t[ 8] * (1. - 3. * ck_u08 + 4.5 * ck_u08 * ck_u08 - usqr);
        double eq_09    = rho * t[ 9] * (1. - usqr);
        double eqopp_00 = eq_00 + rho * t[ 0] * 6. * u[0];
        double eqopp_01 = eq_01 + rho * t[ 1] * 6. * u[1];
        double eqopp_02 = eq_02 + rho * t[ 2] * 6. * u[2];
        double eqopp_03 = eq_03 + rho * t[ 3] * 6. * ck_u03;
        double eqopp_04 = eq_04 + rho * t[ 4] * 6. * ck_u04;
        double eqopp_05 = eq_05 + rho * t[ 5] * 6. * ck_u05;
        double eqopp_06 = eq_06 + rho * t[ 6] * 6. * ck_u06;
        double eqopp_07 = eq_07 + rho * t[ 7] * 6. * ck_u07;
        double eqopp_08 = eq_08 + rho * t[ 8] * 6. * ck_u08;

        pop[ 0] = (1. - omega) * pop[ 0] + omega * eq_00;
        pop[10] = (1. - omega) * pop[10] + omega * eqopp_00;

        pop[ 1] = (1. - omega) * pop[ 1] + omega * eq_01;
        pop[11] = (1. - omega) * pop[11] + omega * eqopp_01;

        pop[ 2] = (1. - omega) * pop[ 2] + omega * eq_02;
        pop[12] = (1. - omega) * pop[12] + omega * eqopp_02;

        pop[ 3] = (1. - omega) * pop[ 3] + omega * eq_03;
        pop[13] = (1. - omega) * pop[13] + omega * eqopp_03;

        pop[ 4] = (1. - omega) * pop[ 4] + omega * eq_04;
        pop[14] = (1. - omega) * pop[14] + omega * eqopp_04;

        pop[ 5] = (1. - omega) * pop[ 5] + omega * eq_05;
        pop[15] = (1. - omega) * pop[15] + omega * eqopp_05;

        pop[ 6] = (1. - omega) * pop[ 6] + omega * eq_06;
        pop[16] = (1. - omega) * pop[16] + omega * eqopp_06;

        pop[ 7] = (1. - omega) * pop[ 7] + omega * eq_07;
        pop[17] = (1. - omega) * pop[17] + omega * eqopp_07;

        pop[ 8] = (1. - omega) * pop[ 8] + omega * eq_08;
        pop[18] = (1. - omega) * pop[18] + omega * eqopp_08;

        pop[9] = (1. - omega) * pop[9] + omega * eq_09;
    }

    void iterateBgkUnrolled(CellData& cell) {
        int i = &cell - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            CellData pop;
            if (periodic) {
                for (int k = 0; k < 19; ++k) {
                    int XX = (iX - c[k][0] + dim.nx) % dim.nx;
                    int YY = (iY - c[k][1] + dim.ny) % dim.ny;
                    int ZZ = (iZ - c[k][2] + dim.nz) % dim.nz;
                    size_t nb = xyz_to_i(XX, YY, ZZ);
                    CellType nbCellType = flag[nb];
                    if (nbCellType == CellType::bounce_back) {
                        pop[k] = cell[k] + f(nb, opp[k]);
                    }
                    else {
                        pop[k] = f(nb, opp[k]);
                    }
                }
            }
            else {
                for (int k = 0; k < 19; ++k) {
                    int XX = iX - c[k][0];
                    int YY = iY - c[k][1];
                    int ZZ = iZ - c[k][2];
                    size_t nb = xyz_to_i(XX, YY, ZZ);
                    CellType nbCellType = flag[nb];
                    if (nbCellType == CellType::bounce_back) {
                        pop[k] = cell[k] + f(nb, opp[k]);
                    }
                    else {
                        pop[k] = f(nb, opp[k]);
                    }
                }
            }

            auto[rho, u] = macro(pop);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);

            collideBgkUnrolled(pop, rho, u, usqr);

            if (periodic) {
                for (int k = 0; k < 19; ++k) {
                    int XX = (iX + c[k][0] + dim.nx) % dim.nx;
                    int YY = (iY + c[k][1] + dim.ny) % dim.ny;
                    int ZZ = (iZ + c[k][2] + dim.nz) % dim.nz;
                    size_t nb = xyz_to_i(XX, YY, ZZ);
                    CellType nbCellType = flag[nb];
                    if (nbCellType == CellType::bounce_back) {
                        cell[opp[k]] = pop[k] + f(nb, k);
                    }
                    else {
                        f(nb, k) = pop[k];
                    }
                }
            }
            else {
                for (int k = 0; k < 19; ++k) {
                    int XX = iX + c[k][0];
                    int YY = iY + c[k][1];
                    int ZZ = iZ + c[k][2];
                    size_t nb = xyz_to_i(XX, YY, ZZ);
                    CellType nbCellType = flag[nb];
                    if (nbCellType == CellType::bounce_back) {
                        cell[opp[k]] = pop[k] + f(nb, k);
                    }
                    else {
                        f(nb, k) = pop[k];
                    }
                }
            }
        }
    }

    void operator() (CellData& cell) {
        iterateBgkUnrolled(cell);
    }
};


} // namespace aa_aos_bgk_unrolled
