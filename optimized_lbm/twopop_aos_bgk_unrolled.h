// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// Implementation of double-population scheme array-of-structure, for O2-BGK, with aggressive loop unrolling.

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>

namespace twopop_aos_bgk_unrolled {

struct LBM {
    using CellData = std::array<double, 19>;
    static size_t sizeOfLattice(size_t nelem) { return 2 * nelem; }

    CellData* lattice;
    CellType* flag;
    int* parity;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;
    bool periodic = false;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[i][k];
    }

    CellData& fin (int i) {
        return lattice[*parity * dim.nelem + i];
    }
    
    CellData& fout (int i) {
        return lattice[(1 - *parity) * dim.nelem + i];
    }

    auto iniLattice (CellData& cell) {
        auto i = &cell - lattice;
        for (int k = 0; k < 19; ++k) {
            fin(i)[k] = t[k];
        }
    };

    auto iniLattice (CellData& cell, double rho, std::array<double, 3> const& u) {
        auto i = &cell - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 19; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            fin(i)[k] = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    auto macro (CellData const& cell) {
        auto i = &cell - lattice;
        CellData& pop = fin(i);
        double X_M1 = pop[0] + pop[3] + pop[4] + pop[5] + pop[6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[9] + pop[1] + pop[2] + pop[7] + pop[8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[1] + pop[3] + pop[7] + pop[8] + pop[14];
        double Y_P1 = pop[4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[2] + pop[5] + pop[7] + pop[16] + pop[18];
        double Z_P1 = pop[6] + pop[8] + pop[12] + pop[15] + pop[17];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };

    // Optimizations are based on: 
    // 1 - Computation of opposite equilibrium populations using symmetries
    // 2 - Precomputation of ck_u terms
    // 3 - Discarding zero-valued terms in indexes for the streaming step
    auto collideAndStreamBgkUnrolled(int i, int iX, int iY, int iZ, double rho, std::array<double, 3> const& u, double usqr) {
        double ck_u03 = u[0] + u[1];
        double ck_u04 = u[0] - u[1];
        double ck_u05 = u[0] + u[2];
        double ck_u06 = u[0] - u[2];
        double ck_u07 = u[1] + u[2];
        double ck_u08 = u[1] - u[2];

        double eq_00    = rho * t[ 0] * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr);
        double eq_01    = rho * t[ 1] * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr);
        double eq_02    = rho * t[ 2] * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr);
        double eq_03    = rho * t[ 3] * (1. - 3. * ck_u03 + 4.5 * ck_u03 * ck_u03 - usqr);
        double eq_04    = rho * t[ 4] * (1. - 3. * ck_u04 + 4.5 * ck_u04 * ck_u04 - usqr);
        double eq_05    = rho * t[ 5] * (1. - 3. * ck_u05 + 4.5 * ck_u05 * ck_u05 - usqr);
        double eq_06    = rho * t[ 6] * (1. - 3. * ck_u06 + 4.5 * ck_u06 * ck_u06 - usqr);
        double eq_07    = rho * t[ 7] * (1. - 3. * ck_u07 + 4.5 * ck_u07 * ck_u07 - usqr);
        double eq_08    = rho * t[ 8] * (1. - 3. * ck_u08 + 4.5 * ck_u08 * ck_u08 - usqr);
        double eq_09    = rho * t[ 9] * (1. - usqr);
        double eqopp_00 = eq_00 + rho * t[ 0] * 6. * u[0];
        double eqopp_01 = eq_01 + rho * t[ 1] * 6. * u[1];
        double eqopp_02 = eq_02 + rho * t[ 2] * 6. * u[2];
        double eqopp_03 = eq_03 + rho * t[ 3] * 6. * ck_u03;
        double eqopp_04 = eq_04 + rho * t[ 4] * 6. * ck_u04;
        double eqopp_05 = eq_05 + rho * t[ 5] * 6. * ck_u05;
        double eqopp_06 = eq_06 + rho * t[ 6] * 6. * ck_u06;
        double eqopp_07 = eq_07 + rho * t[ 7] * 6. * ck_u07;
        double eqopp_08 = eq_08 + rho * t[ 8] * 6. * ck_u08;

        CellData const& fi_in = fin(i);
        double pop_out_00 = (1. - omega) * fi_in[0] + omega * eq_00;
        double pop_out_01 = (1. - omega) * fi_in[1] + omega * eq_01;
        double pop_out_02 = (1. - omega) * fi_in[2] + omega * eq_02;
        double pop_out_03 = (1. - omega) * fi_in[3] + omega * eq_03;
        double pop_out_04 = (1. - omega) * fi_in[4] + omega * eq_04;
        double pop_out_05 = (1. - omega) * fi_in[5] + omega * eq_05;
        double pop_out_06 = (1. - omega) * fi_in[6] + omega * eq_06;
        double pop_out_07 = (1. - omega) * fi_in[7] + omega * eq_07;
        double pop_out_08 = (1. - omega) * fi_in[8] + omega * eq_08;

        double pop_out_opp_00 = (1. - omega) * fi_in[10] + omega * eqopp_00;
        double pop_out_opp_01 = (1. - omega) * fi_in[11] + omega * eqopp_01;
        double pop_out_opp_02 = (1. - omega) * fi_in[12] + omega * eqopp_02;
        double pop_out_opp_03 = (1. - omega) * fi_in[13] + omega * eqopp_03;
        double pop_out_opp_04 = (1. - omega) * fi_in[14] + omega * eqopp_04;
        double pop_out_opp_05 = (1. - omega) * fi_in[15] + omega * eqopp_05;
        double pop_out_opp_06 = (1. - omega) * fi_in[16] + omega * eqopp_06;
        double pop_out_opp_07 = (1. - omega) * fi_in[17] + omega * eqopp_07;
        double pop_out_opp_08 = (1. - omega) * fi_in[18] + omega * eqopp_08;

        double pop_out_09 = (1. - omega) * fi_in[9] + omega * eq_09;

        // Unrolled streaming step
        CellData& fi_out = fout(i);
        int XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        int YY = iY;
        int ZZ = iZ;
        size_t nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[10] = pop_out_00 + f(nb, 0);
        }
        else {
            fout(nb)[0] = pop_out_00;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = iY;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[0] = pop_out_opp_00 + f(nb, 10);
        }
        else {
            fout(nb)[10] = pop_out_opp_00;
        }

        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[11] = pop_out_01 + f(nb, 1);
        }
        else {
            fout(nb)[1] = pop_out_01;
        }

        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[1] = pop_out_opp_01 + f(nb, 11);
        }
        else {
            fout(nb)[11] = pop_out_opp_01;
        }

        XX = iX;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[12] = pop_out_02 + f(nb, 2);
        }
        else {
            fout(nb)[2] = pop_out_02;
        }

        XX = iX;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[2] = pop_out_opp_02 + f(nb, 12);
        }
        else {
            fout(nb)[12] = pop_out_opp_02;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[13] = pop_out_03 + f(nb, 3);
        }
        else {
            fout(nb)[3] = pop_out_03;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[3] = pop_out_opp_03 + f(nb, 13);
        }
        else {
            fout(nb)[13] = pop_out_opp_03;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[14] = pop_out_04 + f(nb, 4);
        }
        else {
            fout(nb)[4] = pop_out_04;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[4] = pop_out_opp_04 + f(nb, 14);
        }
        else {
            fout(nb)[14] = pop_out_opp_04;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[15] = pop_out_05 + f(nb, 5);
        }
        else {
            fout(nb)[5] = pop_out_05;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[5] = pop_out_opp_05 + f(nb, 15);
        }
        else {
            fout(nb)[15] = pop_out_opp_05;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[16] = pop_out_06 + f(nb, 6);
        }
        else {
            fout(nb)[6] = pop_out_06;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[6] = pop_out_opp_06 + f(nb, 16);
        }
        else {
            fout(nb)[16] = pop_out_opp_06;
        }

        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[17] = pop_out_07 + f(nb, 7);
        }
        else {
            fout(nb)[7] = pop_out_07;
        }

        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[7] = pop_out_opp_07 + f(nb, 17);
        }
        else {
            fout(nb)[17] = pop_out_opp_07;
        }

        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[18] = pop_out_08 + f(nb, 8);
        }
        else {
            fout(nb)[8] = pop_out_08;
        }

        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fi_out[8] = pop_out_opp_08 + f(nb, 18);
        }
        else {
            fout(nb)[18] = pop_out_opp_08;
        }


        fi_out[9] =  pop_out_09;
    }


    void iterateBgkUnrolled(CellData& cell) {
        int i = &cell - lattice;
        if (flag[i] == CellType::bulk) {
            auto[rho, u] = macro(cell);
            auto[iX, iY, iZ] = i_to_xyz(i);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
            collideAndStreamBgkUnrolled(i, iX, iY, iZ, rho, u, usqr);
        }
    }

    void operator() (CellData& cell) {
        iterateBgkUnrolled(cell);
    }
};

} // namespace twopop_aos_bgk_unrolled
