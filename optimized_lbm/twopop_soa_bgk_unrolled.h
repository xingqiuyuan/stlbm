// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// Implementation of double-population scheme structure-of-array, for O2-BGK, with aggressive loop unrolling.

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>

namespace twopop_soa_bgk_unrolled {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 2 * 19 * nelem; }

    CellData* lattice;
    CellType* flag;
    int* parity;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;
    bool periodic = false;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    double& fin (int i, int k) {
        return lattice[*parity * dim.npop + k * dim.nelem + i];
    }
    
    double& fout (int i, int k) {
        return lattice[(1 - *parity) * dim.npop + k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 19; ++k) {
            fin(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 19; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6);
        double X_P1 = fin(i, 10) + fin(i, 13) + fin(i, 14) + fin(i, 15) + fin(i, 16);
        double X_0  = fin(i, 9) + fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i, 11) + fin(i, 12) + fin(i, 17) + fin(i, 18);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i, 14);
        double Y_P1 = fin(i, 4) + fin(i, 11) + fin(i, 13) + fin(i, 17) + fin(i, 18);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i, 16) + fin(i, 18);
        double Z_P1 = fin(i, 6) + fin(i, 8) + fin(i, 12) + fin(i, 15) + fin(i, 17);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return std::make_pair(rho, u);
    }

    // Optimizations are based on: 
    // 1 - Computation of opposite equilibrium populations using symmetries
    // 2 - Precomputation of ck_u terms
    // 3 - Discarding zero-valued terms in indexes for the streaming step  
    auto collideAndStreamBgkUnrolled(int i, int iX, int iY, int iZ, double rho, std::array<double, 3> const& u, double usqr) {
        double ck_u03 = u[0] + u[1];
        double ck_u04 = u[0] - u[1];
        double ck_u05 = u[0] + u[2];
        double ck_u06 = u[0] - u[2];
        double ck_u07 = u[1] + u[2];
        double ck_u08 = u[1] - u[2];

        double eq_00    = rho * t[ 0] * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr);
        double eq_01    = rho * t[ 1] * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr);
        double eq_02    = rho * t[ 2] * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr);
        double eq_03    = rho * t[ 3] * (1. - 3. * ck_u03 + 4.5 * ck_u03 * ck_u03 - usqr);
        double eq_04    = rho * t[ 4] * (1. - 3. * ck_u04 + 4.5 * ck_u04 * ck_u04 - usqr);
        double eq_05    = rho * t[ 5] * (1. - 3. * ck_u05 + 4.5 * ck_u05 * ck_u05 - usqr);
        double eq_06    = rho * t[ 6] * (1. - 3. * ck_u06 + 4.5 * ck_u06 * ck_u06 - usqr);
        double eq_07    = rho * t[ 7] * (1. - 3. * ck_u07 + 4.5 * ck_u07 * ck_u07 - usqr);
        double eq_08    = rho * t[ 8] * (1. - 3. * ck_u08 + 4.5 * ck_u08 * ck_u08 - usqr);
        double eq_09    = rho * t[ 9] * (1. - usqr);
        double eqopp_00 = eq_00 + rho * t[ 0] * 6. * u[0];
        double eqopp_01 = eq_01 + rho * t[ 1] * 6. * u[1];
        double eqopp_02 = eq_02 + rho * t[ 2] * 6. * u[2];
        double eqopp_03 = eq_03 + rho * t[ 3] * 6. * ck_u03;
        double eqopp_04 = eq_04 + rho * t[ 4] * 6. * ck_u04;
        double eqopp_05 = eq_05 + rho * t[ 5] * 6. * ck_u05;
        double eqopp_06 = eq_06 + rho * t[ 6] * 6. * ck_u06;
        double eqopp_07 = eq_07 + rho * t[ 7] * 6. * ck_u07;
        double eqopp_08 = eq_08 + rho * t[ 8] * 6. * ck_u08;

        double pop_out_00 = (1. - omega) * fin(i, 0) + omega * eq_00;
        double pop_out_01 = (1. - omega) * fin(i, 1) + omega * eq_01;
        double pop_out_02 = (1. - omega) * fin(i, 2) + omega * eq_02;
        double pop_out_03 = (1. - omega) * fin(i, 3) + omega * eq_03;
        double pop_out_04 = (1. - omega) * fin(i, 4) + omega * eq_04;
        double pop_out_05 = (1. - omega) * fin(i, 5) + omega * eq_05;
        double pop_out_06 = (1. - omega) * fin(i, 6) + omega * eq_06;
        double pop_out_07 = (1. - omega) * fin(i, 7) + omega * eq_07;
        double pop_out_08 = (1. - omega) * fin(i, 8) + omega * eq_08;

        double pop_out_opp_00 = (1. - omega) * fin(i, 10) + omega * eqopp_00;
        double pop_out_opp_01 = (1. - omega) * fin(i, 11) + omega * eqopp_01;
        double pop_out_opp_02 = (1. - omega) * fin(i, 12) + omega * eqopp_02;
        double pop_out_opp_03 = (1. - omega) * fin(i, 13) + omega * eqopp_03;
        double pop_out_opp_04 = (1. - omega) * fin(i, 14) + omega * eqopp_04;
        double pop_out_opp_05 = (1. - omega) * fin(i, 15) + omega * eqopp_05;
        double pop_out_opp_06 = (1. - omega) * fin(i, 16) + omega * eqopp_06;
        double pop_out_opp_07 = (1. - omega) * fin(i, 17) + omega * eqopp_07;
        double pop_out_opp_08 = (1. - omega) * fin(i, 18) + omega * eqopp_08;

        double pop_out_09 = (1. - omega) * fin(i, 9) + omega * eq_09;

        // Unrolled streaming step
        int XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        int YY = iY;
        int ZZ = iZ;
        size_t nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 10) = pop_out_00 + f(nb, 0);
        }
        else {
            fout(nb, 0) = pop_out_00;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = iY;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 0) = pop_out_opp_00 + f(nb,10);
        }
        else {
            fout(nb,10) = pop_out_opp_00;
        }

        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 11) = pop_out_01 + f(nb, 1);
        }
        else {
            fout(nb, 1) = pop_out_01;
        }

        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 1) = pop_out_opp_01 + f(nb,11);
        }
        else {
            fout(nb,11) = pop_out_opp_01;
        }

        XX = iX;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 12) = pop_out_02 + f(nb, 2);
        }
        else {
            fout(nb, 2) = pop_out_02;
        }

        XX = iX;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 2) = pop_out_opp_02 + f(nb,12);
        }
        else {
            fout(nb,12) = pop_out_opp_02;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 13) = pop_out_03 + f(nb, 3);
        }
        else {
            fout(nb, 3) = pop_out_03;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 3) = pop_out_opp_03 + f(nb,13);
        }
        else {
            fout(nb,13) = pop_out_opp_03;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 14) = pop_out_04 + f(nb, 4);
        }
        else {
            fout(nb, 4) = pop_out_04;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 4) = pop_out_opp_04 + f(nb,14);
        }
        else {
            fout(nb,14) = pop_out_opp_04;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 15) = pop_out_05 + f(nb, 5);
        }
        else {
            fout(nb, 5) = pop_out_05;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 5) = pop_out_opp_05 + f(nb,15);
        }
        else {
            fout(nb,15) = pop_out_opp_05;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 16) = pop_out_06 + f(nb, 6);
        }
        else {
            fout(nb, 6) = pop_out_06;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 6) = pop_out_opp_06 + f(nb,16);
        }
        else {
            fout(nb,16) = pop_out_opp_06;
        }

        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 17) = pop_out_07 + f(nb, 7);
        }
        else {
            fout(nb, 7) = pop_out_07;
        }

        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 7) = pop_out_opp_07 + f(nb,17);
        }
        else {
            fout(nb,17) = pop_out_opp_07;
        }

        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 18) = pop_out_08 + f(nb, 8);
        }
        else {
            fout(nb, 8) = pop_out_08;
        }

        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 8) = pop_out_opp_08 + f(nb,18);
        }
        else {
            fout(nb,18) = pop_out_opp_08;
        }


        fout(i, 9) =  pop_out_09;
    }


    void iterateBgkUnrolled(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[rho, u] = macro(f0);
            auto[iX, iY, iZ] = i_to_xyz(i);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
            collideAndStreamBgkUnrolled(i, iX, iY, iZ, rho, u, usqr);
        }
    }

    void operator() (double& f0) {
        iterateBgkUnrolled(f0);
    }
};

} // namespace twopop_soa_bgk_unrolled
