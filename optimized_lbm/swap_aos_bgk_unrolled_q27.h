// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// Implementation of swap scheme array-of-structure, for O2-BGK, with aggressive loop unrolling.

#pragma once

#include "lbm_q27.h"
#include <vector>
#include <array>
#include <tuple>

namespace swap_aos_bgk_unrolled {

struct LBM {
    using CellData = std::array<double, 27>;
    static size_t sizeOfLattice(size_t nelem) { return nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;

    auto i_to_xyz (int i) {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[i][k];
    }

    CellData& cell(int i) {
        return lattice[i];
    }

    auto iniLattice (CellData& cell) {
        for (int k = 0; k < 27; ++k) {
            cell[k] = t[k];
        }
    };

    auto macro (CellData const& pop) {
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6] + pop[ 9] + pop[10] + pop[11] + pop[12];
        double X_P1 = pop[14] + pop[17] + pop[18] + pop[19] + pop[20] + pop[23] + pop[24] + pop[25] + pop[26];
        double X_0  = pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[13] + pop[15] + pop[16] + pop[21] + pop[22];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[ 9] + pop[10] + pop[18] + pop[25] + pop[26];
        double Y_P1 = pop[15] + pop[17] + pop[21] + pop[22] + pop[23] + pop[24] + pop[ 4] + pop[11] + pop[12];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[ 9] + pop[11] + pop[20] + pop[22] + pop[24] + pop[26];
        double Z_P1 = pop[16] + pop[19] + pop[21] + pop[23] + pop[25] + pop[ 6] + pop[ 8] + pop[10] + pop[12];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };
};


struct Stream : public LBM {
    void operator() (CellData& cell) {
        auto i = &cell - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        int XX = iX - 1;
        int YY = iY;
        int ZZ = iZ;
        if (XX >= 0) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 0) += cell[14];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[14] -= f(nb, 0);
                }
                else {
                    std::swap(cell[14], f(nb, 0));
                }
            }
        }

        XX = iX;
        YY = iY - 1;
        ZZ = iZ;
        if (YY >= 0) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 1) += cell[15];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[15] -= f(nb, 1);
                }
                else {
                    std::swap(cell[15], f(nb, 1));
                }
            }
        }

        XX = iX;
        YY = iY;
        ZZ = iZ - 1;
        if (ZZ >= 0) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 2) += cell[16];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[16] -= f(nb, 2);
                }
                else {
                    std::swap(cell[16], f(nb, 2));
                }
            }
        }

        XX = iX - 1;
        YY = iY - 1;
        ZZ = iZ;
        if (XX >= 0 && YY >= 0) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 3) += cell[17];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[17] -= f(nb, 3);
                }
                else {
                    std::swap(cell[17], f(nb, 3));
                }
            }
        }

        XX = iX - 1;
        YY = iY + 1;
        ZZ = iZ;
        if (XX >= 0 && YY < dim.ny) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 4) += cell[18];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[18] -= f(nb, 4);
                }
                else {
                    std::swap(cell[18], f(nb, 4));
                }
            }
        }

        XX = iX - 1;
        YY = iY;
        ZZ = iZ - 1;
        if (XX >= 0 && ZZ >= 0) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 5) += cell[19];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[19] -= f(nb, 5);
                }
                else {
                    std::swap(cell[19], f(nb, 5));
                }
            }
        }

        XX = iX - 1;
        YY = iY;
        ZZ = iZ + 1;
        if (XX >= 0 && ZZ < dim.nz) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 6) += cell[20];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[20] -= f(nb, 6);
                }
                else {
                    std::swap(cell[20], f(nb, 6));
                }
            }
        }

        XX = iX;
        YY = iY - 1;
        ZZ = iZ - 1;
        if (YY >= 0 && ZZ >= 0) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 7) += cell[21];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[21] -= f(nb, 7);
                }
                else {
                    std::swap(cell[21], f(nb, 7));
                }
            }
        }

        XX = iX;
        YY = iY - 1;
        ZZ = iZ + 1;
        if (YY >= 0 && ZZ < dim.nz) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 8) += cell[22];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[22] -= f(nb, 8);
                }
                else {
                    std::swap(cell[22], f(nb, 8));
                }
            }
        }

        XX = iX - 1;
        YY = iY - 1;
        ZZ = iZ - 1;
        if (YY >= 0 && ZZ < dim.nz) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 9) += cell[23];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[23] -= f(nb, 9);
                }
                else {
                    std::swap(cell[23], f(nb, 9));
                }
            }
        }

        XX = iX - 1;
        YY = iY - 1;
        ZZ = iZ + 1;
        if (YY >= 0 && ZZ < dim.nz) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb,10) += cell[24];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[24] -= f(nb,10);
                }
                else {
                    std::swap(cell[24], f(nb,10));
                }
            }
        }

        XX = iX - 1;
        YY = iY + 1;
        ZZ = iZ - 1;
        if (YY >= 0 && ZZ < dim.nz) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb,11) += cell[25];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[25] -= f(nb,11);
                }
                else {
                    std::swap(cell[25], f(nb,11));
                }
            }
        }

        XX = iX - 1;
        YY = iY + 1;
        ZZ = iZ + 1;
        if (YY >= 0 && ZZ < dim.nz) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb,12) += cell[26];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[26] -= f(nb,12);
                }
                else {
                    std::swap(cell[26], f(nb,12));
                }
            }
        }

    }
};


struct Collide : public LBM {

    // Optimizations are based on: 
    // 1 - Computation of opposite equilibrium populations using symmetries
    // 2 - Precomputation of cku terms
    auto collideBgkUnrolled(int i, double rho, std::array<double, 3> const& u, double usqr) {
        double cku1 = u[0] + u[1];
        double cku2 =-u[0] + u[1];
        double cku3 = u[0] + u[2];
        double cku4 =-u[0] + u[2];
        double cku5 = u[1] + u[2];
        double cku6 =-u[1] + u[2];
        double cku7 = u[0] + u[1] + u[2];
        double cku8 =-u[0] + u[1] + u[2];
        double cku9 = u[0] - u[1] + u[2];
        double cku0 = u[0] + u[1] - u[2];

        std::array<double, 27> feqRM;

        feqRM[F000] = rho * t[F000] * (1.                                 - usqr);

        feqRM[FM00] = rho * t[FM00] * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr);
        feqRM[FP00] = rho * t[FP00] * (     6. * u[0] ) + feqRM[FM00];
        
        feqRM[F0M0] = rho * t[F0M0] * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr);
        feqRM[F0P0] = rho * t[F0P0] * (     6. * u[1] ) + feqRM[F0M0];
        
        feqRM[F00M] = rho * t[F00M] * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr);
        feqRM[F00P] = rho * t[F00P] * (     6. * u[2] ) + feqRM[F00M];

        feqRM[FMM0] = rho * t[FMM0] * (1. - 3. * cku1 + 4.5 * cku1 * cku1 - usqr);
        feqRM[FPP0] = rho * t[FPP0] * (     6. * cku1 ) + feqRM[FMM0];
        feqRM[FPM0] = rho * t[FPM0] * (1. - 3. * cku2 + 4.5 * cku2 * cku2 - usqr);
        feqRM[FMP0] = rho * t[FMP0] * (     6. * cku2 ) + feqRM[FPM0];

        feqRM[FM0M] = rho * t[FM0M] * (1. - 3. * cku3 + 4.5 * cku3 * cku3 - usqr);
        feqRM[FP0P] = rho * t[FP0P] * (     6. * cku3 ) + feqRM[FM0M];
        feqRM[FP0M] = rho * t[FP0M] * (1. - 3. * cku4 + 4.5 * cku4 * cku4 - usqr);
        feqRM[FM0P] = rho * t[FM0P] * (     6. * cku4 ) + feqRM[FP0M];

        feqRM[F0MM] = rho * t[F0MM] * (1. - 3. * cku5 + 4.5 * cku5 * cku5 - usqr);
        feqRM[F0PP] = rho * t[F0PP] * (     6. * cku5 ) + feqRM[F0MM];
        feqRM[F0PM] = rho * t[F0PM] * (1. - 3. * cku6 + 4.5 * cku6 * cku6 - usqr);
        feqRM[F0MP] = rho * t[F0MP] * (     6. * cku6 ) + feqRM[F0PM];

        feqRM[FMMM] = rho * t[FMMM] * (1. - 3. * cku7 + 4.5 * cku7 * cku7 - usqr);
        feqRM[FPPP] = rho * t[FPPP] * (     6. * cku7 ) + feqRM[FMMM];
        feqRM[FPMM] = rho * t[FPMM] * (1. - 3. * cku8 + 4.5 * cku8 * cku8 - usqr);
        feqRM[FMPP] = rho * t[FMPP] * (     6. * cku8 ) + feqRM[FPMM];
        feqRM[FMPM] = rho * t[FMPM] * (1. - 3. * cku9 + 4.5 * cku9 * cku9 - usqr);
        feqRM[FPMP] = rho * t[FPMP] * (     6. * cku9 ) + feqRM[FMPM];
        feqRM[FMMP] = rho * t[FMMP] * (1. - 3. * cku0 + 4.5 * cku0 * cku0 - usqr);
        feqRM[FPPM] = rho * t[FPPM] * (     6. * cku0 ) + feqRM[FMMP];

        // BGK Collision based on the second-order equilibrium
        std::array<double, 27> foutRM;
        CellData& cell_i = cell(i);

        foutRM[F000] = (1. - omega) * cell_i[F000] + omega * feqRM[F000];

        foutRM[FP00] = (1. - omega) * cell_i[FP00] + omega * feqRM[FP00];
        foutRM[FM00] = (1. - omega) * cell_i[FM00] + omega * feqRM[FM00];

        foutRM[F0P0] = (1. - omega) * cell_i[F0P0] + omega * feqRM[F0P0];
        foutRM[F0M0] = (1. - omega) * cell_i[F0M0] + omega * feqRM[F0M0];

        foutRM[F00P] = (1. - omega) * cell_i[F00P] + omega * feqRM[F00P];
        foutRM[F00M] = (1. - omega) * cell_i[F00M] + omega * feqRM[F00M];

        foutRM[FPP0] = (1. - omega) * cell_i[FPP0] + omega * feqRM[FPP0];
        foutRM[FMP0] = (1. - omega) * cell_i[FMP0] + omega * feqRM[FMP0];
        foutRM[FPM0] = (1. - omega) * cell_i[FPM0] + omega * feqRM[FPM0];
        foutRM[FMM0] = (1. - omega) * cell_i[FMM0] + omega * feqRM[FMM0];

        foutRM[FP0P] = (1. - omega) * cell_i[FP0P] + omega * feqRM[FP0P];
        foutRM[FM0P] = (1. - omega) * cell_i[FM0P] + omega * feqRM[FM0P];
        foutRM[FP0M] = (1. - omega) * cell_i[FP0M] + omega * feqRM[FP0M];
        foutRM[FM0M] = (1. - omega) * cell_i[FM0M] + omega * feqRM[FM0M];

        foutRM[F0PP] = (1. - omega) * cell_i[F0PP] + omega * feqRM[F0PP];
        foutRM[F0MP] = (1. - omega) * cell_i[F0MP] + omega * feqRM[F0MP];
        foutRM[F0PM] = (1. - omega) * cell_i[F0PM] + omega * feqRM[F0PM];
        foutRM[F0MM] = (1. - omega) * cell_i[F0MM] + omega * feqRM[F0MM];

        foutRM[FPPP] = (1. - omega) * cell_i[FPPP] + omega * feqRM[FPPP];
        foutRM[FMPP] = (1. - omega) * cell_i[FMPP] + omega * feqRM[FMPP];
        foutRM[FPMP] = (1. - omega) * cell_i[FPMP] + omega * feqRM[FPMP];
        foutRM[FPPM] = (1. - omega) * cell_i[FPPM] + omega * feqRM[FPPM];
        foutRM[FMMP] = (1. - omega) * cell_i[FMMP] + omega * feqRM[FMMP];
        foutRM[FMPM] = (1. - omega) * cell_i[FMPM] + omega * feqRM[FMPM];
        foutRM[FPMM] = (1. - omega) * cell_i[FPMM] + omega * feqRM[FPMM];
        foutRM[FMMM] = (1. - omega) * cell_i[FMMM] + omega * feqRM[FMMM];

        cell_i[F000] = foutRM[F000];

        cell_i[FP00] = foutRM[FM00];
        cell_i[FM00] = foutRM[FP00];

        cell_i[F0P0] = foutRM[F0M0];
        cell_i[F0M0] = foutRM[F0P0];

        cell_i[F00P] = foutRM[F00M];
        cell_i[F00M] = foutRM[F00P];

        cell_i[FPP0] = foutRM[FMM0];
        cell_i[FMP0] = foutRM[FPM0];
        cell_i[FPM0] = foutRM[FMP0];
        cell_i[FMM0] = foutRM[FPP0];

        cell_i[FP0P] = foutRM[FM0M];
        cell_i[FM0P] = foutRM[FP0M];
        cell_i[FP0M] = foutRM[FM0P];
        cell_i[FM0M] = foutRM[FP0P];

        cell_i[F0PP] = foutRM[F0MM];
        cell_i[F0MP] = foutRM[F0PM];
        cell_i[F0PM] = foutRM[F0MP];
        cell_i[F0MM] = foutRM[F0PP];

        cell_i[FPPP] = foutRM[FMMM];
        cell_i[FMPP] = foutRM[FPMM];
        cell_i[FPMP] = foutRM[FMPM];
        cell_i[FPPM] = foutRM[FMMP];
        cell_i[FMMP] = foutRM[FPPM];
        cell_i[FMPM] = foutRM[FPMP];
        cell_i[FPMM] = foutRM[FMPP];
        cell_i[FMMM] = foutRM[FPPP];
    }


    void iterateBgkUnrolled(CellData& cell) {
        int i = &cell - lattice;
        if (flag[i] == CellType::bulk) {
            auto[rho, u] = macro(cell);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
            collideBgkUnrolled(i, rho, u, usqr);
        }
    }

    void operator() (CellData& cell) {
        iterateBgkUnrolled(cell);
    }
};

} // namespace swap_aos_bgk_unrolled
