// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// Implementation of double-population scheme structure-of-array, for O2-BGK, with aggressive loop unrolling.

#pragma once

#include "lbm_q27.h"
#include <vector>
#include <array>
#include <tuple>

namespace twopop_soa_bgk_unrolled {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 2 * 27 * nelem; }

    CellData* lattice;
    CellType* flag;
    int* parity;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;

    auto i_to_xyz (int i) {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    double& fin (int i, int k) {
        return lattice[*parity * dim.npop + k * dim.nelem + i];
    }
    
    double& fout (int i, int k) {
        return lattice[(1 - *parity) * dim.npop + k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 27; ++k) {
            fin(i, k) = t[k];
        }
    };

    auto macro (double& f0) {
        auto i = &f0 - lattice;
        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12);
        double X_P1 = fin(i,14) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26);
        double X_0  = fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,13) + fin(i,15) + fin(i,16) + fin(i,21) + fin(i,22);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,18) + fin(i,25) + fin(i,26);
        double Y_P1 = fin(i,15) + fin(i,17) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i, 4) + fin(i,11) + fin(i,12);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i, 9) + fin(i,11) + fin(i,20) + fin(i,22) + fin(i,24) + fin(i,26);
        double Z_P1 = fin(i,16) + fin(i,19) + fin(i,21) + fin(i,23) + fin(i,25) + fin(i, 6) + fin(i, 8) + fin(i,10) + fin(i,12);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return std::make_pair(rho, u);
    }

    // Optimizations are based on: 
    // 1 - Computation of opposite equilibrium populations using symmetries
    // 2 - Precomputation of cku terms
    // 3 - Discarding zero-valued terms in indexes for the streaming step  
    auto collideAndStreamBgkUnrolled(int i, int iX, int iY, int iZ, double rho, std::array<double, 3> const& u, double usqr) {
        double cku1 = u[0] + u[1];
        double cku2 =-u[0] + u[1];
        double cku3 = u[0] + u[2];
        double cku4 =-u[0] + u[2];
        double cku5 = u[1] + u[2];
        double cku6 =-u[1] + u[2];
        double cku7 = u[0] + u[1] + u[2];
        double cku8 =-u[0] + u[1] + u[2];
        double cku9 = u[0] - u[1] + u[2];
        double cku0 = u[0] + u[1] - u[2];

        std::array<double, 27> feqRM;

        feqRM[F000] = rho * t[F000] * (1.                                 - usqr);

        feqRM[FM00] = rho * t[FM00] * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr);
        feqRM[FP00] = rho * t[FP00] * (     6. * u[0] ) + feqRM[FM00];
        
        feqRM[F0M0] = rho * t[F0M0] * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr);
        feqRM[F0P0] = rho * t[F0P0] * (     6. * u[1] ) + feqRM[F0M0];
        
        feqRM[F00M] = rho * t[F00M] * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr);
        feqRM[F00P] = rho * t[F00P] * (     6. * u[2] ) + feqRM[F00M];

        feqRM[FMM0] = rho * t[FMM0] * (1. - 3. * cku1 + 4.5 * cku1 * cku1 - usqr);
        feqRM[FPP0] = rho * t[FPP0] * (     6. * cku1 ) + feqRM[FMM0];
        feqRM[FPM0] = rho * t[FPM0] * (1. - 3. * cku2 + 4.5 * cku2 * cku2 - usqr);
        feqRM[FMP0] = rho * t[FMP0] * (     6. * cku2 ) + feqRM[FPM0];

        feqRM[FM0M] = rho * t[FM0M] * (1. - 3. * cku3 + 4.5 * cku3 * cku3 - usqr);
        feqRM[FP0P] = rho * t[FP0P] * (     6. * cku3 ) + feqRM[FM0M];
        feqRM[FP0M] = rho * t[FP0M] * (1. - 3. * cku4 + 4.5 * cku4 * cku4 - usqr);
        feqRM[FM0P] = rho * t[FM0P] * (     6. * cku4 ) + feqRM[FP0M];

        feqRM[F0MM] = rho * t[F0MM] * (1. - 3. * cku5 + 4.5 * cku5 * cku5 - usqr);
        feqRM[F0PP] = rho * t[F0PP] * (     6. * cku5 ) + feqRM[F0MM];
        feqRM[F0PM] = rho * t[F0PM] * (1. - 3. * cku6 + 4.5 * cku6 * cku6 - usqr);
        feqRM[F0MP] = rho * t[F0MP] * (     6. * cku6 ) + feqRM[F0PM];

        feqRM[FMMM] = rho * t[FMMM] * (1. - 3. * cku7 + 4.5 * cku7 * cku7 - usqr);
        feqRM[FPPP] = rho * t[FPPP] * (     6. * cku7 ) + feqRM[FMMM];
        feqRM[FPMM] = rho * t[FPMM] * (1. - 3. * cku8 + 4.5 * cku8 * cku8 - usqr);
        feqRM[FMPP] = rho * t[FMPP] * (     6. * cku8 ) + feqRM[FPMM];
        feqRM[FMPM] = rho * t[FMPM] * (1. - 3. * cku9 + 4.5 * cku9 * cku9 - usqr);
        feqRM[FPMP] = rho * t[FPMP] * (     6. * cku9 ) + feqRM[FMPM];
        feqRM[FMMP] = rho * t[FMMP] * (1. - 3. * cku0 + 4.5 * cku0 * cku0 - usqr);
        feqRM[FPPM] = rho * t[FPPM] * (     6. * cku0 ) + feqRM[FMMP];

        // BGK Collision based on the second-order equilibrium
        std::array<double, 27> foutRM;

        foutRM[F000] = (1. - omega) * fin(i,F000) + omega * feqRM[F000];

        foutRM[FP00] = (1. - omega) * fin(i,FP00) + omega * feqRM[FP00];
        foutRM[FM00] = (1. - omega) * fin(i,FM00) + omega * feqRM[FM00];

        foutRM[F0P0] = (1. - omega) * fin(i,F0P0) + omega * feqRM[F0P0];
        foutRM[F0M0] = (1. - omega) * fin(i,F0M0) + omega * feqRM[F0M0];

        foutRM[F00P] = (1. - omega) * fin(i,F00P) + omega * feqRM[F00P];
        foutRM[F00M] = (1. - omega) * fin(i,F00M) + omega * feqRM[F00M];

        foutRM[FPP0] = (1. - omega) * fin(i,FPP0) + omega * feqRM[FPP0];
        foutRM[FMP0] = (1. - omega) * fin(i,FMP0) + omega * feqRM[FMP0];
        foutRM[FPM0] = (1. - omega) * fin(i,FPM0) + omega * feqRM[FPM0];
        foutRM[FMM0] = (1. - omega) * fin(i,FMM0) + omega * feqRM[FMM0];

        foutRM[FP0P] = (1. - omega) * fin(i,FP0P) + omega * feqRM[FP0P];
        foutRM[FM0P] = (1. - omega) * fin(i,FM0P) + omega * feqRM[FM0P];
        foutRM[FP0M] = (1. - omega) * fin(i,FP0M) + omega * feqRM[FP0M];
        foutRM[FM0M] = (1. - omega) * fin(i,FM0M) + omega * feqRM[FM0M];

        foutRM[F0PP] = (1. - omega) * fin(i,F0PP) + omega * feqRM[F0PP];
        foutRM[F0MP] = (1. - omega) * fin(i,F0MP) + omega * feqRM[F0MP];
        foutRM[F0PM] = (1. - omega) * fin(i,F0PM) + omega * feqRM[F0PM];
        foutRM[F0MM] = (1. - omega) * fin(i,F0MM) + omega * feqRM[F0MM];

        foutRM[FPPP] = (1. - omega) * fin(i,FPPP) + omega * feqRM[FPPP];
        foutRM[FMPP] = (1. - omega) * fin(i,FMPP) + omega * feqRM[FMPP];
        foutRM[FPMP] = (1. - omega) * fin(i,FPMP) + omega * feqRM[FPMP];
        foutRM[FPPM] = (1. - omega) * fin(i,FPPM) + omega * feqRM[FPPM];
        foutRM[FMMP] = (1. - omega) * fin(i,FMMP) + omega * feqRM[FMMP];
        foutRM[FMPM] = (1. - omega) * fin(i,FMPM) + omega * feqRM[FMPM];
        foutRM[FPMM] = (1. - omega) * fin(i,FPMM) + omega * feqRM[FPMM];
        foutRM[FMMM] = (1. - omega) * fin(i,FMMM) + omega * feqRM[FMMM];

        // Unrolled streaming step
        int XX = iX - 1;
        int YY = iY;
        int ZZ = iZ;
        size_t nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 0]) = foutRM[0] + f(nb, 0);
        }
        else {
            fout(nb,0)= foutRM[0];
        }
        XX = iX;
        YY = iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 1]) = foutRM[1] + f(nb, 1);
        }
        else {
            fout(nb,1)= foutRM[1];
        }
        XX = iX;
        YY = iY;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 2]) = foutRM[2] + f(nb, 2);
        }
        else {
            fout(nb,2)= foutRM[2];
        }

        XX = iX - 1;
        YY = iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 3]) = foutRM[3] + f(nb, 3);
        }
        else {
            fout(nb,3)= foutRM[3];
        }
        XX = iX - 1;
        YY = iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 4]) = foutRM[4] + f(nb, 4);
        }
        else {
            fout(nb,4)= foutRM[4];
        }
        XX = iX - 1;
        YY = iY;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 5]) = foutRM[5] + f(nb, 5);
        }
        else {
            fout(nb,5)= foutRM[5];
        }
        XX = iX - 1;
        YY = iY;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 6]) = foutRM[6] + f(nb, 6);
        }
        else {
            fout(nb,6)= foutRM[6];
        }
        XX = iX;
        YY = iY - 1;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 7]) = foutRM[7] + f(nb, 7);
        }
        else {
            fout(nb,7)= foutRM[7];
        }
        XX = iX;
        YY = iY - 1;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 8]) = foutRM[8] + f(nb, 8);
        }
        else {
            fout(nb,8)= foutRM[8];
        }

        XX = iX - 1;
        YY = iY - 1;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 9]) = foutRM[9] + f(nb, 9);
        }
        else {
            fout(nb,9)= foutRM[9];
        }
        XX = iX - 1;
        YY = iY - 1;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[10]) = foutRM[10] + f(nb, 10);
        }
        else {
            fout(nb,10) = foutRM[10];
        }
        XX = iX - 1;
        YY = iY + 1;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[11]) = foutRM[11] + f(nb, 11);
        }
        else {
            fout(nb,11) = foutRM[11];
        }
        XX = iX - 1;
        YY = iY + 1;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[12]) = foutRM[12] + f(nb, 12);
        }
        else {
            fout(nb,12) = foutRM[12];
        }


        fout(i,13) = foutRM[13];


        XX = iX + 1;
        YY = iY;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[14]) = foutRM[14] + f(nb, 14);
        }
        else {
            fout(nb,14) = foutRM[14];
        }
        XX = iX;
        YY = iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[15]) = foutRM[15] + f(nb, 15);
        }
        else {
            fout(nb,15) = foutRM[15];
        }
        XX = iX;
        YY = iY;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[16]) = foutRM[16] + f(nb, 16);
        }
        else {
            fout(nb,16) = foutRM[16];
        }

        XX = iX + 1;
        YY = iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[17]) = foutRM[17] + f(nb, 17);
        }
        else {
            fout(nb,17) = foutRM[17];
        }
        XX = iX + 1;
        YY = iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[18]) = foutRM[18] + f(nb, 18);
        }
        else {
            fout(nb,18) = foutRM[18];
        }
        XX = iX + 1;
        YY = iY;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[19]) = foutRM[19] + f(nb, 19);
        }
        else {
            fout(nb,19) = foutRM[19];
        }
        XX = iX + 1;
        YY = iY;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[20]) = foutRM[20] + f(nb, 20);
        }
        else {
            fout(nb,20) = foutRM[20];
        }
        XX = iX;
        YY = iY + 1;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[21]) = foutRM[21] + f(nb, 21);
        }
        else {
            fout(nb,21) = foutRM[21];
        }
        XX = iX;
        YY = iY + 1;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[22]) = foutRM[22] + f(nb, 22);
        }
        else {
            fout(nb,22) = foutRM[22];
        }

        XX = iX + 1;
        YY = iY + 1;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[23]) = foutRM[23] + f(nb, 23);
        }
        else {
            fout(nb,23) = foutRM[23];
        }
        XX = iX + 1;
        YY = iY + 1;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[24]) = foutRM[24] + f(nb, 24);
        }
        else {
            fout(nb,24) = foutRM[24];
        }
        XX = iX + 1;
        YY = iY - 1;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[25]) = foutRM[25] + f(nb, 25);
        }
        else {
            fout(nb,25) = foutRM[25];
        }
        XX = iX + 1;
        YY = iY - 1;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[26]) = foutRM[26] + f(nb, 26);
        }
        else {
            fout(nb,26) = foutRM[26];
        }
    }


    void iterateBgkUnrolled(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[rho, u] = macro(f0);
            auto[iX, iY, iZ] = i_to_xyz(i);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
            collideAndStreamBgkUnrolled(i, iX, iY, iZ, rho, u, usqr);
        }
    }

    void operator() (double& f0) {
        iterateBgkUnrolled(f0);
    }
};

} // namespace twopop_soa_bgk_unrolled
