// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// Implementation of swap scheme array-of-structure, for TRT, with aggressive loop unrolling.

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>

namespace swap_aos_trt_unrolled {

struct LBM {
    using CellData = std::array<double, 19>;
    static size_t sizeOfLattice(size_t nelem) { return nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;
    bool periodic = false;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[i][k];
    }

    CellData& cell(int i) {
        return lattice[i];
    }

    auto iniLattice (CellData& cell) {
        for (int k = 0; k < 19; ++k) {
            cell[k] = t[k];
        }
    };

    auto iniLattice (CellData& cell, double rho, std::array<double, 3> const& u) {
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 19; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            cell[k] = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    auto macro (CellData const& pop) {
        double X_M1 = pop[0] + pop[3] + pop[4] + pop[5] + pop[6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[9] + pop[1] + pop[2] + pop[7] + pop[8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[1] + pop[3] + pop[7] + pop[8] + pop[14];
        double Y_P1 = pop[4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[2] + pop[5] + pop[7] + pop[16] + pop[18];
        double Z_P1 = pop[6] + pop[8] + pop[12] + pop[15] + pop[17];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };
};


struct Stream : public LBM {
    void operator() (CellData& cell) {
        auto i = &cell - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        int XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        int YY = iY;
        int ZZ = iZ;
        if (XX >= 0) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 0) += cell[10];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[10] += f(nb, 0);
                }
                else {
                    std::swap(cell[10], f(nb, 0));
                }
            }
        }

        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = iZ;
        if (YY >= 0) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 1) += cell[11];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[11] += f(nb, 1);
                }
                else {
                    std::swap(cell[11], f(nb, 1));
                }
            }
        }

        XX = iX;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        if (ZZ >= 0) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 2) += cell[12];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[12] += f(nb, 2);
                }
                else {
                    std::swap(cell[12], f(nb, 2));
                }
            }
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = iZ;
        if (periodic ||(XX >= 0 && YY >= 0)) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 3) += cell[13];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[13] += f(nb, 3);
                }
                else {
                    std::swap(cell[13], f(nb, 3));
                }
            }
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = iZ;
        if (periodic || (XX >= 0 && YY < dim.ny)) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 4) += cell[14];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[14] += f(nb, 4);
                }
                else {
                    std::swap(cell[14], f(nb, 4));
                }
            }
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        if (periodic || (XX >= 0 && ZZ >= 0)) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 5) += cell[15];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[15] += f(nb, 5);
                }
                else {
                    std::swap(cell[15], f(nb, 5));
                }
            }
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        if (periodic || (XX >= 0 && ZZ < dim.nz)) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 6) += cell[16];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[16] += f(nb, 6);
                }
                else {
                    std::swap(cell[16], f(nb, 6));
                }
            }
        }

        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        if (periodic || (YY >= 0 && ZZ >= 0)) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 7) += cell[17];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[17] += f(nb, 7);
                }
                else {
                    std::swap(cell[17], f(nb, 7));
                }
            }
        }

        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        if (periodic || (YY >= 0 && ZZ < dim.nz)) {
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (cellType == CellType::bounce_back && nbCellType != CellType::bounce_back) {
                f(nb, 8) += cell[18];
            }
            if (cellType != CellType::bounce_back) {
                if (nbCellType == CellType::bounce_back) {
                    cell[18] += f(nb, 8);
                }
                else {
                    std::swap(cell[18], f(nb, 8));
                }
            }
        }

    }
};


struct Collide : public LBM {

    // Optimizations are based on: 
    // 1 - Computation of opposite equilibrium populations using symmetries
    // 2 - Precomputation of ck_u terms
    auto collideTrtUnrolled(int i, double rho, std::array<double, 3> const& u, double usqr) {
        double ck_u03 = u[0] + u[1];
        double ck_u04 = u[0] - u[1];
        double ck_u05 = u[0] + u[2];
        double ck_u06 = u[0] - u[2];
        double ck_u07 = u[1] + u[2];
        double ck_u08 = u[1] - u[2];

        double s_plus = omega;
        double s_minus = 8. * (2. - s_plus) / (8. - s_plus);
        double s1 = 1 - 0.5*(s_plus+s_minus);
        double s2 = 0.5*(-s_plus+s_minus);
        double s3 = 1 - s1;

        double eq_00    = rho * t[ 0] * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr);
        double eq_01    = rho * t[ 1] * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr);
        double eq_02    = rho * t[ 2] * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr);
        double eq_03    = rho * t[ 3] * (1. - 3. * ck_u03 + 4.5 * ck_u03 * ck_u03 - usqr);
        double eq_04    = rho * t[ 4] * (1. - 3. * ck_u04 + 4.5 * ck_u04 * ck_u04 - usqr);
        double eq_05    = rho * t[ 5] * (1. - 3. * ck_u05 + 4.5 * ck_u05 * ck_u05 - usqr);
        double eq_06    = rho * t[ 6] * (1. - 3. * ck_u06 + 4.5 * ck_u06 * ck_u06 - usqr);
        double eq_07    = rho * t[ 7] * (1. - 3. * ck_u07 + 4.5 * ck_u07 * ck_u07 - usqr);
        double eq_08    = rho * t[ 8] * (1. - 3. * ck_u08 + 4.5 * ck_u08 * ck_u08 - usqr);
        double eq_09    = rho * t[ 9] * (1. - usqr);
        double eqopp_00 = eq_00 + rho * t[ 0] * 6. * u[0];
        double eqopp_01 = eq_01 + rho * t[ 1] * 6. * u[1];
        double eqopp_02 = eq_02 + rho * t[ 2] * 6. * u[2];
        double eqopp_03 = eq_03 + rho * t[ 3] * 6. * ck_u03;
        double eqopp_04 = eq_04 + rho * t[ 4] * 6. * ck_u04;
        double eqopp_05 = eq_05 + rho * t[ 5] * 6. * ck_u05;
        double eqopp_06 = eq_06 + rho * t[ 6] * 6. * ck_u06;
        double eqopp_07 = eq_07 + rho * t[ 7] * 6. * ck_u07;
        double eqopp_08 = eq_08 + rho * t[ 8] * 6. * ck_u08;

        CellData& cell_i = cell(i);

        double pop_in_00 = cell_i[ 0];
        double pop_in_01 = cell_i[ 1];
        double pop_in_02 = cell_i[ 2];
        double pop_in_03 = cell_i[ 3];
        double pop_in_04 = cell_i[ 4];
        double pop_in_05 = cell_i[ 5];
        double pop_in_06 = cell_i[ 6];
        double pop_in_07 = cell_i[ 7];
        double pop_in_08 = cell_i[ 8];

        double pop_in_10 = cell_i[10];
        double pop_in_11 = cell_i[11];
        double pop_in_12 = cell_i[12];
        double pop_in_13 = cell_i[13];
        double pop_in_14 = cell_i[14];
        double pop_in_15 = cell_i[15];
        double pop_in_16 = cell_i[16];
        double pop_in_17 = cell_i[17];
        double pop_in_18 = cell_i[18];
        
        cell_i[10] = pop_in_00*s1 + (pop_in_10 - eqopp_00)*s2 + eq_00*s3;
        cell_i[11] = pop_in_01*s1 + (pop_in_11 - eqopp_01)*s2 + eq_01*s3;
        cell_i[12] = pop_in_02*s1 + (pop_in_12 - eqopp_02)*s2 + eq_02*s3;
        cell_i[13] = pop_in_03*s1 + (pop_in_13 - eqopp_03)*s2 + eq_03*s3;
        cell_i[14] = pop_in_04*s1 + (pop_in_14 - eqopp_04)*s2 + eq_04*s3;
        cell_i[15] = pop_in_05*s1 + (pop_in_15 - eqopp_05)*s2 + eq_05*s3;
        cell_i[16] = pop_in_06*s1 + (pop_in_16 - eqopp_06)*s2 + eq_06*s3;
        cell_i[17] = pop_in_07*s1 + (pop_in_17 - eqopp_07)*s2 + eq_07*s3;
        cell_i[18] = pop_in_08*s1 + (pop_in_18 - eqopp_08)*s2 + eq_08*s3;

        cell_i[ 0] = pop_in_10*s1 + (pop_in_00 - eq_00)*s2 + eqopp_00*s3;
        cell_i[ 1] = pop_in_11*s1 + (pop_in_01 - eq_01)*s2 + eqopp_01*s3;
        cell_i[ 2] = pop_in_12*s1 + (pop_in_02 - eq_02)*s2 + eqopp_02*s3;
        cell_i[ 3] = pop_in_13*s1 + (pop_in_03 - eq_03)*s2 + eqopp_03*s3;
        cell_i[ 4] = pop_in_14*s1 + (pop_in_04 - eq_04)*s2 + eqopp_04*s3;
        cell_i[ 5] = pop_in_15*s1 + (pop_in_05 - eq_05)*s2 + eqopp_05*s3;
        cell_i[ 6] = pop_in_16*s1 + (pop_in_06 - eq_06)*s2 + eqopp_06*s3;
        cell_i[ 7] = pop_in_17*s1 + (pop_in_07 - eq_07)*s2 + eqopp_07*s3;
        cell_i[ 8] = pop_in_18*s1 + (pop_in_08 - eq_08)*s2 + eqopp_08*s3;

        cell_i[ 9] = (1. - omega) * cell_i[ 9] + omega * eq_09;
    }


    void iterateTrtUnrolled(CellData& cell) {
        int i = &cell - lattice;
        if (flag[i] == CellType::bulk) {
            auto[rho, u] = macro(cell);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
            collideTrtUnrolled(i, rho, u, usqr);
        }
    }

    void operator() (CellData& cell) {
        iterateTrtUnrolled(cell);
    }
};

} // namespace swap_aos_trt_unrolled
