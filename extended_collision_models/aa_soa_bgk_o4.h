// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>

namespace aa_soa_bgk_o4 {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 19 * nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;
    bool periodic = false;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 19; ++k) {
            f(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 19; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = f(i, 0) + f(i, 3) + f(i, 4) + f(i, 5) + f(i, 6);
        double X_P1 = f(i,10) + f(i,13) + f(i,14) + f(i,15) + f(i,16);
        double X_0  = f(i, 9) + f(i, 1) + f(i, 2) + f(i, 7) + f(i, 8) + f(i,11) + f(i,12) + f(i,17) + f(i,18);

        double Y_M1 = f(i, 1) + f(i, 3) + f(i, 7) + f(i, 8) + f(i,14);
        double Y_P1 = f(i, 4) + f(i,11) + f(i,13) + f(i,17) + f(i,18);

        double Z_M1 = f(i, 2) + f(i, 5) + f(i, 7) + f(i,16) + f(i,18);
        double Z_P1 = f(i, 6) + f(i, 8) + f(i,12) + f(i,15) + f(i,17);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    }

    auto macropop (std::array<double, 19> const& pop) {
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[ 9] + pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[14];
        double Y_P1 = pop[ 4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[16] + pop[18];
        double Z_P1 = pop[ 6] + pop[ 8] + pop[12] + pop[15] + pop[17];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };
};

struct Even : public LBM {

    auto collideAndStream_BGK_o4_opt(int i, double rho, std::array<double, 3> const& u)
    {
        std::array<double, 19> RMeq;
        std::array<double, 19> feqRM;
        double cs2 = 1./3.;

        // Order 2
        RMeq[M200] = u[0] * u[0] + cs2;
        RMeq[M020] = u[1] * u[1] + cs2;
        RMeq[M002] = u[2] * u[2] + cs2;
        RMeq[M110] = u[0] * u[1];
        RMeq[M101] = u[0] * u[2];
        RMeq[M011] = u[1] * u[2];
        // Order 3
        RMeq[M210] = RMeq[M200] * u[1];
        RMeq[M201] = RMeq[M200] * u[2];
        RMeq[M021] = RMeq[M020] * u[2];
        RMeq[M120] = RMeq[M020] * u[0];
        RMeq[M102] = RMeq[M002] * u[0];
        RMeq[M012] = RMeq[M002] * u[1];
        // Order 4
        RMeq[M220] = RMeq[M200] * RMeq[M020];
        RMeq[M202] = RMeq[M200] * RMeq[M002];
        RMeq[M022] = RMeq[M020] * RMeq[M002];

        // Complete equilibrium through RMeq
        // Optimization based on symmetries between populations and their opposite counterpart
        feqRM[ 9] = rho *(1. - RMeq[M200] - RMeq[M020] - RMeq[M002] + RMeq[M220] + RMeq[M202] + RMeq[M022]);
        
        feqRM[10] = 0.5*rho * ( u[0] + RMeq[M200] - RMeq[M120] - RMeq[M102] - RMeq[M220] - RMeq[M202]);
        feqRM[ 0] =     rho * (-u[0]              + RMeq[M120] + RMeq[M102]) + feqRM[10];

        feqRM[11] = 0.5*rho * ( u[1] + RMeq[M020] - RMeq[M210] - RMeq[M012] - RMeq[M220] - RMeq[M022]);
        feqRM[ 1] =     rho * (-u[1]              + RMeq[M210] + RMeq[M012]) + feqRM[11];

        feqRM[12] = 0.5*rho * ( u[2] + RMeq[M002] - RMeq[M201] - RMeq[M021] - RMeq[M202] - RMeq[M022]);
        feqRM[ 2] =     rho * (-u[2]              + RMeq[M201] + RMeq[M021]) + feqRM[12];

        feqRM[13] = 0.25*rho * ( RMeq[M110] + RMeq[M210] + RMeq[M120] + RMeq[M220]);
        feqRM[ 4] =  0.5*rho * (-RMeq[M110]              - RMeq[M120]) + feqRM[13];
        feqRM[14] =  0.5*rho * (-RMeq[M110] - RMeq[M210])              + feqRM[13];
        feqRM[ 3] =  0.5*rho * (            - RMeq[M210] - RMeq[M120]) + feqRM[13];

        feqRM[15] = 0.25*rho * ( RMeq[M101] + RMeq[M201] + RMeq[M102] + RMeq[M202]);
        feqRM[ 6] =  0.5*rho * (-RMeq[M101]              - RMeq[M102]) + feqRM[15];
        feqRM[16] =  0.5*rho * (-RMeq[M101] - RMeq[M201])              + feqRM[15];
        feqRM[ 5] =  0.5*rho * (            - RMeq[M201] - RMeq[M102]) + feqRM[15];

        feqRM[17] = 0.25*rho * ( RMeq[M011] + RMeq[M021] + RMeq[M012] + RMeq[M022]);
        feqRM[ 8] =  0.5*rho * (-RMeq[M011]              - RMeq[M012]) + feqRM[17];
        feqRM[18] =  0.5*rho * (-RMeq[M011] - RMeq[M021])              + feqRM[17];
        feqRM[ 7] =  0.5*rho * (            - RMeq[M021] - RMeq[M012]) + feqRM[17];

        // BGK Collision based on the above extended equilibrium
        double pop_out_00 = (1. - omega) * f(i, 0) + omega * feqRM[ 0];
        double pop_out_01 = (1. - omega) * f(i, 1) + omega * feqRM[ 1];
        double pop_out_02 = (1. - omega) * f(i, 2) + omega * feqRM[ 2];
        double pop_out_03 = (1. - omega) * f(i, 3) + omega * feqRM[ 3];
        double pop_out_04 = (1. - omega) * f(i, 4) + omega * feqRM[ 4];
        double pop_out_05 = (1. - omega) * f(i, 5) + omega * feqRM[ 5];
        double pop_out_06 = (1. - omega) * f(i, 6) + omega * feqRM[ 6];
        double pop_out_07 = (1. - omega) * f(i, 7) + omega * feqRM[ 7];
        double pop_out_08 = (1. - omega) * f(i, 8) + omega * feqRM[ 8];

        double pop_out_10 = (1. - omega) * f(i,10) + omega * feqRM[10];
        double pop_out_11 = (1. - omega) * f(i,11) + omega * feqRM[11];
        double pop_out_12 = (1. - omega) * f(i,12) + omega * feqRM[12];
        double pop_out_13 = (1. - omega) * f(i,13) + omega * feqRM[13];
        double pop_out_14 = (1. - omega) * f(i,14) + omega * feqRM[14];
        double pop_out_15 = (1. - omega) * f(i,15) + omega * feqRM[15];
        double pop_out_16 = (1. - omega) * f(i,16) + omega * feqRM[16];
        double pop_out_17 = (1. - omega) * f(i,17) + omega * feqRM[17];
        double pop_out_18 = (1. - omega) * f(i,18) + omega * feqRM[18];

        double pop_out_09 = (1. - omega) * f(i, 9) + omega * feqRM[ 9];

        f(i,  0) = pop_out_10;
        f(i, 10) = pop_out_00;

        f(i,  1) = pop_out_11;
        f(i, 11) = pop_out_01;

        f(i,  2) = pop_out_12;
        f(i, 12) = pop_out_02;

        f(i,  3) = pop_out_13;
        f(i, 13) = pop_out_03;

        f(i,  4) = pop_out_14;
        f(i, 14) = pop_out_04;

        f(i,  5) = pop_out_15;
        f(i, 15) = pop_out_05;

        f(i,  6) = pop_out_16;
        f(i, 16) = pop_out_06;

        f(i,  7) = pop_out_17;
        f(i, 17) = pop_out_07;

        f(i,  8) = pop_out_18;
        f(i, 18) = pop_out_08;

        f(i, 9) = pop_out_09;
    }


    void iterateBGK_o4(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            std::array<double, 19> pop;
            for (int k = 0; k < 19; ++k) {
                pop[k] = f(i, k);
            }
            auto[rho, u] = macropop(pop);
            collideAndStream_BGK_o4_opt(i, rho, u);
        }
    }

    void operator() (double& f0) {
        iterateBGK_o4(f0);
    }
};

struct Odd : public LBM {

    auto collideAndStream_BGK_o4_opt(std::array<double, 19>& pop, double rho, std::array<double, 3> const& u)
    {
        std::array<double, 19> RMeq;
        std::array<double, 19> feqRM;
        double cs2 = 1./3.;

        // Order 2
        RMeq[M200] = u[0] * u[0] + cs2;
        RMeq[M020] = u[1] * u[1] + cs2;
        RMeq[M002] = u[2] * u[2] + cs2;
        RMeq[M110] = u[0] * u[1];
        RMeq[M101] = u[0] * u[2];
        RMeq[M011] = u[1] * u[2];
        // Order 3
        RMeq[M210] = RMeq[M200] * u[1];
        RMeq[M201] = RMeq[M200] * u[2];
        RMeq[M021] = RMeq[M020] * u[2];
        RMeq[M120] = RMeq[M020] * u[0];
        RMeq[M102] = RMeq[M002] * u[0];
        RMeq[M012] = RMeq[M002] * u[1];
        // Order 4
        RMeq[M220] = RMeq[M200] * RMeq[M020];
        RMeq[M202] = RMeq[M200] * RMeq[M002];
        RMeq[M022] = RMeq[M020] * RMeq[M002];

        // Complete equilibrium through RMeq
        // Optimization based on symmetries between populations and their opposite counterpart
        feqRM[ 9] = rho *(1. - RMeq[M200] - RMeq[M020] - RMeq[M002] + RMeq[M220] + RMeq[M202] + RMeq[M022]);
        
        feqRM[10] = 0.5*rho * ( u[0] + RMeq[M200] - RMeq[M120] - RMeq[M102] - RMeq[M220] - RMeq[M202]);
        feqRM[ 0] =     rho * (-u[0]              + RMeq[M120] + RMeq[M102]) + feqRM[10];

        feqRM[11] = 0.5*rho * ( u[1] + RMeq[M020] - RMeq[M210] - RMeq[M012] - RMeq[M220] - RMeq[M022]);
        feqRM[ 1] =     rho * (-u[1]              + RMeq[M210] + RMeq[M012]) + feqRM[11];

        feqRM[12] = 0.5*rho * ( u[2] + RMeq[M002] - RMeq[M201] - RMeq[M021] - RMeq[M202] - RMeq[M022]);
        feqRM[ 2] =     rho * (-u[2]              + RMeq[M201] + RMeq[M021]) + feqRM[12];

        feqRM[13] = 0.25*rho * ( RMeq[M110] + RMeq[M210] + RMeq[M120] + RMeq[M220]);
        feqRM[ 4] =  0.5*rho * (-RMeq[M110]              - RMeq[M120]) + feqRM[13];
        feqRM[14] =  0.5*rho * (-RMeq[M110] - RMeq[M210])              + feqRM[13];
        feqRM[ 3] =  0.5*rho * (            - RMeq[M210] - RMeq[M120]) + feqRM[13];

        feqRM[15] = 0.25*rho * ( RMeq[M101] + RMeq[M201] + RMeq[M102] + RMeq[M202]);
        feqRM[ 6] =  0.5*rho * (-RMeq[M101]              - RMeq[M102]) + feqRM[15];
        feqRM[16] =  0.5*rho * (-RMeq[M101] - RMeq[M201])              + feqRM[15];
        feqRM[ 5] =  0.5*rho * (            - RMeq[M201] - RMeq[M102]) + feqRM[15];

        feqRM[17] = 0.25*rho * ( RMeq[M011] + RMeq[M021] + RMeq[M012] + RMeq[M022]);
        feqRM[ 8] =  0.5*rho * (-RMeq[M011]              - RMeq[M012]) + feqRM[17];
        feqRM[18] =  0.5*rho * (-RMeq[M011] - RMeq[M021])              + feqRM[17];
        feqRM[ 7] =  0.5*rho * (            - RMeq[M021] - RMeq[M012]) + feqRM[17];

        // BGK Collision based on the above extended equilibrium
        double pop_out_00 = (1. - omega) * pop[ 0] + omega * feqRM[ 0];
        double pop_out_01 = (1. - omega) * pop[ 1] + omega * feqRM[ 1];
        double pop_out_02 = (1. - omega) * pop[ 2] + omega * feqRM[ 2];
        double pop_out_03 = (1. - omega) * pop[ 3] + omega * feqRM[ 3];
        double pop_out_04 = (1. - omega) * pop[ 4] + omega * feqRM[ 4];
        double pop_out_05 = (1. - omega) * pop[ 5] + omega * feqRM[ 5];
        double pop_out_06 = (1. - omega) * pop[ 6] + omega * feqRM[ 6];
        double pop_out_07 = (1. - omega) * pop[ 7] + omega * feqRM[ 7];
        double pop_out_08 = (1. - omega) * pop[ 8] + omega * feqRM[ 8];

        double pop_out_10 = (1. - omega) * pop[10] + omega * feqRM[10];
        double pop_out_11 = (1. - omega) * pop[11] + omega * feqRM[11];
        double pop_out_12 = (1. - omega) * pop[12] + omega * feqRM[12];
        double pop_out_13 = (1. - omega) * pop[13] + omega * feqRM[13];
        double pop_out_14 = (1. - omega) * pop[14] + omega * feqRM[14];
        double pop_out_15 = (1. - omega) * pop[15] + omega * feqRM[15];
        double pop_out_16 = (1. - omega) * pop[16] + omega * feqRM[16];
        double pop_out_17 = (1. - omega) * pop[17] + omega * feqRM[17];
        double pop_out_18 = (1. - omega) * pop[18] + omega * feqRM[18];

        double pop_out_09 = (1. - omega) * pop[ 9] + omega * feqRM[ 9];

        pop[ 0] = pop_out_00;
        pop[10] = pop_out_10;

        pop[ 1] = pop_out_01;
        pop[11] = pop_out_11;

        pop[ 2] = pop_out_02;
        pop[12] = pop_out_12;

        pop[ 3] = pop_out_03;
        pop[13] = pop_out_13;

        pop[ 4] = pop_out_04;
        pop[14] = pop_out_14;

        pop[ 5] = pop_out_05;
        pop[15] = pop_out_15;

        pop[ 6] = pop_out_06;
        pop[16] = pop_out_16;

        pop[ 7] = pop_out_07;
        pop[17] = pop_out_17;

        pop[ 8] = pop_out_08;
        pop[18] = pop_out_18;

        pop[ 9] = pop_out_09;
    }

    auto streamingPull(int i, int iX, int iY, int iZ, std::array<double, 19>& pop)
    {
        if(periodic){
            for (int k = 0; k < 19; ++k) {
                int XX = (iX - c[k][0] + dim.nx) % dim.nx;
                int YY = (iY - c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ - c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
        else {
            for (int k = 0; k < 19; ++k) {
                int XX = iX - c[k][0];
                int YY = iY - c[k][1];
                int ZZ = iZ - c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
    }

    auto streamingPush(int i, int iX, int iY, int iZ, std::array<double, 19>& pop)
    {
        if(periodic){
            for (int k = 0; k < 19; ++k) {
                int XX = (iX + c[k][0] + dim.nx) % dim.nx;
                int YY = (iY + c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ + c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
        else {
            for (int k = 0; k < 19; ++k) {
                int XX = iX + c[k][0];
                int YY = iY + c[k][1];
                int ZZ = iZ + c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
    }

    void iterateBGK_o4(double& f0) {
        int i = &f0 - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            std::array<double, 19> pop;

            streamingPull(i, iX, iY, iZ, pop);

            auto[rho, u] = macropop(pop);

            collideAndStream_BGK_o4_opt(pop, rho, u);

            streamingPush(i, iX, iY, iZ, pop);
        }
    }

    void operator() (double& f0) {
        iterateBGK_o4(f0);
    }
};

} // namespace aa_soa_bgk_o4
