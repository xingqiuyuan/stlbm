// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>
#include <iostream>

namespace twopop_soa_chm {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 2 * 19 * nelem; }

    CellData* lattice;
    CellType* flag;
    int* parity;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega1;
    Dim dim;
    LBModel model;
    bool periodic = false;
    double omegaBulk = 0.;
    double omega2 = omega1;
    double omega3 = omega1;
    double omega4 = omega1;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    double& fin (int i, int k) {
        return lattice[*parity * dim.npop + k * dim.nelem + i];
    }
    
    double& fout (int i, int k) {
        return lattice[(1 - *parity) * dim.npop + k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 19; ++k) {
            fin(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 19; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    // Required for the nonregression based on computeEnergy() 
    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6);
        double X_P1 = fin(i,10) + fin(i,13) + fin(i,14) + fin(i,15) + fin(i,16);
        double X_0  = fin(i, 9) + fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,11) + fin(i,12) + fin(i,17) + fin(i,18);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i,14);
        double Y_P1 = fin(i, 4) + fin(i,11) + fin(i,13) + fin(i,17) + fin(i,18);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i,16) + fin(i,18);
        double Z_P1 = fin(i, 6) + fin(i, 8) + fin(i,12) + fin(i,15) + fin(i,17);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return std::make_pair(rho, u);
    }
 
    // The function below has only an educational purpose since its optimized version 
    // computeCHMopt() is preferred to achieve better performance
    auto computeCHM(double& f0, double const& rho, std::array<double, 3> const& u) {

        auto i = &f0 - lattice;
        std::array<double, 19> CHM;

        std::fill(CHM.begin(), CHM.end(), 0.);
        double Hxx;
        double Hyy;
        double Hzz;
        double cMux;
        double cMuy;
        double cMuz;
        double cs2 = 1./3.;
        for (int k = 0; k<19; ++k) {
            cMux = c[k][0] - u[0];
            cMuy = c[k][1] - u[1];
            cMuz = c[k][2] - u[2];

            Hxx = cMux * cMux - cs2;
            Hyy = cMuy * cMuy - cs2;
            Hzz = cMuz * cMuz - cs2;

            // Order 2
            CHM[M200] += Hxx * fin(i,k);
            CHM[M020] += Hyy * fin(i,k);
            CHM[M002] += Hzz * fin(i,k);
            CHM[M110] += cMux * cMuy * fin(i,k);
            CHM[M101] += cMux * cMuz * fin(i,k);
            CHM[M011] += cMuy * cMuz * fin(i,k);
            // Order 3
            CHM[M210] += Hxx * cMuy * fin(i,k);
            CHM[M201] += Hxx * cMuz * fin(i,k);
            CHM[M021] += Hyy * cMuz * fin(i,k);
            CHM[M120] += cMux * Hyy * fin(i,k);
            CHM[M102] += cMux * Hzz * fin(i,k);
            CHM[M012] += cMuy * Hzz * fin(i,k);
            // Order 4
            CHM[M220] += Hxx * Hyy * fin(i,k);
            CHM[M202] += Hxx * Hzz * fin(i,k);
            CHM[M022] += Hyy * Hzz * fin(i,k);
        }

        double invRho = 1./rho;
        for (int k = 0; k<19; ++k) {
            CHM[k] *= invRho;
        }

        return CHM;
    }

    auto computeCHMopt(double& f0, double const& rho, std::array<double, 3> const& u) {

        auto i = &f0 - lattice;
        std::array<double, 19> CHM;
        // Optimized way to compute raw moments
        double invRho = 1./rho;
        // Order 4
        CHM[M220] = invRho * (fin(i, 3) + fin(i, 4) + fin(i,13) + fin(i,14));
        CHM[M202] = invRho * (fin(i, 5) + fin(i, 6) + fin(i,15) + fin(i,16));
        CHM[M022] = invRho * (fin(i, 7) + fin(i, 8) + fin(i,17) + fin(i,18));
        // Order 2
        CHM[M200] = invRho * (fin(i, 0) + fin(i,10)) + CHM[M220] + CHM[M202];
        CHM[M020] = invRho * (fin(i, 1) + fin(i,11)) + CHM[M220] + CHM[M022];
        CHM[M002] = invRho * (fin(i, 2) + fin(i,12)) + CHM[M202] + CHM[M022];
        CHM[M110] = CHM[M220] - 2.*invRho * (fin(i, 4) + fin(i,14));
        CHM[M101] = CHM[M202] - 2.*invRho * (fin(i, 6) + fin(i,16));
        CHM[M011] = CHM[M022] - 2.*invRho * (fin(i, 8) + fin(i,18));
        // Order 3
        CHM[M210] = CHM[M220] - 2.*invRho * (fin(i, 3) + fin(i,14));
        CHM[M201] = CHM[M202] - 2.*invRho * (fin(i, 5) + fin(i,16));
        CHM[M021] = CHM[M022] - 2.*invRho * (fin(i, 7) + fin(i,18));
        CHM[M120] = CHM[M220] - 2.*invRho * (fin(i, 3) + fin(i, 4));
        CHM[M102] = CHM[M202] - 2.*invRho * (fin(i, 5) + fin(i, 6));
        CHM[M012] = CHM[M022] - 2.*invRho * (fin(i, 7) + fin(i, 8));

        // Compute central moments from raw moments using binomial formulas
        double ux2 = u[0]*u[0];
        double uy2 = u[1]*u[1];
        double uz2 = u[2]*u[2];
        double uxy = u[0]*u[1];
        double uxz = u[0]*u[2];
        double uyz = u[1]*u[2];

        CHM[M200] -= ux2;
        CHM[M020] -= uy2;
        CHM[M002] -= uz2;
        
        CHM[M110] -= uxy;
        CHM[M101] -= uxz;
        CHM[M011] -= uyz;

        CHM[M210] -= (u[1]*CHM[M200] + 2.*u[0]*CHM[M110] + ux2*u[1]);
        CHM[M201] -= (u[2]*CHM[M200] + 2.*u[0]*CHM[M101] + ux2*u[2]);
        CHM[M021] -= (u[2]*CHM[M020] + 2.*u[1]*CHM[M011] + uy2*u[2]);
        CHM[M120] -= (u[0]*CHM[M020] + 2.*u[1]*CHM[M110] + u[0]*uy2);
        CHM[M102] -= (u[0]*CHM[M002] + 2.*u[2]*CHM[M101] + u[0]*uz2);
        CHM[M012] -= (u[1]*CHM[M002] + 2.*u[2]*CHM[M011] + u[1]*uz2);
        
        CHM[M220] -= (2.*u[1]*CHM[M210] + 2.*u[0]*CHM[M120] + uy2*CHM[M200] + ux2*CHM[M020] + 4.*uxy*CHM[M110] + ux2*uy2);
        CHM[M202] -= (2.*u[2]*CHM[M201] + 2.*u[0]*CHM[M102] + uz2*CHM[M200] + ux2*CHM[M002] + 4.*uxz*CHM[M101] + ux2*uz2);
        CHM[M022] -= (2.*u[2]*CHM[M021] + 2.*u[1]*CHM[M012] + uz2*CHM[M020] + uy2*CHM[M002] + 4.*uyz*CHM[M011] + uy2*uz2);

        // Compute central Hermite moments from central moments
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        CHM[M200] -= cs2;
        CHM[M020] -= cs2;
        CHM[M002] -= cs2;

        CHM[M220] -= (cs2*(CHM[M200] + CHM[M020]) + cs4);
        CHM[M202] -= (cs2*(CHM[M200] + CHM[M002]) + cs4);
        CHM[M022] -= (cs2*(CHM[M020] + CHM[M002]) + cs4);

        return CHM;
    }

    // Further optimization through merge with the computation of macros
    auto computeCHMopt2(double& f0) {

        auto i = &f0 - lattice;
        std::array<double, 19> CHM;

        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6);
        double X_P1 = fin(i,10) + fin(i,13) + fin(i,14) + fin(i,15) + fin(i,16);
        double X_0  = fin(i, 9) + fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,11) + fin(i,12) + fin(i,17) + fin(i,18);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i,14);
        double Y_P1 = fin(i, 4) + fin(i,11) + fin(i,13) + fin(i,17) + fin(i,18);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i,16) + fin(i,18);
        double Z_P1 = fin(i, 6) + fin(i, 8) + fin(i,12) + fin(i,15) + fin(i,17);

        // Order 0
        CHM[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / CHM[M000];
        double two_invRho= 2.*invRho;
        // Order 1
        CHM[M100] = invRho * (X_P1 - X_M1);
        CHM[M010] = invRho * (Y_P1 - Y_M1); 
        CHM[M001] = invRho * (Z_P1 - Z_M1);
        // Order 2
        CHM[M200] = invRho * (X_M1 + X_P1);
        CHM[M020] = invRho * (Y_M1 + Y_P1);
        CHM[M002] = invRho * (Z_M1 + Z_P1);
        CHM[M110] = invRho * ( fin(i, 3) - fin(i, 4) + fin(i,13) - fin(i,14));
        CHM[M101] = invRho * ( fin(i, 5) - fin(i, 6) + fin(i,15) - fin(i,16));
        CHM[M011] = invRho * ( fin(i, 7) - fin(i, 8) + fin(i,17) - fin(i,18));
        // Order 3
        CHM[M210] = CHM[M110] - two_invRho * (fin(i, 3) - fin(i, 4));
        CHM[M201] = CHM[M101] - two_invRho * (fin(i, 5) - fin(i, 6));
        CHM[M021] = CHM[M011] - two_invRho * (fin(i, 7) - fin(i, 8));
        CHM[M120] = CHM[M110] - two_invRho * (fin(i, 3) - fin(i,14));
        CHM[M102] = CHM[M101] - two_invRho * (fin(i, 5) - fin(i,16));
        CHM[M012] = CHM[M011] - two_invRho * (fin(i, 7) - fin(i,18));
        // Order 4
        CHM[M220] = CHM[M110] + two_invRho * (fin(i, 4) + fin(i,14));
        CHM[M202] = CHM[M101] + two_invRho * (fin(i, 6) + fin(i,16));
        CHM[M022] = CHM[M011] + two_invRho * (fin(i, 8) + fin(i,18));

        // Compute central moments from raw moments using binomial formulas
        double ux2 = CHM[M100]*CHM[M100];
        double uy2 = CHM[M010]*CHM[M010];
        double uz2 = CHM[M001]*CHM[M001];
        double uxy = CHM[M100]*CHM[M010];
        double uxz = CHM[M100]*CHM[M001];
        double uyz = CHM[M010]*CHM[M001];

        CHM[M200] -= ux2;
        CHM[M020] -= uy2;
        CHM[M002] -= uz2;
        
        CHM[M110] -= uxy;
        CHM[M101] -= uxz;
        CHM[M011] -= uyz;

        CHM[M210] -= (CHM[M010]*CHM[M200] + 2.*CHM[M100]*CHM[M110] + ux2*CHM[M010]);
        CHM[M201] -= (CHM[M001]*CHM[M200] + 2.*CHM[M100]*CHM[M101] + ux2*CHM[M001]);
        CHM[M021] -= (CHM[M001]*CHM[M020] + 2.*CHM[M010]*CHM[M011] + uy2*CHM[M001]);
        CHM[M120] -= (CHM[M100]*CHM[M020] + 2.*CHM[M010]*CHM[M110] + CHM[M100]*uy2);
        CHM[M102] -= (CHM[M100]*CHM[M002] + 2.*CHM[M001]*CHM[M101] + CHM[M100]*uz2);
        CHM[M012] -= (CHM[M010]*CHM[M002] + 2.*CHM[M001]*CHM[M011] + CHM[M010]*uz2);
        
        CHM[M220] -= (2.*CHM[M010]*CHM[M210] + 2.*CHM[M100]*CHM[M120] + uy2*CHM[M200] + ux2*CHM[M020] + 4.*uxy*CHM[M110] + ux2*uy2);
        CHM[M202] -= (2.*CHM[M001]*CHM[M201] + 2.*CHM[M100]*CHM[M102] + uz2*CHM[M200] + ux2*CHM[M002] + 4.*uxz*CHM[M101] + ux2*uz2);
        CHM[M022] -= (2.*CHM[M001]*CHM[M021] + 2.*CHM[M010]*CHM[M012] + uz2*CHM[M020] + uy2*CHM[M002] + 4.*uyz*CHM[M011] + uy2*uz2);

        // Compute central Hermite moments from central moments
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        CHM[M200] -= cs2;
        CHM[M020] -= cs2;
        CHM[M002] -= cs2;

        CHM[M220] -= (cs2*(CHM[M200] + CHM[M020]) + cs4);
        CHM[M202] -= (cs2*(CHM[M200] + CHM[M002]) + cs4);
        CHM[M022] -= (cs2*(CHM[M020] + CHM[M002]) + cs4);

        return CHM;
    }

    // The function below has only an educational purpose since it is discarded
    // during the compilation because CHMeq are not used in collideCHM()
    auto computeCHMeq() {

        std::array<double, 19> CHMeq;

        // Order 2
        CHMeq[M200] = 0.;
        CHMeq[M020] = 0.;
        CHMeq[M002] = 0.;
        CHMeq[M110] = 0.;
        CHMeq[M101] = 0.;
        CHMeq[M011] = 0.;
        // Order 3
        CHMeq[M210] = 0.;
        CHMeq[M201] = 0.;
        CHMeq[M021] = 0.;
        CHMeq[M120] = 0.;
        CHMeq[M102] = 0.;
        CHMeq[M012] = 0.;
        // Order 4
        CHMeq[M220] = 0.;
        CHMeq[M202] = 0.;
        CHMeq[M022] = 0.;

        return CHMeq;
    }

    auto collideAndStreamCHM(int i, int iX, int iY, int iZ, double rho, std::array<double, 3> const& u, 
                             std::array<double, 19> const& CHM, std::array<double, 19> const& CHMeq) {

        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        // Post-collision moments.
        std::array<double, 19> RMcoll;
        std::array<double, 19> HMcoll;
        std::array<double, 19> CHMcoll;

        // Collision in the central Hermite moment space (CHMeq are replaced by their values)
        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity
            CHMcoll[M200] = (1. - omega1) * CHM[M200];
            CHMcoll[M020] = (1. - omega1) * CHM[M020];
            CHMcoll[M002] = (1. - omega1) * CHM[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaBulk = omega1;
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            CHMcoll[M200] = CHM[M200] - omegaPlus  * (CHM[M200]) - omegaMinus * (CHM[M020]) - omegaMinus * (CHM[M002]) ;
            CHMcoll[M020] = CHM[M020] - omegaMinus * (CHM[M200]) - omegaPlus  * (CHM[M020]) - omegaMinus * (CHM[M002]) ;
            CHMcoll[M002] = CHM[M002] - omegaMinus * (CHM[M200]) - omegaMinus * (CHM[M020]) - omegaPlus  * (CHM[M002]) ;
        }

        CHMcoll[M110] = (1. - omega2) * CHM[M110];
        CHMcoll[M101] = (1. - omega2) * CHM[M101];
        CHMcoll[M011] = (1. - omega2) * CHM[M011];

        // Order 3
        CHMcoll[M210] = (1. - omega3) * CHM[M210];
        CHMcoll[M201] = (1. - omega3) * CHM[M201];
        CHMcoll[M021] = (1. - omega3) * CHM[M021];
        CHMcoll[M120] = (1. - omega3) * CHM[M120];
        CHMcoll[M102] = (1. - omega3) * CHM[M102];
        CHMcoll[M012] = (1. - omega3) * CHM[M012];
        
        // Order 4
        CHMcoll[M220] = (1. - omega4) * CHM[M220];
        CHMcoll[M202] = (1. - omega4) * CHM[M202];
        CHMcoll[M022] = (1. - omega4) * CHM[M022];

        // Come back to HMcoll using relationships between CHMs and HMs

        double ux2 = u[0]*u[0];
        double uy2 = u[1]*u[1];
        double uz2 = u[2]*u[2];

        HMcoll[M200] = CHMcoll[M200] + ux2;
        HMcoll[M020] = CHMcoll[M020] + uy2;
        HMcoll[M002] = CHMcoll[M002] + uz2;
        
        HMcoll[M110] = CHMcoll[M110] + u[0]*u[1];
        HMcoll[M101] = CHMcoll[M101] + u[0]*u[2];
        HMcoll[M011] = CHMcoll[M011] + u[1]*u[2];

        HMcoll[M210] = CHMcoll[M210] + u[1]*CHMcoll[M200] + 2.*u[0]*CHMcoll[M110] + ux2*u[1];
        HMcoll[M201] = CHMcoll[M201] + u[2]*CHMcoll[M200] + 2.*u[0]*CHMcoll[M101] + ux2*u[2];
        HMcoll[M021] = CHMcoll[M021] + u[2]*CHMcoll[M020] + 2.*u[1]*CHMcoll[M011] + uy2*u[2];
        HMcoll[M120] = CHMcoll[M120] + u[0]*CHMcoll[M020] + 2.*u[1]*CHMcoll[M110] + u[0]*uy2;
        HMcoll[M102] = CHMcoll[M102] + u[0]*CHMcoll[M002] + 2.*u[2]*CHMcoll[M101] + u[0]*uz2;
        HMcoll[M012] = CHMcoll[M012] + u[1]*CHMcoll[M002] + 2.*u[2]*CHMcoll[M011] + u[1]*uz2;
        
        HMcoll[M220] = CHMcoll[M220] + 2.*u[1]*CHMcoll[M210] + 2.*u[0]*CHMcoll[M120] + uy2*CHMcoll[M200] + ux2*CHMcoll[M020] + 4.*u[0]*u[1]*CHMcoll[M110] + ux2*uy2;
        HMcoll[M202] = CHMcoll[M202] + 2.*u[2]*CHMcoll[M201] + 2.*u[0]*CHMcoll[M102] + uz2*CHMcoll[M200] + ux2*CHMcoll[M002] + 4.*u[0]*u[2]*CHMcoll[M101] + ux2*uz2;
        HMcoll[M022] = CHMcoll[M022] + 2.*u[2]*CHMcoll[M021] + 2.*u[1]*CHMcoll[M012] + uz2*CHMcoll[M020] + uy2*CHMcoll[M002] + 4.*u[1]*u[2]*CHMcoll[M011] + uy2*uz2;

        // Come back to RMcoll using relationships between HMs and RMs
        RMcoll[M200] = HMcoll[M200] + cs2;
        RMcoll[M020] = HMcoll[M020] + cs2;
        RMcoll[M002] = HMcoll[M002] + cs2;
        
        RMcoll[M110] = HMcoll[M110];
        RMcoll[M101] = HMcoll[M101];
        RMcoll[M011] = HMcoll[M011];

        RMcoll[M210] = HMcoll[M210] + cs2*u[1];
        RMcoll[M201] = HMcoll[M201] + cs2*u[2];
        RMcoll[M021] = HMcoll[M021] + cs2*u[2];
        RMcoll[M120] = HMcoll[M120] + cs2*u[0];
        RMcoll[M102] = HMcoll[M102] + cs2*u[0];
        RMcoll[M012] = HMcoll[M012] + cs2*u[1];
        
        RMcoll[M220] = HMcoll[M220] + cs2*(HMcoll[M200] + HMcoll[M020]) + cs4;
        RMcoll[M202] = HMcoll[M202] + cs2*(HMcoll[M200] + HMcoll[M002]) + cs4;
        RMcoll[M022] = HMcoll[M022] + cs2*(HMcoll[M020] + HMcoll[M002]) + cs4;

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_opp_00 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00     =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_opp_00;

        double pop_out_opp_01 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01     =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_opp_01;

        double pop_out_opp_02 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02     =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_opp_02;

        double pop_out_opp_03 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04     =  0.5*rho * (-RMcoll[M110]                - RMcoll[M120]) + pop_out_opp_03;
        double pop_out_opp_04 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])                + pop_out_opp_03;
        double pop_out_03     =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_opp_03;

        double pop_out_opp_05 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06     =  0.5*rho * (-RMcoll[M101]                - RMcoll[M102]) + pop_out_opp_05;
        double pop_out_opp_06 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])                + pop_out_opp_05;
        double pop_out_05     =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_opp_05;

        double pop_out_opp_07 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08     =  0.5*rho * (-RMcoll[M011]                - RMcoll[M012]) + pop_out_opp_07;
        double pop_out_opp_08 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])                + pop_out_opp_07;
        double pop_out_07     =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_opp_07;


        int XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        int YY = iY;
        int ZZ = iZ;
        size_t nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 10) = pop_out_00 + f(nb, 0);
        }
        else {
            fout(nb, 0) = pop_out_00;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = iY;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 0) = pop_out_opp_00 + f(nb,10);
        }
        else {
            fout(nb,10) = pop_out_opp_00;
        }

        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 11) = pop_out_01 + f(nb, 1);
        }
        else {
            fout(nb, 1) = pop_out_01;
        }

        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 1) = pop_out_opp_01 + f(nb,11);
        }
        else {
            fout(nb,11) = pop_out_opp_01;
        }

        XX = iX;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 12) = pop_out_02 + f(nb, 2);
        }
        else {
            fout(nb, 2) = pop_out_02;
        }

        XX = iX;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 2) = pop_out_opp_02 + f(nb,12);
        }
        else {
            fout(nb,12) = pop_out_opp_02;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 13) = pop_out_03 + f(nb, 3);
        }
        else {
            fout(nb, 3) = pop_out_03;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 3) = pop_out_opp_03 + f(nb,13);
        }
        else {
            fout(nb,13) = pop_out_opp_03;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 14) = pop_out_04 + f(nb, 4);
        }
        else {
            fout(nb, 4) = pop_out_04;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 4) = pop_out_opp_04 + f(nb,14);
        }
        else {
            fout(nb,14) = pop_out_opp_04;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 15) = pop_out_05 + f(nb, 5);
        }
        else {
            fout(nb, 5) = pop_out_05;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 5) = pop_out_opp_05 + f(nb,15);
        }
        else {
            fout(nb,15) = pop_out_opp_05;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 16) = pop_out_06 + f(nb, 6);
        }
        else {
            fout(nb, 6) = pop_out_06;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 6) = pop_out_opp_06 + f(nb,16);
        }
        else {
            fout(nb,16) = pop_out_opp_06;
        }

        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 17) = pop_out_07 + f(nb, 7);
        }
        else {
            fout(nb, 7) = pop_out_07;
        }

        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 7) = pop_out_opp_07 + f(nb,17);
        }
        else {
            fout(nb,17) = pop_out_opp_07;
        }

        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 18) = pop_out_08 + f(nb, 8);
        }
        else {
            fout(nb, 8) = pop_out_08;
        }

        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 8) = pop_out_opp_08 + f(nb,18);
        }
        else {
            fout(nb,18) = pop_out_opp_08;
        }


        fout(i, 9) =  pop_out_09;
    }


    void iterateCHM(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[iX, iY, iZ] = i_to_xyz(i);
            // auto[rho, u] = macro(f0);
            // auto CHM = computeCHM(f0, rho, u);
            // auto CHM = computeCHMopt(f0, rho, u);
            auto CHM = computeCHMopt2(f0);
            double rho = CHM[M000];
            std::array<double, 3> u = {CHM[M100], CHM[M010], CHM[M001]}; 
            auto CHMeq = computeCHMeq();
            collideAndStreamCHM(i, iX, iY, iZ, rho, u, CHM, CHMeq);
        }
    }

    void operator() (double& f0) {
        iterateCHM(f0);
    }
};

} // namespace twopop_soa_chm


