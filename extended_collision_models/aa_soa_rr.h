// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>

namespace aa_soa_rr {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 19 * nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega1;
    Dim dim;
    LBModel model;
    bool periodic = false;
    double omegaBulk = 0.;
    double omega2 = omega1;
    double omega3 = omega1;
    double omega4 = omega1;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 19; ++k) {
            f(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 19; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = f(i, 0) + f(i, 3) + f(i, 4) + f(i, 5) + f(i, 6);
        double X_P1 = f(i,10) + f(i,13) + f(i,14) + f(i,15) + f(i,16);
        double X_0  = f(i, 9) + f(i, 1) + f(i, 2) + f(i, 7) + f(i, 8) + f(i,11) + f(i,12) + f(i,17) + f(i,18);

        double Y_M1 = f(i, 1) + f(i, 3) + f(i, 7) + f(i, 8) + f(i,14);
        double Y_P1 = f(i, 4) + f(i,11) + f(i,13) + f(i,17) + f(i,18);

        double Z_M1 = f(i, 2) + f(i, 5) + f(i, 7) + f(i,16) + f(i,18);
        double Z_P1 = f(i, 6) + f(i, 8) + f(i,12) + f(i,15) + f(i,17);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    }

    auto macropop (std::array<double, 19> const& pop) {
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[ 9] + pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[14];
        double Y_P1 = pop[ 4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[16] + pop[18];
        double Z_P1 = pop[ 6] + pop[ 8] + pop[12] + pop[15] + pop[17];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };

    // We only need second-order moments for the RR procedure (optimized computation)
    auto computeRRopt(std::array<double, 19> const& pop, double const& rho) {

        std::array<double, 19> RR;
        std::fill(RR.begin(), RR.end(), 0.);

        double cs2 = 1./3.;
        double invRho = 1. / rho;

        RR[M200] = invRho * (  pop[0] + pop[3] + pop[4] + pop[5] + pop[6] + pop[10] + pop[13] + pop[14] + pop[15] + pop[16]);
        RR[M020] = invRho * (  pop[1] + pop[3] + pop[4] + pop[7] + pop[8] + pop[11] + pop[13] + pop[14] + pop[17] + pop[18]);
        RR[M002] = invRho * (  pop[2] + pop[5] + pop[6] + pop[7] + pop[8] + pop[12] + pop[15] + pop[16] + pop[17] + pop[18]);
        
        RR[M200] -= cs2;
        RR[M020] -= cs2;
        RR[M002] -= cs2;

        RR[M110] = invRho * (  pop[3] - pop[4] + pop[13] - pop[14]);
        RR[M101] = invRho * (  pop[5] - pop[6] + pop[15] - pop[16]);
        RR[M011] = invRho * (  pop[7] - pop[8] + pop[17] - pop[18]);

        return RR;
    }

    // We only need second-order moments for the RR procedure (optimized computation)
    // Further optimization through merge with the computation of macros
    auto computeRRopt2(std::array<double, 19> const& pop) {
        std::array<double, 19> RR;
        std::fill(RR.begin(), RR.end(), 0.);

        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[ 9] + pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[14];
        double Y_P1 = pop[ 4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[16] + pop[18];
        double Z_P1 = pop[ 6] + pop[ 8] + pop[12] + pop[15] + pop[17];

        // Order 0
        RR[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / RR[M000];

        // Order 1
        RR[M100] = invRho * (X_P1 - X_M1);
        RR[M010] = invRho * (Y_P1 - Y_M1); 
        RR[M001] = invRho * (Z_P1 - Z_M1);

        // Order 2
        double cs2 = 1./3.;

        RR[M200] = invRho * (X_M1 + X_P1);
        RR[M020] = invRho * (Y_M1 + Y_P1);
        RR[M002] = invRho * (Z_M1 + Z_P1);
        
        RR[M200] -= cs2;
        RR[M020] -= cs2;
        RR[M002] -= cs2;

        RR[M110] = invRho * (  pop[3] - pop[4] + pop[13] - pop[14]);
        RR[M101] = invRho * (  pop[5] - pop[6] + pop[15] - pop[16]);
        RR[M011] = invRho * (  pop[7] - pop[8] + pop[17] - pop[18]);

        return RR;
    }

    auto computeRReq(std::array<double, 3> const& u) {

        std::array<double, 19> RReq;

        // Order 2
        RReq[M200] = u[0] * u[0];
        RReq[M020] = u[1] * u[1];
        RReq[M002] = u[2] * u[2];
        RReq[M110] = u[0] * u[1];
        RReq[M101] = u[0] * u[2];
        RReq[M011] = u[1] * u[2];
        // Order 3
        RReq[M210] = RReq[M200] * u[1];
        RReq[M201] = RReq[M200] * u[2];
        RReq[M021] = RReq[M020] * u[2];
        RReq[M120] = RReq[M020] * u[0];
        RReq[M102] = RReq[M002] * u[0];
        RReq[M012] = RReq[M002] * u[1];
        // Order 4
        RReq[M220] = RReq[M200] * RReq[M020];
        RReq[M202] = RReq[M200] * RReq[M002];
        RReq[M022] = RReq[M020] * RReq[M002];

        return RReq;
    }
};

struct Even : public LBM {

    auto collideRR(int i, double rho, std::array<double, 3> const& u, std::array<double, 19> const& RR, std::array<double, 19> const& RReq)
    {
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        // Post-collision and Nonequilibrium moments.
        std::array<double, 19> RRneq;
        std::array<double, 19> RRcoll;
        std::array<double, 19> RMcoll;

        // Recursive computation of nonequilibrium Hermite moments
        // Order 2 (standard way to compute them)
        RRneq[M200] = RR[M200] - RReq[M200];
        RRneq[M020] = RR[M020] - RReq[M020];
        RRneq[M002] = RR[M002] - RReq[M002];
        RRneq[M110] = RR[M110] - RReq[M110];
        RRneq[M101] = RR[M101] - RReq[M101];
        RRneq[M011] = RR[M011] - RReq[M011];

        // Order 3 (reconstruction using Chapman-Enskog formulas)
        RRneq[M210] = u[1]*RRneq[M200] + 2.*u[0]*RRneq[M110];
        RRneq[M201] = u[2]*RRneq[M200] + 2.*u[0]*RRneq[M101];
        RRneq[M021] = u[2]*RRneq[M020] + 2.*u[1]*RRneq[M011];
        RRneq[M120] = u[0]*RRneq[M020] + 2.*u[1]*RRneq[M110];
        RRneq[M102] = u[0]*RRneq[M002] + 2.*u[2]*RRneq[M101];
        RRneq[M012] = u[1]*RRneq[M002] + 2.*u[2]*RRneq[M011];

        // Order 4 (reconstruction using Chapman-Enskog formulas)
        RRneq[M220] = RReq[M020]*RRneq[M200] + RReq[M200]*RRneq[M020] + 4.*RReq[M110]*RRneq[M110];
        RRneq[M202] = RReq[M002]*RRneq[M200] + RReq[M200]*RRneq[M002] + 4.*RReq[M101]*RRneq[M101];
        RRneq[M022] = RReq[M002]*RRneq[M020] + RReq[M020]*RRneq[M002] + 4.*RReq[M011]*RRneq[M011];

        // Collision in the Hermite moment space
        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity
            RRcoll[M200] = RR[M200] - omega1 * RRneq[M200];
            RRcoll[M020] = RR[M020] - omega1 * RRneq[M020];
            RRcoll[M002] = RR[M002] - omega1 * RRneq[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            RRcoll[M200] = RR[M200] - omegaPlus  * RRneq[M200] - omegaMinus * RRneq[M020] - omegaMinus * RRneq[M002] ;
            RRcoll[M020] = RR[M020] - omegaMinus * RRneq[M200] - omegaPlus  * RRneq[M020] - omegaMinus * RRneq[M002] ;
            RRcoll[M002] = RR[M002] - omegaMinus * RRneq[M200] - omegaMinus * RRneq[M020] - omegaPlus  * RRneq[M002] ;
        }
        
        RRcoll[M110] = RR[M110] - omega2 * RRneq[M110];
        RRcoll[M101] = RR[M101] - omega2 * RRneq[M101];
        RRcoll[M011] = RR[M011] - omega2 * RRneq[M011];

        // Order 3 (Optimization: use RReq to avoid the computation of 3rd-order RR moments)
        RRcoll[M210] = RReq[M210] + (1. - omega3) * RRneq[M210];
        RRcoll[M201] = RReq[M201] + (1. - omega3) * RRneq[M201];
        RRcoll[M021] = RReq[M021] + (1. - omega3) * RRneq[M021];
        RRcoll[M120] = RReq[M120] + (1. - omega3) * RRneq[M120];
        RRcoll[M102] = RReq[M102] + (1. - omega3) * RRneq[M102];
        RRcoll[M012] = RReq[M012] + (1. - omega3) * RRneq[M012];

        // Order 4 (Optimization: use RReq to avoid the computation of 4th-order RR moments)
        RRcoll[M220] = RReq[M220] + (1. - omega4) * RRneq[M220];
        RRcoll[M202] = RReq[M202] + (1. - omega4) * RRneq[M202];
        RRcoll[M022] = RReq[M022] + (1. - omega4) * RRneq[M022];

        // Come back to RMcoll using relationships between RRs and RMs
        RMcoll[M200] = RRcoll[M200] + cs2;
        RMcoll[M020] = RRcoll[M020] + cs2;
        RMcoll[M002] = RRcoll[M002] + cs2;
        
        RMcoll[M110] = RRcoll[M110];
        RMcoll[M101] = RRcoll[M101];
        RMcoll[M011] = RRcoll[M011];

        RMcoll[M210] = RRcoll[M210] + cs2*u[1];
        RMcoll[M201] = RRcoll[M201] + cs2*u[2];
        RMcoll[M021] = RRcoll[M021] + cs2*u[2];
        RMcoll[M120] = RRcoll[M120] + cs2*u[0];
        RMcoll[M102] = RRcoll[M102] + cs2*u[0];
        RMcoll[M012] = RRcoll[M012] + cs2*u[1];
        
        RMcoll[M220] = RRcoll[M220] + cs2*(RRcoll[M200] + RRcoll[M020]) + cs4;
        RMcoll[M202] = RRcoll[M202] + cs2*(RRcoll[M200] + RRcoll[M002]) + cs4;
        RMcoll[M022] = RRcoll[M022] + cs2*(RRcoll[M020] + RRcoll[M002]) + cs4;

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_10 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00 =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_10;

        double pop_out_11 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01 =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_11;

        double pop_out_12 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02 =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_12;

        double pop_out_13 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04 =  0.5*rho * (-RMcoll[M110]              - RMcoll[M120]) + pop_out_13;
        double pop_out_14 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])              + pop_out_13;
        double pop_out_03 =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_13;

        double pop_out_15 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06 =  0.5*rho * (-RMcoll[M101]              - RMcoll[M102]) + pop_out_15;
        double pop_out_16 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])              + pop_out_15;
        double pop_out_05 =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_15;

        double pop_out_17 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08 =  0.5*rho * (-RMcoll[M011]              - RMcoll[M012]) + pop_out_17;
        double pop_out_18 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])              + pop_out_17;
        double pop_out_07 =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_17;

        f(i,  0) = pop_out_10;
        f(i, 10) = pop_out_00;

        f(i,  1) = pop_out_11;
        f(i, 11) = pop_out_01;

        f(i,  2) = pop_out_12;
        f(i, 12) = pop_out_02;

        f(i,  3) = pop_out_13;
        f(i, 13) = pop_out_03;

        f(i,  4) = pop_out_14;
        f(i, 14) = pop_out_04;

        f(i,  5) = pop_out_15;
        f(i, 15) = pop_out_05;

        f(i,  6) = pop_out_16;
        f(i, 16) = pop_out_06;

        f(i,  7) = pop_out_17;
        f(i, 17) = pop_out_07;

        f(i,  8) = pop_out_18;
        f(i, 18) = pop_out_08;

        f(i, 9) = pop_out_09;
    }


    void iterateRR(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            std::array<double, 19> pop;
            for (int k = 0; k < 19; ++k) {
                pop[k] = f(i, k);
            }
            //auto[rho, u] = macropop(pop);
            //auto RR = computeRRopt(pop, rho);
            auto RR = computeRRopt2(pop);
            double rho = RR[M000];
            std::array<double, 3> u = {RR[M100], RR[M010], RR[M001]};
            auto RReq = computeRReq(u);
            collideRR(i, rho, u, RR, RReq);
        }
    }

    void operator() (double& f0) {
        iterateRR(f0);
    }
};

struct Odd : public LBM {

    auto collideRR(std::array<double, 19>& pop, double rho, std::array<double, 3> const& u, std::array<double, 19> const& RR, std::array<double, 19> const& RReq)
    {
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        // Post-collision and Nonequilibrium moments.
        std::array<double, 19> RRneq;
        std::array<double, 19> RRcoll;
        std::array<double, 19> RMcoll;

        // Recursive computation of nonequilibrium Hermite moments
        // Order 2 (standard way to compute them)
        RRneq[M200] = RR[M200] - RReq[M200];
        RRneq[M020] = RR[M020] - RReq[M020];
        RRneq[M002] = RR[M002] - RReq[M002];
        RRneq[M110] = RR[M110] - RReq[M110];
        RRneq[M101] = RR[M101] - RReq[M101];
        RRneq[M011] = RR[M011] - RReq[M011];

        // Order 3 (reconstruction using Chapman-Enskog formulas)
        RRneq[M210] = u[1]*RRneq[M200] + 2.*u[0]*RRneq[M110];
        RRneq[M201] = u[2]*RRneq[M200] + 2.*u[0]*RRneq[M101];
        RRneq[M021] = u[2]*RRneq[M020] + 2.*u[1]*RRneq[M011];
        RRneq[M120] = u[0]*RRneq[M020] + 2.*u[1]*RRneq[M110];
        RRneq[M102] = u[0]*RRneq[M002] + 2.*u[2]*RRneq[M101];
        RRneq[M012] = u[1]*RRneq[M002] + 2.*u[2]*RRneq[M011];

        // Order 4 (reconstruction using Chapman-Enskog formulas)
        RRneq[M220] = RReq[M020]*RRneq[M200] + RReq[M200]*RRneq[M020] + 4.*RReq[M110]*RRneq[M110];
        RRneq[M202] = RReq[M002]*RRneq[M200] + RReq[M200]*RRneq[M002] + 4.*RReq[M101]*RRneq[M101];
        RRneq[M022] = RReq[M002]*RRneq[M020] + RReq[M020]*RRneq[M002] + 4.*RReq[M011]*RRneq[M011];

        // Collision in the Hermite moment space
        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity
            RRcoll[M200] = RR[M200] - omega1 * RRneq[M200];
            RRcoll[M020] = RR[M020] - omega1 * RRneq[M020];
            RRcoll[M002] = RR[M002] - omega1 * RRneq[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            RRcoll[M200] = RR[M200] - omegaPlus  * RRneq[M200] - omegaMinus * RRneq[M020] - omegaMinus * RRneq[M002] ;
            RRcoll[M020] = RR[M020] - omegaMinus * RRneq[M200] - omegaPlus  * RRneq[M020] - omegaMinus * RRneq[M002] ;
            RRcoll[M002] = RR[M002] - omegaMinus * RRneq[M200] - omegaMinus * RRneq[M020] - omegaPlus  * RRneq[M002] ;
        }
        
        RRcoll[M110] = RR[M110] - omega2 * RRneq[M110];
        RRcoll[M101] = RR[M101] - omega2 * RRneq[M101];
        RRcoll[M011] = RR[M011] - omega2 * RRneq[M011];

        // Order 3 (Optimization: use RReq to avoid the computation of 3rd-order RR moments)
        RRcoll[M210] = RReq[M210] + (1. - omega3) * RRneq[M210];
        RRcoll[M201] = RReq[M201] + (1. - omega3) * RRneq[M201];
        RRcoll[M021] = RReq[M021] + (1. - omega3) * RRneq[M021];
        RRcoll[M120] = RReq[M120] + (1. - omega3) * RRneq[M120];
        RRcoll[M102] = RReq[M102] + (1. - omega3) * RRneq[M102];
        RRcoll[M012] = RReq[M012] + (1. - omega3) * RRneq[M012];

        // Order 4 (Optimization: use RReq to avoid the computation of 4th-order RR moments)
        RRcoll[M220] = RReq[M220] + (1. - omega4) * RRneq[M220];
        RRcoll[M202] = RReq[M202] + (1. - omega4) * RRneq[M202];
        RRcoll[M022] = RReq[M022] + (1. - omega4) * RRneq[M022];

        // Come back to RMcoll using relationships between RRs and RMs
        RMcoll[M200] = RRcoll[M200] + cs2;
        RMcoll[M020] = RRcoll[M020] + cs2;
        RMcoll[M002] = RRcoll[M002] + cs2;
        
        RMcoll[M110] = RRcoll[M110];
        RMcoll[M101] = RRcoll[M101];
        RMcoll[M011] = RRcoll[M011];

        RMcoll[M210] = RRcoll[M210] + cs2*u[1];
        RMcoll[M201] = RRcoll[M201] + cs2*u[2];
        RMcoll[M021] = RRcoll[M021] + cs2*u[2];
        RMcoll[M120] = RRcoll[M120] + cs2*u[0];
        RMcoll[M102] = RRcoll[M102] + cs2*u[0];
        RMcoll[M012] = RRcoll[M012] + cs2*u[1];
        
        RMcoll[M220] = RRcoll[M220] + cs2*(RRcoll[M200] + RRcoll[M020]) + cs4;
        RMcoll[M202] = RRcoll[M202] + cs2*(RRcoll[M200] + RRcoll[M002]) + cs4;
        RMcoll[M022] = RRcoll[M022] + cs2*(RRcoll[M020] + RRcoll[M002]) + cs4;

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_10 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00 =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_10;

        double pop_out_11 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01 =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_11;

        double pop_out_12 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02 =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_12;

        double pop_out_13 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04 =  0.5*rho * (-RMcoll[M110]              - RMcoll[M120]) + pop_out_13;
        double pop_out_14 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])              + pop_out_13;
        double pop_out_03 =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_13;

        double pop_out_15 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06 =  0.5*rho * (-RMcoll[M101]              - RMcoll[M102]) + pop_out_15;
        double pop_out_16 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])              + pop_out_15;
        double pop_out_05 =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_15;

        double pop_out_17 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08 =  0.5*rho * (-RMcoll[M011]              - RMcoll[M012]) + pop_out_17;
        double pop_out_18 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])              + pop_out_17;
        double pop_out_07 =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_17;


        pop[ 0] = pop_out_00;
        pop[10] = pop_out_10;

        pop[ 1] = pop_out_01;
        pop[11] = pop_out_11;

        pop[ 2] = pop_out_02;
        pop[12] = pop_out_12;

        pop[ 3] = pop_out_03;
        pop[13] = pop_out_13;

        pop[ 4] = pop_out_04;
        pop[14] = pop_out_14;

        pop[ 5] = pop_out_05;
        pop[15] = pop_out_15;

        pop[ 6] = pop_out_06;
        pop[16] = pop_out_16;

        pop[ 7] = pop_out_07;
        pop[17] = pop_out_17;

        pop[ 8] = pop_out_08;
        pop[18] = pop_out_18;

        pop[ 9] = pop_out_09;
    }

    auto streamingPull(int i, int iX, int iY, int iZ, std::array<double, 19>& pop)
    {
        if(periodic){
            for (int k = 0; k < 19; ++k) {
                int XX = (iX - c[k][0] + dim.nx) % dim.nx;
                int YY = (iY - c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ - c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
        else {
            for (int k = 0; k < 19; ++k) {
                int XX = iX - c[k][0];
                int YY = iY - c[k][1];
                int ZZ = iZ - c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
    }

    auto streamingPush(int i, int iX, int iY, int iZ, std::array<double, 19>& pop)
    {
        if(periodic){
            for (int k = 0; k < 19; ++k) {
                int XX = (iX + c[k][0] + dim.nx) % dim.nx;
                int YY = (iY + c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ + c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
        else {
            for (int k = 0; k < 19; ++k) {
                int XX = iX + c[k][0];
                int YY = iY + c[k][1];
                int ZZ = iZ + c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
    }

    void iterateRR(double& f0) {
        int i = &f0 - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            std::array<double, 19> pop;

            streamingPull(i, iX, iY, iZ, pop);

            //auto[rho, u] = macropop(pop);
            //auto RR = computeRRopt(pop, rho);
            auto RR = computeRRopt2(pop);
            double rho = RR[M000];
            std::array<double, 3> u = {RR[M100], RR[M010], RR[M001]};

            auto RReq = computeRReq(u);

            collideRR(pop, rho, u, RR, RReq);

            streamingPush(i, iX, iY, iZ, pop);
        }
    }

    void operator() (double& f0) {
        iterateRR(f0);
    }
};

} // namespace aa_soa_rr
