// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>

namespace aa_soa_chm {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 19 * nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega1;
    Dim dim;
    LBModel model;
    bool periodic = false;
    double omegaBulk = 0.;
    double omega2 = omega1;
    double omega3 = omega1;
    double omega4 = omega1;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 19; ++k) {
            f(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 19; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = f(i, 0) + f(i, 3) + f(i, 4) + f(i, 5) + f(i, 6);
        double X_P1 = f(i,10) + f(i,13) + f(i,14) + f(i,15) + f(i,16);
        double X_0  = f(i, 9) + f(i, 1) + f(i, 2) + f(i, 7) + f(i, 8) + f(i,11) + f(i,12) + f(i,17) + f(i,18);

        double Y_M1 = f(i, 1) + f(i, 3) + f(i, 7) + f(i, 8) + f(i,14);
        double Y_P1 = f(i, 4) + f(i,11) + f(i,13) + f(i,17) + f(i,18);

        double Z_M1 = f(i, 2) + f(i, 5) + f(i, 7) + f(i,16) + f(i,18);
        double Z_P1 = f(i, 6) + f(i, 8) + f(i,12) + f(i,15) + f(i,17);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    }

    auto macropop (std::array<double, 19> const& pop) {
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[ 9] + pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[14];
        double Y_P1 = pop[ 4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[16] + pop[18];
        double Z_P1 = pop[ 6] + pop[ 8] + pop[12] + pop[15] + pop[17];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };

    auto computeCHMopt(std::array<double, 19> const& pop, double const& rho, std::array<double, 3> const& u) {

        std::array<double, 19> CHM;

        // Optimized way to compute raw moments
        double invRho = 1./rho;
        // Order 4
        CHM[M220] = invRho * (pop[ 3] + pop[ 4] + pop[13] + pop[14]);
        CHM[M202] = invRho * (pop[ 5] + pop[ 6] + pop[15] + pop[16]);
        CHM[M022] = invRho * (pop[ 7] + pop[ 8] + pop[17] + pop[18]);
        // Order 2
        CHM[M200] = invRho * (pop[ 0] + pop[10]) + CHM[M220] + CHM[M202];
        CHM[M020] = invRho * (pop[ 1] + pop[11]) + CHM[M220] + CHM[M022];
        CHM[M002] = invRho * (pop[ 2] + pop[12]) + CHM[M202] + CHM[M022];
        CHM[M110] = CHM[M220] - 2.*invRho * (pop[ 4] + pop[14]);
        CHM[M101] = CHM[M202] - 2.*invRho * (pop[ 6] + pop[16]);
        CHM[M011] = CHM[M022] - 2.*invRho * (pop[ 8] + pop[18]);
        // Order 3
        CHM[M210] = CHM[M220] - 2.*invRho * (pop[ 3] + pop[14]);
        CHM[M201] = CHM[M202] - 2.*invRho * (pop[ 5] + pop[16]);
        CHM[M021] = CHM[M022] - 2.*invRho * (pop[ 7] + pop[18]);
        CHM[M120] = CHM[M220] - 2.*invRho * (pop[ 3] + pop[ 4]);
        CHM[M102] = CHM[M202] - 2.*invRho * (pop[ 5] + pop[ 6]);
        CHM[M012] = CHM[M022] - 2.*invRho * (pop[ 7] + pop[ 8]);

        // Compute central moments from raw moments using binomial formulas
        double ux2 = u[0]*u[0];
        double uy2 = u[1]*u[1];
        double uz2 = u[2]*u[2];
        double uxy = u[0]*u[1];
        double uxz = u[0]*u[2];
        double uyz = u[1]*u[2];

        CHM[M200] -= ux2;
        CHM[M020] -= uy2;
        CHM[M002] -= uz2;
        
        CHM[M110] -= uxy;
        CHM[M101] -= uxz;
        CHM[M011] -= uyz;

        CHM[M210] -= (u[1]*CHM[M200] + 2.*u[0]*CHM[M110] + ux2*u[1]);
        CHM[M201] -= (u[2]*CHM[M200] + 2.*u[0]*CHM[M101] + ux2*u[2]);
        CHM[M021] -= (u[2]*CHM[M020] + 2.*u[1]*CHM[M011] + uy2*u[2]);
        CHM[M120] -= (u[0]*CHM[M020] + 2.*u[1]*CHM[M110] + u[0]*uy2);
        CHM[M102] -= (u[0]*CHM[M002] + 2.*u[2]*CHM[M101] + u[0]*uz2);
        CHM[M012] -= (u[1]*CHM[M002] + 2.*u[2]*CHM[M011] + u[1]*uz2);
        
        CHM[M220] -= (2.*u[1]*CHM[M210] + 2.*u[0]*CHM[M120] + uy2*CHM[M200] + ux2*CHM[M020] + 4.*uxy*CHM[M110] + ux2*uy2);
        CHM[M202] -= (2.*u[2]*CHM[M201] + 2.*u[0]*CHM[M102] + uz2*CHM[M200] + ux2*CHM[M002] + 4.*uxz*CHM[M101] + ux2*uz2);
        CHM[M022] -= (2.*u[2]*CHM[M021] + 2.*u[1]*CHM[M012] + uz2*CHM[M020] + uy2*CHM[M002] + 4.*uyz*CHM[M011] + uy2*uz2);

        // Compute central Hermite moments from central moments
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        CHM[M200] -= cs2;
        CHM[M020] -= cs2;
        CHM[M002] -= cs2;

        CHM[M220] -= (cs2*(CHM[M200] + CHM[M020]) + cs4);
        CHM[M202] -= (cs2*(CHM[M200] + CHM[M002]) + cs4);
        CHM[M022] -= (cs2*(CHM[M020] + CHM[M002]) + cs4);

        return CHM;
    }

    // Further optimization through merge with the computation of macros
    auto computeCHMopt2(std::array<double, 19> const& pop) {
        std::array<double, 19> CHM;
        std::fill(CHM.begin(), CHM.end(), 0.);

        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[ 9] + pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[14];
        double Y_P1 = pop[ 4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[16] + pop[18];
        double Z_P1 = pop[ 6] + pop[ 8] + pop[12] + pop[15] + pop[17];

        // Order 0
        CHM[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / CHM[M000];
        double two_invRho= 2.*invRho;
        // Order 1
        CHM[M100] = invRho * (X_P1 - X_M1);
        CHM[M010] = invRho * (Y_P1 - Y_M1); 
        CHM[M001] = invRho * (Z_P1 - Z_M1);
        // Order 2
        CHM[M200] = invRho * (X_M1 + X_P1);
        CHM[M020] = invRho * (Y_M1 + Y_P1);
        CHM[M002] = invRho * (Z_M1 + Z_P1);
        CHM[M110] = invRho * ( pop[ 3] - pop[ 4] + pop[13] - pop[14]);
        CHM[M101] = invRho * ( pop[ 5] - pop[ 6] + pop[15] - pop[16]);
        CHM[M011] = invRho * ( pop[ 7] - pop[ 8] + pop[17] - pop[18]);
        // Order 3
        CHM[M210] = CHM[M110] - two_invRho * (pop[ 3] - pop[ 4]);
        CHM[M201] = CHM[M101] - two_invRho * (pop[ 5] - pop[ 6]);
        CHM[M021] = CHM[M011] - two_invRho * (pop[ 7] - pop[ 8]);
        CHM[M120] = CHM[M110] - two_invRho * (pop[ 3] - pop[14]);
        CHM[M102] = CHM[M101] - two_invRho * (pop[ 5] - pop[16]);
        CHM[M012] = CHM[M011] - two_invRho * (pop[ 7] - pop[18]);
        // Order 4
        CHM[M220] = CHM[M110] + two_invRho * (pop[ 4] + pop[14]);
        CHM[M202] = CHM[M101] + two_invRho * (pop[ 6] + pop[16]);
        CHM[M022] = CHM[M011] + two_invRho * (pop[ 8] + pop[18]);

        // Compute central moments from raw moments using binomial formulas
        double ux2 = CHM[M100]*CHM[M100];
        double uy2 = CHM[M010]*CHM[M010];
        double uz2 = CHM[M001]*CHM[M001];
        double uxy = CHM[M100]*CHM[M010];
        double uxz = CHM[M100]*CHM[M001];
        double uyz = CHM[M010]*CHM[M001];

        CHM[M200] -= ux2;
        CHM[M020] -= uy2;
        CHM[M002] -= uz2;
        
        CHM[M110] -= uxy;
        CHM[M101] -= uxz;
        CHM[M011] -= uyz;

        CHM[M210] -= (CHM[M010]*CHM[M200] + 2.*CHM[M100]*CHM[M110] + ux2*CHM[M010]);
        CHM[M201] -= (CHM[M001]*CHM[M200] + 2.*CHM[M100]*CHM[M101] + ux2*CHM[M001]);
        CHM[M021] -= (CHM[M001]*CHM[M020] + 2.*CHM[M010]*CHM[M011] + uy2*CHM[M001]);
        CHM[M120] -= (CHM[M100]*CHM[M020] + 2.*CHM[M010]*CHM[M110] + CHM[M100]*uy2);
        CHM[M102] -= (CHM[M100]*CHM[M002] + 2.*CHM[M001]*CHM[M101] + CHM[M100]*uz2);
        CHM[M012] -= (CHM[M010]*CHM[M002] + 2.*CHM[M001]*CHM[M011] + CHM[M010]*uz2);
        
        CHM[M220] -= (2.*CHM[M010]*CHM[M210] + 2.*CHM[M100]*CHM[M120] + uy2*CHM[M200] + ux2*CHM[M020] + 4.*uxy*CHM[M110] + ux2*uy2);
        CHM[M202] -= (2.*CHM[M001]*CHM[M201] + 2.*CHM[M100]*CHM[M102] + uz2*CHM[M200] + ux2*CHM[M002] + 4.*uxz*CHM[M101] + ux2*uz2);
        CHM[M022] -= (2.*CHM[M001]*CHM[M021] + 2.*CHM[M010]*CHM[M012] + uz2*CHM[M020] + uy2*CHM[M002] + 4.*uyz*CHM[M011] + uy2*uz2);

        // Compute central Hermite moments from central moments
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        CHM[M200] -= cs2;
        CHM[M020] -= cs2;
        CHM[M002] -= cs2;

        CHM[M220] -= (cs2*(CHM[M200] + CHM[M020]) + cs4);
        CHM[M202] -= (cs2*(CHM[M200] + CHM[M002]) + cs4);
        CHM[M022] -= (cs2*(CHM[M020] + CHM[M002]) + cs4);

        return CHM;
    }

    // The function below has only an educational purpose since it is discarded
    // during the compilation because CHMeq are not used in collideCHM()
    auto computeCHMeq() {

        std::array<double, 19> CHMeq;

        // Order 2
        CHMeq[M200] = 0.;
        CHMeq[M020] = 0.;
        CHMeq[M002] = 0.;
        CHMeq[M110] = 0.;
        CHMeq[M101] = 0.;
        CHMeq[M011] = 0.;
        // Order 3
        CHMeq[M210] = 0.;
        CHMeq[M201] = 0.;
        CHMeq[M021] = 0.;
        CHMeq[M120] = 0.;
        CHMeq[M102] = 0.;
        CHMeq[M012] = 0.;
        // Order 4
        CHMeq[M220] = 0.;
        CHMeq[M202] = 0.;
        CHMeq[M022] = 0.;

        return CHMeq;
    }
};

struct Even : public LBM {

    auto collideCHM(int i, double rho, std::array<double, 3> const& u, std::array<double, 19> const& CHM, std::array<double, 19> const& CHMeq)
    {
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        // Post-collision moments.
        std::array<double, 19> RMcoll;
        std::array<double, 19> HMcoll;
        std::array<double, 19> CHMcoll;

        // Collision in the central Hermite moment space (CHMeq are replaced by their values)
        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity
            CHMcoll[M200] = (1. - omega1) * CHM[M200];
            CHMcoll[M020] = (1. - omega1) * CHM[M020];
            CHMcoll[M002] = (1. - omega1) * CHM[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaBulk = omega1;
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            CHMcoll[M200] = CHM[M200] - omegaPlus  * (CHM[M200]) - omegaMinus * (CHM[M020]) - omegaMinus * (CHM[M002]) ;
            CHMcoll[M020] = CHM[M020] - omegaMinus * (CHM[M200]) - omegaPlus  * (CHM[M020]) - omegaMinus * (CHM[M002]) ;
            CHMcoll[M002] = CHM[M002] - omegaMinus * (CHM[M200]) - omegaMinus * (CHM[M020]) - omegaPlus  * (CHM[M002]) ;
        }

        CHMcoll[M110] = (1. - omega2) * CHM[M110];
        CHMcoll[M101] = (1. - omega2) * CHM[M101];
        CHMcoll[M011] = (1. - omega2) * CHM[M011];

        // Order 3
        CHMcoll[M210] = (1. - omega3) * CHM[M210];
        CHMcoll[M201] = (1. - omega3) * CHM[M201];
        CHMcoll[M021] = (1. - omega3) * CHM[M021];
        CHMcoll[M120] = (1. - omega3) * CHM[M120];
        CHMcoll[M102] = (1. - omega3) * CHM[M102];
        CHMcoll[M012] = (1. - omega3) * CHM[M012];
        
        // Order 4
        CHMcoll[M220] = (1. - omega4) * CHM[M220];
        CHMcoll[M202] = (1. - omega4) * CHM[M202];
        CHMcoll[M022] = (1. - omega4) * CHM[M022];

        // Come back to HMcoll using relationships between CHMs and HMs

        double ux2 = u[0]*u[0];
        double uy2 = u[1]*u[1];
        double uz2 = u[2]*u[2];

        HMcoll[M200] = CHMcoll[M200] + ux2;
        HMcoll[M020] = CHMcoll[M020] + uy2;
        HMcoll[M002] = CHMcoll[M002] + uz2;
        
        HMcoll[M110] = CHMcoll[M110] + u[0]*u[1];
        HMcoll[M101] = CHMcoll[M101] + u[0]*u[2];
        HMcoll[M011] = CHMcoll[M011] + u[1]*u[2];

        HMcoll[M210] = CHMcoll[M210] + u[1]*CHMcoll[M200] + 2.*u[0]*CHMcoll[M110] + ux2*u[1];
        HMcoll[M201] = CHMcoll[M201] + u[2]*CHMcoll[M200] + 2.*u[0]*CHMcoll[M101] + ux2*u[2];
        HMcoll[M021] = CHMcoll[M021] + u[2]*CHMcoll[M020] + 2.*u[1]*CHMcoll[M011] + uy2*u[2];
        HMcoll[M120] = CHMcoll[M120] + u[0]*CHMcoll[M020] + 2.*u[1]*CHMcoll[M110] + u[0]*uy2;
        HMcoll[M102] = CHMcoll[M102] + u[0]*CHMcoll[M002] + 2.*u[2]*CHMcoll[M101] + u[0]*uz2;
        HMcoll[M012] = CHMcoll[M012] + u[1]*CHMcoll[M002] + 2.*u[2]*CHMcoll[M011] + u[1]*uz2;
        
        HMcoll[M220] = CHMcoll[M220] + 2.*u[1]*CHMcoll[M210] + 2.*u[0]*CHMcoll[M120] + uy2*CHMcoll[M200] + ux2*CHMcoll[M020] + 4.*u[0]*u[1]*CHMcoll[M110] + ux2*uy2;
        HMcoll[M202] = CHMcoll[M202] + 2.*u[2]*CHMcoll[M201] + 2.*u[0]*CHMcoll[M102] + uz2*CHMcoll[M200] + ux2*CHMcoll[M002] + 4.*u[0]*u[2]*CHMcoll[M101] + ux2*uz2;
        HMcoll[M022] = CHMcoll[M022] + 2.*u[2]*CHMcoll[M021] + 2.*u[1]*CHMcoll[M012] + uz2*CHMcoll[M020] + uy2*CHMcoll[M002] + 4.*u[1]*u[2]*CHMcoll[M011] + uy2*uz2;

        // Come back to RMcoll using relationships between HMs and RMs
        RMcoll[M200] = HMcoll[M200] + cs2;
        RMcoll[M020] = HMcoll[M020] + cs2;
        RMcoll[M002] = HMcoll[M002] + cs2;
        
        RMcoll[M110] = HMcoll[M110];
        RMcoll[M101] = HMcoll[M101];
        RMcoll[M011] = HMcoll[M011];

        RMcoll[M210] = HMcoll[M210] + cs2*u[1];
        RMcoll[M201] = HMcoll[M201] + cs2*u[2];
        RMcoll[M021] = HMcoll[M021] + cs2*u[2];
        RMcoll[M120] = HMcoll[M120] + cs2*u[0];
        RMcoll[M102] = HMcoll[M102] + cs2*u[0];
        RMcoll[M012] = HMcoll[M012] + cs2*u[1];
        
        RMcoll[M220] = HMcoll[M220] + cs2*(HMcoll[M200] + HMcoll[M020]) + cs4;
        RMcoll[M202] = HMcoll[M202] + cs2*(HMcoll[M200] + HMcoll[M002]) + cs4;
        RMcoll[M022] = HMcoll[M022] + cs2*(HMcoll[M020] + HMcoll[M002]) + cs4;

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_10 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00 =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_10;

        double pop_out_11 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01 =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_11;

        double pop_out_12 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02 =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_12;

        double pop_out_13 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04 =  0.5*rho * (-RMcoll[M110]              - RMcoll[M120]) + pop_out_13;
        double pop_out_14 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])              + pop_out_13;
        double pop_out_03 =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_13;

        double pop_out_15 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06 =  0.5*rho * (-RMcoll[M101]              - RMcoll[M102]) + pop_out_15;
        double pop_out_16 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])              + pop_out_15;
        double pop_out_05 =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_15;

        double pop_out_17 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08 =  0.5*rho * (-RMcoll[M011]              - RMcoll[M012]) + pop_out_17;
        double pop_out_18 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])              + pop_out_17;
        double pop_out_07 =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_17;

        f(i,  0) = pop_out_10;
        f(i, 10) = pop_out_00;

        f(i,  1) = pop_out_11;
        f(i, 11) = pop_out_01;

        f(i,  2) = pop_out_12;
        f(i, 12) = pop_out_02;

        f(i,  3) = pop_out_13;
        f(i, 13) = pop_out_03;

        f(i,  4) = pop_out_14;
        f(i, 14) = pop_out_04;

        f(i,  5) = pop_out_15;
        f(i, 15) = pop_out_05;

        f(i,  6) = pop_out_16;
        f(i, 16) = pop_out_06;

        f(i,  7) = pop_out_17;
        f(i, 17) = pop_out_07;

        f(i,  8) = pop_out_18;
        f(i, 18) = pop_out_08;

        f(i, 9) = pop_out_09;
    }


    void iterateCHM(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            std::array<double, 19> pop;
            for (int k = 0; k < 19; ++k) {
                pop[k] = f(i, k);
            }
/*            auto[rho, u] = macropop(pop);
            auto CHM = computeCHMopt(pop, rho, u);*/
            auto CHM = computeCHMopt2(pop);
            double rho = CHM[M000];
            std::array<double, 3> u = {CHM[M100], CHM[M010], CHM[M001]};
            auto CHMeq = computeCHMeq();
            collideCHM(i, rho, u, CHM, CHMeq);
        }
    }

    void operator() (double& f0) {
        iterateCHM(f0);
    }
};

struct Odd : public LBM {

    auto collideCHM(std::array<double, 19>& pop, double rho, std::array<double, 3> const& u,
        std::array<double, 19> const& CHM, std::array<double, 19> const& CHMeq)
    {
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        // Post-collision moments.
        std::array<double, 19> RMcoll;
        std::array<double, 19> HMcoll;
        std::array<double, 19> CHMcoll;

        // Collision in the central Hermite moment space (CHMeq are replaced by their values)
        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity
            CHMcoll[M200] = (1. - omega1) * CHM[M200];
            CHMcoll[M020] = (1. - omega1) * CHM[M020];
            CHMcoll[M002] = (1. - omega1) * CHM[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaBulk = omega1;
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            CHMcoll[M200] = CHM[M200] - omegaPlus  * (CHM[M200]) - omegaMinus * (CHM[M020]) - omegaMinus * (CHM[M002]) ;
            CHMcoll[M020] = CHM[M020] - omegaMinus * (CHM[M200]) - omegaPlus  * (CHM[M020]) - omegaMinus * (CHM[M002]) ;
            CHMcoll[M002] = CHM[M002] - omegaMinus * (CHM[M200]) - omegaMinus * (CHM[M020]) - omegaPlus  * (CHM[M002]) ;
        }

        CHMcoll[M110] = (1. - omega2) * CHM[M110];
        CHMcoll[M101] = (1. - omega2) * CHM[M101];
        CHMcoll[M011] = (1. - omega2) * CHM[M011];

        // Order 3
        CHMcoll[M210] = (1. - omega3) * CHM[M210];
        CHMcoll[M201] = (1. - omega3) * CHM[M201];
        CHMcoll[M021] = (1. - omega3) * CHM[M021];
        CHMcoll[M120] = (1. - omega3) * CHM[M120];
        CHMcoll[M102] = (1. - omega3) * CHM[M102];
        CHMcoll[M012] = (1. - omega3) * CHM[M012];
        
        // Order 4
        CHMcoll[M220] = (1. - omega4) * CHM[M220];
        CHMcoll[M202] = (1. - omega4) * CHM[M202];
        CHMcoll[M022] = (1. - omega4) * CHM[M022];

        // Come back to HMcoll using relationships between CHMs and HMs

        double ux2 = u[0]*u[0];
        double uy2 = u[1]*u[1];
        double uz2 = u[2]*u[2];

        HMcoll[M200] = CHMcoll[M200] + ux2;
        HMcoll[M020] = CHMcoll[M020] + uy2;
        HMcoll[M002] = CHMcoll[M002] + uz2;
        
        HMcoll[M110] = CHMcoll[M110] + u[0]*u[1];
        HMcoll[M101] = CHMcoll[M101] + u[0]*u[2];
        HMcoll[M011] = CHMcoll[M011] + u[1]*u[2];

        HMcoll[M210] = CHMcoll[M210] + u[1]*CHMcoll[M200] + 2.*u[0]*CHMcoll[M110] + ux2*u[1];
        HMcoll[M201] = CHMcoll[M201] + u[2]*CHMcoll[M200] + 2.*u[0]*CHMcoll[M101] + ux2*u[2];
        HMcoll[M021] = CHMcoll[M021] + u[2]*CHMcoll[M020] + 2.*u[1]*CHMcoll[M011] + uy2*u[2];
        HMcoll[M120] = CHMcoll[M120] + u[0]*CHMcoll[M020] + 2.*u[1]*CHMcoll[M110] + u[0]*uy2;
        HMcoll[M102] = CHMcoll[M102] + u[0]*CHMcoll[M002] + 2.*u[2]*CHMcoll[M101] + u[0]*uz2;
        HMcoll[M012] = CHMcoll[M012] + u[1]*CHMcoll[M002] + 2.*u[2]*CHMcoll[M011] + u[1]*uz2;
        
        HMcoll[M220] = CHMcoll[M220] + 2.*u[1]*CHMcoll[M210] + 2.*u[0]*CHMcoll[M120] + uy2*CHMcoll[M200] + ux2*CHMcoll[M020] + 4.*u[0]*u[1]*CHMcoll[M110] + ux2*uy2;
        HMcoll[M202] = CHMcoll[M202] + 2.*u[2]*CHMcoll[M201] + 2.*u[0]*CHMcoll[M102] + uz2*CHMcoll[M200] + ux2*CHMcoll[M002] + 4.*u[0]*u[2]*CHMcoll[M101] + ux2*uz2;
        HMcoll[M022] = CHMcoll[M022] + 2.*u[2]*CHMcoll[M021] + 2.*u[1]*CHMcoll[M012] + uz2*CHMcoll[M020] + uy2*CHMcoll[M002] + 4.*u[1]*u[2]*CHMcoll[M011] + uy2*uz2;

        // Come back to RMcoll using relationships between HMs and RMs
        RMcoll[M200] = HMcoll[M200] + cs2;
        RMcoll[M020] = HMcoll[M020] + cs2;
        RMcoll[M002] = HMcoll[M002] + cs2;
        
        RMcoll[M110] = HMcoll[M110];
        RMcoll[M101] = HMcoll[M101];
        RMcoll[M011] = HMcoll[M011];

        RMcoll[M210] = HMcoll[M210] + cs2*u[1];
        RMcoll[M201] = HMcoll[M201] + cs2*u[2];
        RMcoll[M021] = HMcoll[M021] + cs2*u[2];
        RMcoll[M120] = HMcoll[M120] + cs2*u[0];
        RMcoll[M102] = HMcoll[M102] + cs2*u[0];
        RMcoll[M012] = HMcoll[M012] + cs2*u[1];
        
        RMcoll[M220] = HMcoll[M220] + cs2*(HMcoll[M200] + HMcoll[M020]) + cs4;
        RMcoll[M202] = HMcoll[M202] + cs2*(HMcoll[M200] + HMcoll[M002]) + cs4;
        RMcoll[M022] = HMcoll[M022] + cs2*(HMcoll[M020] + HMcoll[M002]) + cs4;

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_10 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00 =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_10;

        double pop_out_11 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01 =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_11;

        double pop_out_12 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02 =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_12;

        double pop_out_13 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04 =  0.5*rho * (-RMcoll[M110]              - RMcoll[M120]) + pop_out_13;
        double pop_out_14 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])              + pop_out_13;
        double pop_out_03 =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_13;

        double pop_out_15 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06 =  0.5*rho * (-RMcoll[M101]              - RMcoll[M102]) + pop_out_15;
        double pop_out_16 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])              + pop_out_15;
        double pop_out_05 =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_15;

        double pop_out_17 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08 =  0.5*rho * (-RMcoll[M011]              - RMcoll[M012]) + pop_out_17;
        double pop_out_18 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])              + pop_out_17;
        double pop_out_07 =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_17;


        pop[ 0] = pop_out_00;
        pop[10] = pop_out_10;

        pop[ 1] = pop_out_01;
        pop[11] = pop_out_11;

        pop[ 2] = pop_out_02;
        pop[12] = pop_out_12;

        pop[ 3] = pop_out_03;
        pop[13] = pop_out_13;

        pop[ 4] = pop_out_04;
        pop[14] = pop_out_14;

        pop[ 5] = pop_out_05;
        pop[15] = pop_out_15;

        pop[ 6] = pop_out_06;
        pop[16] = pop_out_16;

        pop[ 7] = pop_out_07;
        pop[17] = pop_out_17;

        pop[ 8] = pop_out_08;
        pop[18] = pop_out_18;

        pop[ 9] = pop_out_09;
    }

    auto streamingPull(int i, int iX, int iY, int iZ, std::array<double, 19>& pop)
    {
        if(periodic){
            for (int k = 0; k < 19; ++k) {
                int XX = (iX - c[k][0] + dim.nx) % dim.nx;
                int YY = (iY - c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ - c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
        else {
            for (int k = 0; k < 19; ++k) {
                int XX = iX - c[k][0];
                int YY = iY - c[k][1];
                int ZZ = iZ - c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }
        }
    }

    auto streamingPush(int i, int iX, int iY, int iZ, std::array<double, 19>& pop)
    {
        if(periodic){
            for (int k = 0; k < 19; ++k) {
                int XX = (iX + c[k][0] + dim.nx) % dim.nx;
                int YY = (iY + c[k][1] + dim.ny) % dim.ny;
                int ZZ = (iZ + c[k][2] + dim.nz) % dim.nz;
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
        else {
            for (int k = 0; k < 19; ++k) {
                int XX = iX + c[k][0];
                int YY = iY + c[k][1];
                int ZZ = iZ + c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
    }

    void iterateCHM(double& f0) {
        int i = &f0 - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            std::array<double, 19> pop;

            streamingPull(i, iX, iY, iZ, pop);

/*            auto[rho, u] = macropop(pop);
            auto CHM = computeCHMopt(pop, rho, u);*/
            auto CHM = computeCHMopt2(pop);
            double rho = CHM[M000];
            std::array<double, 3> u = {CHM[M100], CHM[M010], CHM[M001]};
            auto CHMeq = computeCHMeq();

            collideCHM(pop, rho, u, CHM, CHMeq);

            streamingPush(i, iX, iY, iZ, pop);
        }
    }

    void operator() (double& f0) {
        iterateCHM(f0);
    }
};

} // namespace aa_soa_chm
